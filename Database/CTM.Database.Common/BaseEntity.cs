﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.Common
{
    public class BaseEntity
    {
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
