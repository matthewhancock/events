﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Constants
{
    public static class Roles
    {
        public static class Names
        {
            public const string Administrator = "Administrator";
            public const string ClientManager = "Client Manager";
            public const string Traveller = "Traveller";
            public const string TravelArranger = "Travel Arranger";
        }
    }
}
