using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_FareForecasterItem
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string LocationCode = nameof(LocationCode);
        public const string MaxNumberOfStops = nameof(MaxNumberOfStops);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string NextGenFareForecasterId = nameof(NextGenFareForecasterId);
        public const string OriginLocationCode = nameof(OriginLocationCode);
    }
}