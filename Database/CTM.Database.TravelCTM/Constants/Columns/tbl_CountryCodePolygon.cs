using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_CountryCodePolygon
    {
        public const string Coordinates = nameof(Coordinates);
        public const string CountryCodeId = nameof(CountryCodeId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Type = nameof(Type);
    }
}