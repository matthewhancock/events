using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_SMSProvider
    {
        public const string ApiTimeoutMilliseconds = nameof(ApiTimeoutMilliseconds);
        public const string ApiUrl = nameof(ApiUrl);
        public const string CallbackTimeLimit = nameof(CallbackTimeLimit);
        public const string CallbackUrl = nameof(CallbackUrl);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Title = nameof(Title);
    }
}