using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class SchemaVersions
    {
        public const string Applied = nameof(Applied);
        public const string Id = nameof(Id);
        public const string ScriptName = nameof(ScriptName);
    }
}