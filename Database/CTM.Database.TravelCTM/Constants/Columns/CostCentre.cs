using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class CostCentre
    {
        public const string CostCentreCode = nameof(CostCentreCode);
        public const string CostCentreName = nameof(CostCentreName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorCode = nameof(DebtorCode);
        public const string DepartmentCode = nameof(DepartmentCode);
        public const string Enabled = nameof(Enabled);
        public const string ExternalCostCentreId = nameof(ExternalCostCentreId);
        public const string ExternalDepartmentId = nameof(ExternalDepartmentId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string SourceTypeId = nameof(SourceTypeId);
        public const string TerminationDate = nameof(TerminationDate);
    }
}