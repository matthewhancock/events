using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class Department
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorCode = nameof(DebtorCode);
        public const string DepartmentCode = nameof(DepartmentCode);
        public const string DepartmentL2Code = nameof(DepartmentL2Code);
        public const string DepartmentL2Name = nameof(DepartmentL2Name);
        public const string DepartmentName = nameof(DepartmentName);
        public const string Enabled = nameof(Enabled);
        public const string ExternalDepartmentId = nameof(ExternalDepartmentId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string SourceTypeId = nameof(SourceTypeId);
        public const string TerminationDate = nameof(TerminationDate);
    }
}