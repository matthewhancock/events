using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_ItineraryItemRiskMessage
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Email = nameof(Email);
        public const string EmailSent = nameof(EmailSent);
        public const string Enabled = nameof(Enabled);
        public const string GivenName = nameof(GivenName);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryItemId = nameof(ItineraryItemId);
        public const string Mobile = nameof(Mobile);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PNR = nameof(PNR);
        public const string RiskDebtorGroupId = nameof(RiskDebtorGroupId);
        public const string RiskMessageId = nameof(RiskMessageId);
        public const string RiskTravelArrangerGroupId = nameof(RiskTravelArrangerGroupId);
        public const string SMSSent = nameof(SMSSent);
        public const string Surname = nameof(Surname);
        public const string Title = nameof(Title);
    }
}