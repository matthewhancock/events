using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_Widget
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string FeatureId = nameof(FeatureId);
        public const string FeatureIdentifier = nameof(FeatureIdentifier);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string MaxWidgets = nameof(MaxWidgets);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string ObjectName = nameof(ObjectName);
        public const string Title = nameof(Title);
        public const string WidgetInfo = nameof(WidgetInfo);
    }
}