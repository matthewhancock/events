using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_tbl_DebtorSerkoPolicy
    {
        public const string Countries = nameof(Countries);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PolicyCode = nameof(PolicyCode);
        public const string PolicyPriority = nameof(PolicyPriority);
        public const string Type = nameof(Type);
        public const string TypeId = nameof(TypeId);
    }
}