using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_ReviewResultAnswer
    {
        public const string AnswerId = nameof(AnswerId);
        public const string AnswerValue = nameof(AnswerValue);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string QuestionId = nameof(QuestionId);
        public const string QuestionTitle = nameof(QuestionTitle);
        public const string ReviewResultId = nameof(ReviewResultId);
        public const string Title = nameof(Title);
    }
}