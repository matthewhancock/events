using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_BedTrackerItem
    {
        public const string AverageRate = nameof(AverageRate);
        public const string BuildEndDateTime = nameof(BuildEndDateTime);
        public const string BuildStartDateTime = nameof(BuildStartDateTime);
        public const string CombinedAverageChangePercent = nameof(CombinedAverageChangePercent);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string ForecastDate = nameof(ForecastDate);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string NextGenBedTrackerId = nameof(NextGenBedTrackerId);
    }
}