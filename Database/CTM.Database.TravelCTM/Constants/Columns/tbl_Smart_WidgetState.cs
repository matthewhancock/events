using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Smart_WidgetState
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Id = nameof(Id);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string SettingState = nameof(SettingState);
        public const string UiState = nameof(UiState);
        public const string UserId = nameof(UserId);
        public const string UserState = nameof(UserState);
        public const string WidgetState = nameof(WidgetState);
    }
}