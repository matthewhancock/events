using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_UserPasswordToken
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string RedirectUrl = nameof(RedirectUrl);
        public const string RequestDate = nameof(RequestDate);
        public const string TokenDurationInSeconds = nameof(TokenDurationInSeconds);
        public const string TokenExpiryDate = nameof(TokenExpiryDate);
        public const string TokenUsedDate = nameof(TokenUsedDate);
        public const string UserId = nameof(UserId);
    }
}