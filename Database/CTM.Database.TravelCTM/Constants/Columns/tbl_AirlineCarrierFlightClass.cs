using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_AirlineCarrierFlightClass
    {
        public const string BookingCodes = nameof(BookingCodes);
        public const string CarrierCode = nameof(CarrierCode);
        public const string CarrierId = nameof(CarrierId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string GroupType = nameof(GroupType);
        public const string GroupTypeId = nameof(GroupTypeId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Title = nameof(Title);
    }
}