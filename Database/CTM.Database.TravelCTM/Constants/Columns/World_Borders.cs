using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class World_Borders
    {
        public const string FIPS = nameof(FIPS);
        public const string GEOG = nameof(GEOG);
        public const string GEOM = nameof(GEOM);
        public const string ISO2 = nameof(ISO2);
        public const string ISO3 = nameof(ISO3);
        public const string LAT = nameof(LAT);
        public const string LON = nameof(LON);
        public const string NAME = nameof(NAME);
        public const string POP2005 = nameof(POP2005);
        public const string REGION = nameof(REGION);
        public const string SUBREGION = nameof(SUBREGION);
        public const string UN = nameof(UN);
    }
}