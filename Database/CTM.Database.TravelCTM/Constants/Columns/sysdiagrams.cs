using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class sysdiagrams
    {
        public const string definition = nameof(definition);
        public const string diagram_id = nameof(diagram_id);
        public const string name = nameof(name);
        public const string principal_id = nameof(principal_id);
        public const string version = nameof(version);
    }
}