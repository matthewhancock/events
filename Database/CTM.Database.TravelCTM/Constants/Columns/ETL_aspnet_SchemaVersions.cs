using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_aspnet_SchemaVersions
    {
        public const string CompatibleSchemaVersion = nameof(CompatibleSchemaVersion);
        public const string Feature = nameof(Feature);
        public const string IsCurrentVersion = nameof(IsCurrentVersion);
    }
}