using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_SabreNotification
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string ErrorMessage = nameof(ErrorMessage);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsDownloaded = nameof(IsDownloaded);
        public const string IsProcessing = nameof(IsProcessing);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PCC = nameof(PCC);
        public const string PNR = nameof(PNR);
        public const string ProcessingIdentifier = nameof(ProcessingIdentifier);
        public const string RequestDataXml = nameof(RequestDataXml);
    }
}