using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_CarSupplierVehicle
    {
        public const string CarSupplierId = nameof(CarSupplierId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Image = nameof(Image);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OptionAirConditioning = nameof(OptionAirConditioning);
        public const string OptionAutomatic = nameof(OptionAutomatic);
        public const string OptionDoorCount = nameof(OptionDoorCount);
        public const string OptionLuggageCount = nameof(OptionLuggageCount);
        public const string OptionPassengerCount = nameof(OptionPassengerCount);
        public const string Title = nameof(Title);
        public const string VehicleCategory = nameof(VehicleCategory);
        public const string VehicleClass = nameof(VehicleClass);
        public const string VehicleSize = nameof(VehicleSize);
        public const string VehicleTypeCode = nameof(VehicleTypeCode);
    }
}