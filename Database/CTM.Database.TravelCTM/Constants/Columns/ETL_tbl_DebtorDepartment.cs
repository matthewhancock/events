using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_tbl_DebtorDepartment
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DepartmentCode = nameof(DepartmentCode);
        public const string DepartmentCodeName = nameof(DepartmentCodeName);
        public const string DepartmentId = nameof(DepartmentId);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string RiskEngineCountries = nameof(RiskEngineCountries);
        public const string RiskEngineEmailEnabled = nameof(RiskEngineEmailEnabled);
        public const string RiskEngineMinRating = nameof(RiskEngineMinRating);
        public const string RiskEngineSMSEnabled = nameof(RiskEngineSMSEnabled);
        public const string Title = nameof(Title);
    }
}