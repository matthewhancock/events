using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class provision_marker_dss
    {
        public const string object_id = nameof(object_id);
        public const string owner_scope_local_id = nameof(owner_scope_local_id);
        public const string provision_datetime = nameof(provision_datetime);
        public const string provision_local_peer_key = nameof(provision_local_peer_key);
        public const string provision_scope_local_id = nameof(provision_scope_local_id);
        public const string provision_scope_peer_key = nameof(provision_scope_peer_key);
        public const string provision_scope_peer_timestamp = nameof(provision_scope_peer_timestamp);
        public const string provision_timestamp = nameof(provision_timestamp);
        public const string state = nameof(state);
        public const string version = nameof(version);
    }
}