using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_BedTrackerDemandLevel
    {
        public const string BasePercentDifference = nameof(BasePercentDifference);
        public const string CeilingPercentDifference = nameof(CeilingPercentDifference);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DemandLevel = nameof(DemandLevel);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
    }
}