using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_ItinerarySearchFlight
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DepartureDate = nameof(DepartureDate);
        public const string DestinationLocation = nameof(DestinationLocation);
        public const string DestinationLocationCode = nameof(DestinationLocationCode);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryId = nameof(ItineraryId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginLocation = nameof(OriginLocation);
        public const string OriginLocationCode = nameof(OriginLocationCode);
    }
}