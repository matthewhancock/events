using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_ItineraryCustomer
    {
        public const string BiTravellerEmail = nameof(BiTravellerEmail);
        public const string BiTravellerId = nameof(BiTravellerId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string GivenName = nameof(GivenName);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryId = nameof(ItineraryId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Surname = nameof(Surname);
    }
}