using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_ItinerarySearchCar
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DropOffDate = nameof(DropOffDate);
        public const string DropOffLocation = nameof(DropOffLocation);
        public const string DropOffLocationCode = nameof(DropOffLocationCode);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryId = nameof(ItineraryId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PickUpDate = nameof(PickUpDate);
        public const string PickUpLocation = nameof(PickUpLocation);
        public const string PickUpLocationCode = nameof(PickUpLocationCode);
    }
}