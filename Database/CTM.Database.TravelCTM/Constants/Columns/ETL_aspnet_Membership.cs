using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_aspnet_Membership
    {
        public const string ApplicationId = nameof(ApplicationId);
        public const string Comment = nameof(Comment);
        public const string CreateDate = nameof(CreateDate);
        public const string Email = nameof(Email);
        public const string FailedPasswordAnswerAttemptCount = nameof(FailedPasswordAnswerAttemptCount);
        public const string FailedPasswordAnswerAttemptWindowStart = nameof(FailedPasswordAnswerAttemptWindowStart);
        public const string FailedPasswordAttemptCount = nameof(FailedPasswordAttemptCount);
        public const string FailedPasswordAttemptWindowStart = nameof(FailedPasswordAttemptWindowStart);
        public const string IsApproved = nameof(IsApproved);
        public const string IsLockedOut = nameof(IsLockedOut);
        public const string LastLockoutDate = nameof(LastLockoutDate);
        public const string LastLoginDate = nameof(LastLoginDate);
        public const string LastPasswordChangedDate = nameof(LastPasswordChangedDate);
        public const string LoweredEmail = nameof(LoweredEmail);
        public const string MobilePIN = nameof(MobilePIN);
        public const string Password = nameof(Password);
        public const string PasswordAnswer = nameof(PasswordAnswer);
        public const string PasswordFormat = nameof(PasswordFormat);
        public const string PasswordQuestion = nameof(PasswordQuestion);
        public const string PasswordSalt = nameof(PasswordSalt);
        public const string UserId = nameof(UserId);
    }
}