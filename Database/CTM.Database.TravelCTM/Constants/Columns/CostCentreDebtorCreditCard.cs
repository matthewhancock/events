using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class CostCentreDebtorCreditCard
    {
        public const string CostCentreId = nameof(CostCentreId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorCreditCardId = nameof(DebtorCreditCardId);
        public const string Enabled = nameof(Enabled);
        public const string ExternalCostCentreId = nameof(ExternalCostCentreId);
        public const string ExternalDebtorCreditCardId = nameof(ExternalDebtorCreditCardId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string SourceTypeId = nameof(SourceTypeId);
    }
}