using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_Itinerary
    {
        public const string AgencyReasonCodeId = nameof(AgencyReasonCodeId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string DebtorId = nameof(DebtorId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string DestinationInternationalFlight = nameof(DestinationInternationalFlight);
        public const string DestinationLocation = nameof(DestinationLocation);
        public const string DestinationLocationCode = nameof(DestinationLocationCode);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OrderNumber = nameof(OrderNumber);
        public const string OriginInternationalFlight = nameof(OriginInternationalFlight);
        public const string OriginLocation = nameof(OriginLocation);
        public const string OriginLocationCode = nameof(OriginLocationCode);
        public const string PaxCount = nameof(PaxCount);
        public const string ReturnTrip = nameof(ReturnTrip);
        public const string SearchTypes = nameof(SearchTypes);
        public const string StatusId = nameof(StatusId);
        public const string UserId = nameof(UserId);
    }
}