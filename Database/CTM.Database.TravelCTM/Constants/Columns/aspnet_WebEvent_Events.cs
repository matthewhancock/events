using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class aspnet_WebEvent_Events
    {
        public const string ApplicationPath = nameof(ApplicationPath);
        public const string ApplicationVirtualPath = nameof(ApplicationVirtualPath);
        public const string Details = nameof(Details);
        public const string EventCode = nameof(EventCode);
        public const string EventDetailCode = nameof(EventDetailCode);
        public const string EventId = nameof(EventId);
        public const string EventOccurrence = nameof(EventOccurrence);
        public const string EventSequence = nameof(EventSequence);
        public const string EventTime = nameof(EventTime);
        public const string EventTimeUtc = nameof(EventTimeUtc);
        public const string EventType = nameof(EventType);
        public const string ExceptionType = nameof(ExceptionType);
        public const string MachineName = nameof(MachineName);
        public const string Message = nameof(Message);
        public const string RequestUrl = nameof(RequestUrl);
    }
}