using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Smart_FeatureOption
    {
        public const string DefaultValue = nameof(DefaultValue);
        public const string FeatureId = nameof(FeatureId);
        public const string Id = nameof(Id);
        public const string IsRoleOption = nameof(IsRoleOption);
        public const string Key = nameof(Key);
    }
}