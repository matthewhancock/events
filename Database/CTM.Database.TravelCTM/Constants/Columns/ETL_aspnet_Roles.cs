using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_aspnet_Roles
    {
        public const string ApplicationId = nameof(ApplicationId);
        public const string Description = nameof(Description);
        public const string LoweredRoleName = nameof(LoweredRoleName);
        public const string RoleId = nameof(RoleId);
        public const string RoleName = nameof(RoleName);
    }
}