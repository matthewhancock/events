using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class __EFMigrationsHistory
    {
        public const string MigrationId = nameof(MigrationId);
        public const string ProductVersion = nameof(ProductVersion);
    }
}