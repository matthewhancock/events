using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_ItinerarySearchHotel
    {
        public const string CheckIn = nameof(CheckIn);
        public const string CheckOut = nameof(CheckOut);
        public const string City = nameof(City);
        public const string CityCode = nameof(CityCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryId = nameof(ItineraryId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
    }
}