using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_CarSupplierLocation
    {
        public const string AddressLine = nameof(AddressLine);
        public const string CarSupplierId = nameof(CarSupplierId);
        public const string CityName = nameof(CityName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string ExtendedLocationCode = nameof(ExtendedLocationCode);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string Latitude = nameof(Latitude);
        public const string LocationCode = nameof(LocationCode);
        public const string LocationName = nameof(LocationName);
        public const string Longtitude = nameof(Longtitude);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string NoRateReturned = nameof(NoRateReturned);
        public const string StateCode = nameof(StateCode);
        public const string Title = nameof(Title);
    }
}