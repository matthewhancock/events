using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_SubmissionQuoteItem
    {
        public const string Address = nameof(Address);
        public const string Amount = nameof(Amount);
        public const string CodeShareText = nameof(CodeShareText);
        public const string Comments = nameof(Comments);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItemCode = nameof(ItemCode);
        public const string ItineraryItemId = nameof(ItineraryItemId);
        public const string ItineraryTypeId = nameof(ItineraryTypeId);
        public const string LocationCityName = nameof(LocationCityName);
        public const string LocationCode = nameof(LocationCode);
        public const string LocationCountryName = nameof(LocationCountryName);
        public const string LocationTerminal = nameof(LocationTerminal);
        public const string MarketingCode = nameof(MarketingCode);
        public const string MarketingFlightNumber = nameof(MarketingFlightNumber);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginCityName = nameof(OriginCityName);
        public const string OriginCountryName = nameof(OriginCountryName);
        public const string OriginLocationCode = nameof(OriginLocationCode);
        public const string OriginTerminal = nameof(OriginTerminal);
        public const string PNR = nameof(PNR);
        public const string PTASubmissionQuoteId = nameof(PTASubmissionQuoteId);
        public const string Reference = nameof(Reference);
        public const string RequestOrder = nameof(RequestOrder);
        public const string SeatNumber = nameof(SeatNumber);
        public const string StopQuantity = nameof(StopQuantity);
        public const string SupplierCode = nameof(SupplierCode);
        public const string SupplierName = nameof(SupplierName);
        public const string SupplierReference = nameof(SupplierReference);
        public const string Title = nameof(Title);
    }
}