using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_SMSLog
    {
        public const string CallbackResponseMessage = nameof(CallbackResponseMessage);
        public const string Country = nameof(Country);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string ExceptionMessage = nameof(ExceptionMessage);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsDelivered = nameof(IsDelivered);
        public const string IsSendingSuccessful = nameof(IsSendingSuccessful);
        public const string Message = nameof(Message);
        public const string Mobiles = nameof(Mobiles);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string ProviderId = nameof(ProviderId);
        public const string Reference = nameof(Reference);
        public const string ResponseMessage = nameof(ResponseMessage);
        public const string Source = nameof(Source);
        public const string TravellerFullName = nameof(TravellerFullName);
    }
}