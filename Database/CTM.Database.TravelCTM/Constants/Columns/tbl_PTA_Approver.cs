using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_Approver
    {
        public const string CostCentreCodes = nameof(CostCentreCodes);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DepartmentCodes = nameof(DepartmentCodes);
        public const string EmployeeTypeCodes = nameof(EmployeeTypeCodes);
        public const string Enabled = nameof(Enabled);
        public const string HideApprover = nameof(HideApprover);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsSuperApprover = nameof(IsSuperApprover);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string ProjectCodes = nameof(ProjectCodes);
        public const string PTAApproverDebtorTierId = nameof(PTAApproverDebtorTierId);
        public const string Title = nameof(Title);
        public const string UserId = nameof(UserId);
    }
}