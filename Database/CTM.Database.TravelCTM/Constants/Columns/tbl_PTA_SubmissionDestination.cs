using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_SubmissionDestination
    {
        public const string CityName = nameof(CityName);
        public const string Comments = nameof(Comments);
        public const string CountryName = nameof(CountryName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsInternational = nameof(IsInternational);
        public const string IsLandOnly = nameof(IsLandOnly);
        public const string LocationCode = nameof(LocationCode);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginCityName = nameof(OriginCityName);
        public const string OriginCountryName = nameof(OriginCountryName);
        public const string OriginLocationCode = nameof(OriginLocationCode);
        public const string PTASubmissionId = nameof(PTASubmissionId);
    }
}