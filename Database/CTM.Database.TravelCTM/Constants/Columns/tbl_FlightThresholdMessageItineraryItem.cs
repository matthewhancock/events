using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_FlightThresholdMessageItineraryItem
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Enabled = nameof(Enabled);
        public const string FlightThresholdMessageId = nameof(FlightThresholdMessageId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryItemId = nameof(ItineraryItemId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PNR = nameof(PNR);
        public const string Title = nameof(Title);
    }
}