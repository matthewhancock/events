using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class scope_info_dss
    {
        public const string scope_config_id = nameof(scope_config_id);
        public const string scope_id = nameof(scope_id);
        public const string scope_local_id = nameof(scope_local_id);
        public const string scope_restore_count = nameof(scope_restore_count);
        public const string scope_sync_knowledge = nameof(scope_sync_knowledge);
        public const string scope_timestamp = nameof(scope_timestamp);
        public const string scope_tombstone_cleanup_knowledge = nameof(scope_tombstone_cleanup_knowledge);
        public const string scope_user_comment = nameof(scope_user_comment);
        public const string sync_scope_name = nameof(sync_scope_name);
    }
}