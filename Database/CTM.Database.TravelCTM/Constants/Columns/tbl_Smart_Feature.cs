using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Smart_Feature
    {
        public const string Description = nameof(Description);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Name = nameof(Name);
    }
}