using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class Tbl_user_jorge
    {
        public const string Azure_Authnmethodsreferences = nameof(Azure_Authnmethodsreferences);
        public const string Azure_Chash = nameof(Azure_Chash);
        public const string Azure_Givenname = nameof(Azure_Givenname);
        public const string Azure_Name = nameof(Azure_Name);
        public const string Azure_NameIdentifier = nameof(Azure_NameIdentifier);
        public const string Azure_ObjectIdentifier = nameof(Azure_ObjectIdentifier);
        public const string Azure_PasswordExp = nameof(Azure_PasswordExp);
        public const string Azure_PasswordUrl = nameof(Azure_PasswordUrl);
        public const string Azure_Surname = nameof(Azure_Surname);
        public const string Azure_TenantId = nameof(Azure_TenantId);
        public const string Azure_Upn = nameof(Azure_Upn);
        public const string BusinessIntel_PseudoEmployeeId = nameof(BusinessIntel_PseudoEmployeeId);
        public const string BusinessIntelligence_TravelArrangerId = nameof(BusinessIntelligence_TravelArrangerId);
        public const string BusinessIntelligence_TravellerId = nameof(BusinessIntelligence_TravellerId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Email = nameof(Email);
        public const string ExternalIdentifier = nameof(ExternalIdentifier);
        public const string Id = nameof(Id);
        public const string IsChildUserAccount = nameof(IsChildUserAccount);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsSsoUser = nameof(IsSsoUser);
        public const string IsWelcomeEmailSent = nameof(IsWelcomeEmailSent);
        public const string MembershipGuid = nameof(MembershipGuid);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string NextGen_BedTracker_DefaultCountryCode = nameof(NextGen_BedTracker_DefaultCountryCode);
        public const string NextGen_BedTracker_DefaultLocationCode = nameof(NextGen_BedTracker_DefaultLocationCode);
        public const string NextGen_BusinessIntelligence_DefaultReportAxisId = nameof(NextGen_BusinessIntelligence_DefaultReportAxisId);
        public const string NextGen_BusinessIntelligence_DefaultReportId = nameof(NextGen_BusinessIntelligence_DefaultReportId);
        public const string NextGen_BusinessIntelligence_DefaultReportPeriodTypeId = nameof(NextGen_BusinessIntelligence_DefaultReportPeriodTypeId);
        public const string NextGen_BusinessIntelligence_DefaultReportSpendTypeId = nameof(NextGen_BusinessIntelligence_DefaultReportSpendTypeId);
        public const string NextGen_FareForecaster_DefaultReverseTrip = nameof(NextGen_FareForecaster_DefaultReverseTrip);
        public const string NextGen_FareForecaster_DefaultReverseTripTimeslotId = nameof(NextGen_FareForecaster_DefaultReverseTripTimeslotId);
        public const string NextGen_FareForecaster_DefaultRouteId = nameof(NextGen_FareForecaster_DefaultRouteId);
        public const string NextGen_FareForecaster_DefaultTimeslotId = nameof(NextGen_FareForecaster_DefaultTimeslotId);
        public const string NextGen_PasswordAttemptTimes = nameof(NextGen_PasswordAttemptTimes);
        public const string NextGen_PasswordForgotPasswordResetDate = nameof(NextGen_PasswordForgotPasswordResetDate);
        public const string NextGen_PasswordUpdateDate = nameof(NextGen_PasswordUpdateDate);
        public const string NextGen_TermConditionsRead = nameof(NextGen_TermConditionsRead);
        public const string NextGen_TravelTracker_DefaultTabId = nameof(NextGen_TravelTracker_DefaultTabId);
        public const string NextGen_UserWidgets = nameof(NextGen_UserWidgets);
        public const string NextGen_WelcomeVideoRead = nameof(NextGen_WelcomeVideoRead);
        public const string ResetPasswordTriggered = nameof(ResetPasswordTriggered);
        public const string ResetSecurityQuestionTriggered = nameof(ResetSecurityQuestionTriggered);
        public const string Sabre_ProfileName = nameof(Sabre_ProfileName);
        public const string Serko_CorporateAccountCode = nameof(Serko_CorporateAccountCode);
        public const string Title = nameof(Title);
        public const string UApprove_Hash = nameof(UApprove_Hash);
        public const string Username = nameof(Username);
        public const string UserRoleList = nameof(UserRoleList);
        public const string WebApiAccessToken = nameof(WebApiAccessToken);
    }
}