using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_ItineraryPassenger
    {
        public const string BusinessIntelligence_CreditCardId = nameof(BusinessIntelligence_CreditCardId);
        public const string ClientId = nameof(ClientId);
        public const string CompanyName = nameof(CompanyName);
        public const string ContactId = nameof(ContactId);
        public const string CostCentreCode = nameof(CostCentreCode);
        public const string CostCentreId = nameof(CostCentreId);
        public const string CostCentreName = nameof(CostCentreName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string CreditCardDisplayNumber = nameof(CreditCardDisplayNumber);
        public const string CreditCardEncryptedNumber = nameof(CreditCardEncryptedNumber);
        public const string CreditCardExpiryDate = nameof(CreditCardExpiryDate);
        public const string CreditCardHolder = nameof(CreditCardHolder);
        public const string CreditCardTypeCode = nameof(CreditCardTypeCode);
        public const string DepartmentCode = nameof(DepartmentCode);
        public const string DepartmentId = nameof(DepartmentId);
        public const string DepartmentName = nameof(DepartmentName);
        public const string Email = nameof(Email);
        public const string EmployeeNumber = nameof(EmployeeNumber);
        public const string Enabled = nameof(Enabled);
        public const string Firstname = nameof(Firstname);
        public const string FrequentFlyerCodes = nameof(FrequentFlyerCodes);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IndexField = nameof(IndexField);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryId = nameof(ItineraryId);
        public const string Lastname = nameof(Lastname);
        public const string Mobile = nameof(Mobile);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PNR = nameof(PNR);
        public const string SabreErrorXml = nameof(SabreErrorXml);
        public const string SabreReservationXml = nameof(SabreReservationXml);
        public const string SelectedCostCentreCode = nameof(SelectedCostCentreCode);
        public const string SelectedCostCentreId = nameof(SelectedCostCentreId);
        public const string SelectedCostCentreName = nameof(SelectedCostCentreName);
        public const string StatusId = nameof(StatusId);
        public const string Title = nameof(Title);
    }
}