using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_QuestionLogic
    {
        public const string ActionValue = nameof(ActionValue);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PTAActionTypeId = nameof(PTAActionTypeId);
        public const string PTAActionTypeTitle = nameof(PTAActionTypeTitle);
        public const string PTAQuestionId = nameof(PTAQuestionId);
        public const string Sort = nameof(Sort);
    }
}