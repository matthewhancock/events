using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_Document
    {
        public const string CategoryId = nameof(CategoryId);
        public const string CategoryTitle = nameof(CategoryTitle);
        public const string CountryCode = nameof(CountryCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string DocumentRoleType = nameof(DocumentRoleType);
        public const string Enabled = nameof(Enabled);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string ExternalUrl = nameof(ExternalUrl);
        public const string FilePath = nameof(FilePath);
        public const string FileSize = nameof(FileSize);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Title = nameof(Title);
        public const string UserId = nameof(UserId);
    }
}