using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_RiskSmartTravellerAdvice
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Description = nameof(Description);
        public const string Destinations = nameof(Destinations);
        public const string Enabled = nameof(Enabled);
        public const string Guid = nameof(Guid);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string Link = nameof(Link);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PubDate = nameof(PubDate);
        public const string RiskRatingID = nameof(RiskRatingID);
        public const string Title = nameof(Title);
        public const string WarningCoverage = nameof(WarningCoverage);
        public const string WarningDescription = nameof(WarningDescription);
        public const string WarningLevel = nameof(WarningLevel);
    }
}