using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_ItineraryCustomerPhone
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsMobile = nameof(IsMobile);
        public const string ItineraryCustomerId = nameof(ItineraryCustomerId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Phone = nameof(Phone);
        public const string Source = nameof(Source);
    }
}