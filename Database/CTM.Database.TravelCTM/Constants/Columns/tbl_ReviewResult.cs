using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_ReviewResult
    {
        public const string Comments = nameof(Comments);
        public const string CompanyIdentifier = nameof(CompanyIdentifier);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Enabled = nameof(Enabled);
        public const string HotelCity = nameof(HotelCity);
        public const string HotelCountryCode = nameof(HotelCountryCode);
        public const string HotelID = nameof(HotelID);
        public const string HotelName = nameof(HotelName);
        public const string HotelSabreID = nameof(HotelSabreID);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OverallRating = nameof(OverallRating);
        public const string ReviewStatusId = nameof(ReviewStatusId);
        public const string Source = nameof(Source);
        public const string Title = nameof(Title);
        public const string UserFullName = nameof(UserFullName);
        public const string UserIPAddress = nameof(UserIPAddress);
    }
}