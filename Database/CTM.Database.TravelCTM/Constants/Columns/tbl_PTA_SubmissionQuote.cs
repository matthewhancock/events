using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_SubmissionQuote
    {
        public const string Canx = nameof(Canx);
        public const string Changes = nameof(Changes);
        public const string Comment = nameof(Comment);
        public const string ConsultantQuoteName = nameof(ConsultantQuoteName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string Enabled = nameof(Enabled);
        public const string FareClass = nameof(FareClass);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsFromConsultant = nameof(IsFromConsultant);
        public const string IsProcessed = nameof(IsProcessed);
        public const string IsUserSelected = nameof(IsUserSelected);
        public const string ItineraryId = nameof(ItineraryId);
        public const string LostSavings = nameof(LostSavings);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PNR = nameof(PNR);
        public const string PTASubmissionPaxId = nameof(PTASubmissionPaxId);
        public const string PTASubmissionQuoteStatusId = nameof(PTASubmissionQuoteStatusId);
        public const string PTASubmissionQuoteStatusTitle = nameof(PTASubmissionQuoteStatusTitle);
        public const string TotalAmount = nameof(TotalAmount);
        public const string Ttl = nameof(Ttl);
        public const string Visas = nameof(Visas);
    }
}