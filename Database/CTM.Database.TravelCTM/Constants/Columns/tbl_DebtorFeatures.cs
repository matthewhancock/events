using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_DebtorFeatures
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string EnableBookingsClientManager = nameof(EnableBookingsClientManager);
        public const string EnableBookingsTravelArranger = nameof(EnableBookingsTravelArranger);
        public const string EnableBookingsTraveller = nameof(EnableBookingsTraveller);
        public const string EnableConcierge = nameof(EnableConcierge);
        public const string Enabled = nameof(Enabled);
        public const string EnableProofOfLife = nameof(EnableProofOfLife);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string ProofOfLifeEmails = nameof(ProofOfLifeEmails);
    }
}