using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class MaintenanceLog
    {
        public const string FinishDate = nameof(FinishDate);
        public const string Id = nameof(Id);
        public const string Process = nameof(Process);
        public const string StartDate = nameof(StartDate);
    }
}