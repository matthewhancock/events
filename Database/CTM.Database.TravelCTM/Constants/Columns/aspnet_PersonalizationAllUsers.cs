using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class aspnet_PersonalizationAllUsers
    {
        public const string LastUpdatedDate = nameof(LastUpdatedDate);
        public const string PageSettings = nameof(PageSettings);
        public const string PathId = nameof(PathId);
    }
}