using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_ItineraryPassengerFlight
    {
        public const string Amount = nameof(Amount);
        public const string ArrivalDateTime = nameof(ArrivalDateTime);
        public const string Carrier = nameof(Carrier);
        public const string CheapestFlightAmount = nameof(CheapestFlightAmount);
        public const string CheapestFlightArrivalDateTime = nameof(CheapestFlightArrivalDateTime);
        public const string CheapestFlightCarrier = nameof(CheapestFlightCarrier);
        public const string CheapestFlightDepartureDateTime = nameof(CheapestFlightDepartureDateTime);
        public const string CheapestFlightReference = nameof(CheapestFlightReference);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DepartureDateTime = nameof(DepartureDateTime);
        public const string Enabled = nameof(Enabled);
        public const string FareBasis = nameof(FareBasis);
        public const string FlightBenchmarkAmount = nameof(FlightBenchmarkAmount);
        public const string FlightBenchmarkArrivalDateTime = nameof(FlightBenchmarkArrivalDateTime);
        public const string FlightBenchmarkCarrier = nameof(FlightBenchmarkCarrier);
        public const string FlightBenchmarkDepartureDateTime = nameof(FlightBenchmarkDepartureDateTime);
        public const string FlightBenchmarkReference = nameof(FlightBenchmarkReference);
        public const string FlightNumber = nameof(FlightNumber);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryPassengerId = nameof(ItineraryPassengerId);
        public const string Location = nameof(Location);
        public const string LocationCode = nameof(LocationCode);
        public const string LostSavingsAmount = nameof(LostSavingsAmount);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginLocation = nameof(OriginLocation);
        public const string OriginLocationCode = nameof(OriginLocationCode);
        public const string PNR = nameof(PNR);
        public const string PolicyExceptionCode = nameof(PolicyExceptionCode);
        public const string PolicyExceptionComments = nameof(PolicyExceptionComments);
        public const string PolicyExceptionTitle = nameof(PolicyExceptionTitle);
        public const string Quantity = nameof(Quantity);
        public const string RateCode = nameof(RateCode);
        public const string ReturnFlight = nameof(ReturnFlight);
        public const string SabreFlightReservationXml = nameof(SabreFlightReservationXml);
        public const string SabreFlightSegmentXml = nameof(SabreFlightSegmentXml);
        public const string StandardFlightAmount = nameof(StandardFlightAmount);
        public const string StandardFlightArrivalDateTime = nameof(StandardFlightArrivalDateTime);
        public const string StandardFlightCarrier = nameof(StandardFlightCarrier);
        public const string StandardFlightDepartureDateTime = nameof(StandardFlightDepartureDateTime);
        public const string StandardFlightReference = nameof(StandardFlightReference);
        public const string StatusId = nameof(StatusId);
        public const string TaxAmount = nameof(TaxAmount);
    }
}