using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_Submission
    {
        public const string CarRequired = nameof(CarRequired);
        public const string CompletedOn = nameof(CompletedOn);
        public const string CompletedOnOffset = nameof(CompletedOnOffset);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Destinations = nameof(Destinations);
        public const string Enabled = nameof(Enabled);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string FlightRequired = nameof(FlightRequired);
        public const string FollowUpNotificationCount = nameof(FollowUpNotificationCount);
        public const string FollowUpNotificationOn = nameof(FollowUpNotificationOn);
        public const string FollowUpNotificationOnOffset = nameof(FollowUpNotificationOnOffset);
        public const string HotelRequired = nameof(HotelRequired);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsAutoDecline = nameof(IsAutoDecline);
        public const string IsAutoDeclineProcessed = nameof(IsAutoDeclineProcessed);
        public const string IsBookingQuoteRequired = nameof(IsBookingQuoteRequired);
        public const string IsCancelRequestNotificationSent = nameof(IsCancelRequestNotificationSent);
        public const string IsConsultantNotificationSent = nameof(IsConsultantNotificationSent);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsMoreInformationNotificationSent = nameof(IsMoreInformationNotificationSent);
        public const string IsNonConsultantQuote = nameof(IsNonConsultantQuote);
        public const string IsNonConsultantQuoteProcessed = nameof(IsNonConsultantQuoteProcessed);
        public const string IsRequestForApprovalNotificationSent = nameof(IsRequestForApprovalNotificationSent);
        public const string IsRequoteNotificationSent = nameof(IsRequoteNotificationSent);
        public const string IsTravellerPendingQuoteSelectionSent = nameof(IsTravellerPendingQuoteSelectionSent);
        public const string LastApprover = nameof(LastApprover);
        public const string LastApproverId = nameof(LastApproverId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PaxRolesAllowApprovalByPass = nameof(PaxRolesAllowApprovalByPass);
        public const string PolicyAllowsApprovalByPass = nameof(PolicyAllowsApprovalByPass);
        public const string ProjectCode = nameof(ProjectCode);
        public const string PTAPolicyId = nameof(PTAPolicyId);
        public const string ReasonForTravel = nameof(ReasonForTravel);
        public const string SerkoPnr = nameof(SerkoPnr);
        public const string SerkoProfileCode = nameof(SerkoProfileCode);
        public const string SubmissionDebtorId = nameof(SubmissionDebtorId);
        public const string SubmissionStatusId = nameof(SubmissionStatusId);
        public const string SubmissionUserId = nameof(SubmissionUserId);
        public const string Title = nameof(Title);
        public const string TravelArrangementsComments = nameof(TravelArrangementsComments);
    }
}