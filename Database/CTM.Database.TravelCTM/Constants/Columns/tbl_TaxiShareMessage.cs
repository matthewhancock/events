using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_TaxiShareMessage
    {
        public const string Airline = nameof(Airline);
        public const string ArrivalDateTime = nameof(ArrivalDateTime);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Destination = nameof(Destination);
        public const string Email = nameof(Email);
        public const string EmailMessage = nameof(EmailMessage);
        public const string EmailSent = nameof(EmailSent);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsCarRentalBooked = nameof(IsCarRentalBooked);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryItemId = nameof(ItineraryItemId);
        public const string MessageSent = nameof(MessageSent);
        public const string Mobile = nameof(Mobile);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginCountryName = nameof(OriginCountryName);
        public const string PNR = nameof(PNR);
        public const string Reference = nameof(Reference);
        public const string SMSMessage = nameof(SMSMessage);
        public const string SMSSent = nameof(SMSSent);
        public const string Terminal = nameof(Terminal);
        public const string Title = nameof(Title);
    }
}