using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_aspnet_Users
    {
        public const string ApplicationId = nameof(ApplicationId);
        public const string IsAnonymous = nameof(IsAnonymous);
        public const string LastActivityDate = nameof(LastActivityDate);
        public const string LoweredUserName = nameof(LoweredUserName);
        public const string MobileAlias = nameof(MobileAlias);
        public const string UserId = nameof(UserId);
        public const string UserName = nameof(UserName);
    }
}