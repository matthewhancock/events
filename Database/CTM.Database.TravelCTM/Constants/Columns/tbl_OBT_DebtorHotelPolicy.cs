using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_DebtorHotelPolicy
    {
        public const string ChainCodes = nameof(ChainCodes);
        public const string CountryCode = nameof(CountryCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string Enabled = nameof(Enabled);
        public const string HotelCodes = nameof(HotelCodes);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string LocationCode = nameof(LocationCode);
        public const string MaximumNightlyPrice = nameof(MaximumNightlyPrice);
        public const string MinimumNightlyPrice = nameof(MinimumNightlyPrice);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
    }
}