using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_BusinessIntelligence_ReportData
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PeriodId = nameof(PeriodId);
        public const string PeriodTitle = nameof(PeriodTitle);
        public const string ReportData = nameof(ReportData);
        public const string ReportId = nameof(ReportId);
        public const string SortOrder = nameof(SortOrder);
        public const string SpendTypeId = nameof(SpendTypeId);
        public const string SpendTypeTitle = nameof(SpendTypeTitle);
        public const string Title = nameof(Title);
        public const string ValidFrom = nameof(ValidFrom);
        public const string ValidTo = nameof(ValidTo);
    }
}