using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_DebtorFlightBenchmark
    {
        public const string CarrierCode = nameof(CarrierCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Enabled = nameof(Enabled);
        public const string FareBasis = nameof(FareBasis);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
    }
}