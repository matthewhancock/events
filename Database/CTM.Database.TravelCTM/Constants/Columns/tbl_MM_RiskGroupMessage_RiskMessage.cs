using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_MM_RiskGroupMessage_RiskMessage
    {
        public const string group_message_id = nameof(group_message_id);
        public const string id = nameof(id);
        public const string risk_message_id = nameof(risk_message_id);
    }
}