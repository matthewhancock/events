using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_SubmissionApprover
    {
        public const string ApproverTierSort = nameof(ApproverTierSort);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PTAApproverId = nameof(PTAApproverId);
        public const string PTASubmissionId = nameof(PTASubmissionId);
        public const string PTASubmissionPaxId = nameof(PTASubmissionPaxId);
        public const string StatusId = nameof(StatusId);
        public const string StatusText = nameof(StatusText);
    }
}