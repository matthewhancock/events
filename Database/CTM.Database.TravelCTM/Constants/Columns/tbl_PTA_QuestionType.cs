using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_QuestionType
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DisplayInQAndA = nameof(DisplayInQAndA);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsChildAvailable = nameof(IsChildAvailable);
        public const string IsChildOnly = nameof(IsChildOnly);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsFixed = nameof(IsFixed);
        public const string IsParentOnly = nameof(IsParentOnly);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Title = nameof(Title);
        public const string ViewName = nameof(ViewName);
    }
}