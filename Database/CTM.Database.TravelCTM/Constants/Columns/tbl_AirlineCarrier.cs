using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_AirlineCarrier
    {
        public const string BookableFlightNumbers = nameof(BookableFlightNumbers);
        public const string CheckInUrl = nameof(CheckInUrl);
        public const string Code = nameof(Code);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string FareRuleUrl = nameof(FareRuleUrl);
        public const string Id = nameof(Id);
        public const string Image = nameof(Image);
        public const string IsDeleted = nameof(IsDeleted);
        public const string MediumImage = nameof(MediumImage);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Title = nameof(Title);
    }
}