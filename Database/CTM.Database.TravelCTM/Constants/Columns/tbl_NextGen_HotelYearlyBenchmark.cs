using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_HotelYearlyBenchmark
    {
        public const string BenchmarkLevelHigh = nameof(BenchmarkLevelHigh);
        public const string BenchmarkLevelLow = nameof(BenchmarkLevelLow);
        public const string BuildEndDateTime = nameof(BuildEndDateTime);
        public const string BuildStartDateTime = nameof(BuildStartDateTime);
        public const string CountryCode = nameof(CountryCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string CurrencyCode = nameof(CurrencyCode);
        public const string Day = nameof(Day);
        public const string HotelCityCode = nameof(HotelCityCode);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PCC = nameof(PCC);
    }
}