using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Advertisement
    {
        public const string AdvertisementCategoryId = nameof(AdvertisementCategoryId);
        public const string AdvertisementImage = nameof(AdvertisementImage);
        public const string AdvertisementUrl = nameof(AdvertisementUrl);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string Debtors = nameof(Debtors);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Title = nameof(Title);
    }
}