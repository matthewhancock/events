using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_DebtorFlightPolicy
    {
        public const string BookingCodes = nameof(BookingCodes);
        public const string Carrier = nameof(Carrier);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string DestinationLocationCode = nameof(DestinationLocationCode);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginLocationCode = nameof(OriginLocationCode);
    }
}