using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_SubmissionAnswer
    {
        public const string Answer = nameof(Answer);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PTASubmissionDestinationId = nameof(PTASubmissionDestinationId);
        public const string PTASubmissionPaxId = nameof(PTASubmissionPaxId);
        public const string PTASubmissionQuestionId = nameof(PTASubmissionQuestionId);
        public const string Title = nameof(Title);
    }
}