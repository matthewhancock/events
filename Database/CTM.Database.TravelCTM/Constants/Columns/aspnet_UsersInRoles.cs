using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class aspnet_UsersInRoles
    {
        public const string RoleId = nameof(RoleId);
        public const string UserId = nameof(UserId);
    }
}