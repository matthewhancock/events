using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_tbl_DebtorPaxSettings
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string Email = nameof(Email);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string Mobile = nameof(Mobile);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string ReviewBatchEmailEnabled = nameof(ReviewBatchEmailEnabled);
        public const string RiskEngineEmailEnabled = nameof(RiskEngineEmailEnabled);
        public const string RiskEngineSMSEnabled = nameof(RiskEngineSMSEnabled);
        public const string TaxiShareEmailEnabled = nameof(TaxiShareEmailEnabled);
        public const string TaxiShareMobileNumbersShown = nameof(TaxiShareMobileNumbersShown);
        public const string TaxiShareSMSEnabled = nameof(TaxiShareSMSEnabled);
        public const string Title = nameof(Title);
    }
}