using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_ItineraryPassengerHotel
    {
        public const string Amount = nameof(Amount);
        public const string ChainCode = nameof(ChainCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string Enabled = nameof(Enabled);
        public const string HotelCode = nameof(HotelCode);
        public const string HotelName = nameof(HotelName);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryPassengerId = nameof(ItineraryPassengerId);
        public const string Location = nameof(Location);
        public const string LocationCode = nameof(LocationCode);
        public const string LostSavingsAmount = nameof(LostSavingsAmount);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PNR = nameof(PNR);
        public const string PolicyExceptionCode = nameof(PolicyExceptionCode);
        public const string PolicyExceptionComments = nameof(PolicyExceptionComments);
        public const string PolicyExceptionTitle = nameof(PolicyExceptionTitle);
        public const string Quantity = nameof(Quantity);
        public const string RateCode = nameof(RateCode);
        public const string RoomName = nameof(RoomName);
        public const string SabreHotelReservationXml = nameof(SabreHotelReservationXml);
        public const string SpecialRequirements = nameof(SpecialRequirements);
        public const string StatusId = nameof(StatusId);
    }
}