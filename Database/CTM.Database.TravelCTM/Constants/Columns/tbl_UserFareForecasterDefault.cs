using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_UserFareForecasterDefault
    {
        public const string AirlineCarrierCode = nameof(AirlineCarrierCode);
        public const string CabinId = nameof(CabinId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DepartureDate = nameof(DepartureDate);
        public const string DepartureIataCode = nameof(DepartureIataCode);
        public const string DepartureTimeslotId = nameof(DepartureTimeslotId);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IncludeBaggage = nameof(IncludeBaggage);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OneWayOrReturn = nameof(OneWayOrReturn);
        public const string ReturnDate = nameof(ReturnDate);
        public const string ReturnIataCode = nameof(ReturnIataCode);
        public const string ReturnTimeslotId = nameof(ReturnTimeslotId);
        public const string StopId = nameof(StopId);
        public const string UserId = nameof(UserId);
    }
}