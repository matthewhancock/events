using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_ItineraryItemRiskGroupMessage
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string Email = nameof(Email);
        public const string EmailSent = nameof(EmailSent);
        public const string Enabled = nameof(Enabled);
        public const string GroupMessageTypeId = nameof(GroupMessageTypeId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string RiskMessageId = nameof(RiskMessageId);
    }
}