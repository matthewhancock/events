using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_ScheduledService
    {
        public const string Concurrent = nameof(Concurrent);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string ProcessEndTime = nameof(ProcessEndTime);
        public const string Processing = nameof(Processing);
        public const string ProcessingMaxMinutes = nameof(ProcessingMaxMinutes);
        public const string ProcessStartTime = nameof(ProcessStartTime);
        public const string ServiceAction = nameof(ServiceAction);
        public const string ServiceName = nameof(ServiceName);
        public const string Title = nameof(Title);
    }
}