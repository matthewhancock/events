using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Smart_MenuItem
    {
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string FeatureId = nameof(FeatureId);
        public const string FeatureIdentifier = nameof(FeatureIdentifier);
        public const string Href = nameof(Href);
        public const string Icon = nameof(Icon);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string Label = nameof(Label);
        public const string LegacyIdentifier = nameof(LegacyIdentifier);
        public const string MenuId = nameof(MenuId);
        public const string ModifiedOn = nameof(ModifiedOn);
    }
}