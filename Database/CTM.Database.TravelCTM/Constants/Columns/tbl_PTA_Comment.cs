using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_Comment
    {
        public const string comment = nameof(comment);
        public const string id = nameof(id);
        public const string submission_id = nameof(submission_id);
        public const string unix = nameof(unix);
        public const string userId = nameof(userId);
        public const string username = nameof(username);
    }
}