using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_FareForecasterItemItem
    {
        public const string BuildEndDateTime = nameof(BuildEndDateTime);
        public const string BuildStartDateTime = nameof(BuildStartDateTime);
        public const string Carrier = nameof(Carrier);
        public const string CorporateId = nameof(CorporateId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string CurrencyCode = nameof(CurrencyCode);
        public const string FareAmount = nameof(FareAmount);
        public const string FinalLegFlightNumber = nameof(FinalLegFlightNumber);
        public const string FlightArrivalDate = nameof(FlightArrivalDate);
        public const string FlightDepartureDate = nameof(FlightDepartureDate);
        public const string FlightNumber = nameof(FlightNumber);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsDirectFlight = nameof(IsDirectFlight);
        public const string JetStarSourceOrganization = nameof(JetStarSourceOrganization);
        public const string LocationCode = nameof(LocationCode);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string NextGenFareForecasterItemId = nameof(NextGenFareForecasterItemId);
        public const string NextGenFareForecasterTimeslotId = nameof(NextGenFareForecasterTimeslotId);
        public const string OriginLocationCode = nameof(OriginLocationCode);
        public const string Pcc = nameof(Pcc);
        public const string ProcessId = nameof(ProcessId);
        public const string ProcessName = nameof(ProcessName);
    }
}