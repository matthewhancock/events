using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_FlightThresholdMessage
    {
        public const string Airline = nameof(Airline);
        public const string CityName = nameof(CityName);
        public const string CountryName = nameof(CountryName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string DebtorId = nameof(DebtorId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string EmailMessage = nameof(EmailMessage);
        public const string EmailSent = nameof(EmailSent);
        public const string EmployeeTypeCode = nameof(EmployeeTypeCode);
        public const string Enabled = nameof(Enabled);
        public const string FlightReference = nameof(FlightReference);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string LocationCode = nameof(LocationCode);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginCityName = nameof(OriginCityName);
        public const string OriginCountryName = nameof(OriginCountryName);
        public const string OriginLocationCode = nameof(OriginLocationCode);
        public const string OriginTerminal = nameof(OriginTerminal);
        public const string PaxCount = nameof(PaxCount);
        public const string Terminal = nameof(Terminal);
        public const string ThresholdLimit = nameof(ThresholdLimit);
        public const string Title = nameof(Title);
    }
}