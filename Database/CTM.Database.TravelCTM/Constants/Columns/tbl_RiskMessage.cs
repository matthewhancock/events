using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_RiskMessage
    {
        public const string ContentText = nameof(ContentText);
        public const string Country = nameof(Country);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Destinations = nameof(Destinations);
        public const string EmailSubject = nameof(EmailSubject);
        public const string Enabled = nameof(Enabled);
        public const string FormattedEmailMessage = nameof(FormattedEmailMessage);
        public const string FormattedSMSMessage = nameof(FormattedSMSMessage);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsProcessed = nameof(IsProcessed);
        public const string IsSent = nameof(IsSent);
        public const string IsTwitterPosted = nameof(IsTwitterPosted);
        public const string MessageDate = nameof(MessageDate);
        public const string MessageID = nameof(MessageID);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string RegionCodes = nameof(RegionCodes);
        public const string RegionNames = nameof(RegionNames);
        public const string RiskDate = nameof(RiskDate);
        public const string RiskProviderId = nameof(RiskProviderId);
        public const string RiskRatingID = nameof(RiskRatingID);
        public const string Source = nameof(Source);
        public const string Title = nameof(Title);
        public const string ViewerShortUrl = nameof(ViewerShortUrl);
        public const string ViewerShortUrlHash = nameof(ViewerShortUrlHash);
    }
}