using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_QuestionLogicCommand
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string LogicId = nameof(LogicId);
        public const string LogicTitle = nameof(LogicTitle);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OperatorId = nameof(OperatorId);
        public const string OperatorTitle = nameof(OperatorTitle);
        public const string Property = nameof(Property);
        public const string PTAQuestionLogicId = nameof(PTAQuestionLogicId);
        public const string Sort = nameof(Sort);
        public const string Value = nameof(Value);
    }
}