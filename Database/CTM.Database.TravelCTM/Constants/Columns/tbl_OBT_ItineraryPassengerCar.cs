using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_ItineraryPassengerCar
    {
        public const string Amount = nameof(Amount);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string CurrencyCode = nameof(CurrencyCode);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryPassengerId = nameof(ItineraryPassengerId);
        public const string LostSavingsAmount = nameof(LostSavingsAmount);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PickupLocation = nameof(PickupLocation);
        public const string PickupLocationCode = nameof(PickupLocationCode);
        public const string PNR = nameof(PNR);
        public const string PolicyExceptionCode = nameof(PolicyExceptionCode);
        public const string PolicyExceptionComments = nameof(PolicyExceptionComments);
        public const string PolicyExceptionTitle = nameof(PolicyExceptionTitle);
        public const string Quantity = nameof(Quantity);
        public const string RateCode = nameof(RateCode);
        public const string ReturnLocation = nameof(ReturnLocation);
        public const string ReturnLocationCode = nameof(ReturnLocationCode);
        public const string SabreCarReservationXml = nameof(SabreCarReservationXml);
        public const string SabreCarSegmentXml = nameof(SabreCarSegmentXml);
        public const string StatusId = nameof(StatusId);
        public const string Supplier = nameof(Supplier);
        public const string SupplierCode = nameof(SupplierCode);
        public const string Vehicle = nameof(Vehicle);
        public const string VehicleClass = nameof(VehicleClass);
        public const string VehicleCode = nameof(VehicleCode);
    }
}