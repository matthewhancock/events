using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_SubmissionPax
    {
        public const string BusIntEmployeeCode = nameof(BusIntEmployeeCode);
        public const string CheckedBaggage = nameof(CheckedBaggage);
        public const string ClientId = nameof(ClientId);
        public const string CostCentreCode = nameof(CostCentreCode);
        public const string CostCentreName = nameof(CostCentreName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DepartmentCode = nameof(DepartmentCode);
        public const string DepartmentName = nameof(DepartmentName);
        public const string Email = nameof(Email);
        public const string EmployeeCode = nameof(EmployeeCode);
        public const string Enabled = nameof(Enabled);
        public const string FirstName = nameof(FirstName);
        public const string FlightClass = nameof(FlightClass);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string LastName = nameof(LastName);
        public const string Mobile = nameof(Mobile);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string ProfileName = nameof(ProfileName);
        public const string PTALastApproverId = nameof(PTALastApproverId);
        public const string PTASubmissionId = nameof(PTASubmissionId);
        public const string PTASubmissionPaxStatusId = nameof(PTASubmissionPaxStatusId);
        public const string PTASubmissionPaxStatusTitle = nameof(PTASubmissionPaxStatusTitle);
        public const string TravellerTitle = nameof(TravellerTitle);
    }
}