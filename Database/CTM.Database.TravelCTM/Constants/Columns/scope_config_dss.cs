using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class scope_config_dss
    {
        public const string config_data = nameof(config_data);
        public const string config_id = nameof(config_id);
        public const string scope_status = nameof(scope_status);
    }
}