using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_aspnet_Applications
    {
        public const string ApplicationId = nameof(ApplicationId);
        public const string ApplicationName = nameof(ApplicationName);
        public const string Description = nameof(Description);
        public const string LoweredApplicationName = nameof(LoweredApplicationName);
    }
}