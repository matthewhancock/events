using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_ItineraryItem
    {
        public const string Address = nameof(Address);
        public const string CityName = nameof(CityName);
        public const string CountryName = nameof(CountryName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string Debtor = nameof(Debtor);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItemCode = nameof(ItemCode);
        public const string ItineraryId = nameof(ItineraryId);
        public const string ItineraryTypeId = nameof(ItineraryTypeId);
        public const string LocationCode = nameof(LocationCode);
        public const string MarketingCode = nameof(MarketingCode);
        public const string MarketingFlightNumber = nameof(MarketingFlightNumber);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginCityName = nameof(OriginCityName);
        public const string OriginCountryName = nameof(OriginCountryName);
        public const string OriginLocationCode = nameof(OriginLocationCode);
        public const string OriginTerminal = nameof(OriginTerminal);
        public const string PCC = nameof(PCC);
        public const string Phone = nameof(Phone);
        public const string PNR = nameof(PNR);
        public const string Reference = nameof(Reference);
        public const string RequestOrder = nameof(RequestOrder);
        public const string ReservationItemXml = nameof(ReservationItemXml);
        public const string ReviewBatchEmailDateSent = nameof(ReviewBatchEmailDateSent);
        public const string ReviewBatchEmailSent = nameof(ReviewBatchEmailSent);
        public const string RiskEngineMessagesEmailSent = nameof(RiskEngineMessagesEmailSent);
        public const string RiskEngineMessagesSMSSent = nameof(RiskEngineMessagesSMSSent);
        public const string SeatNumber = nameof(SeatNumber);
        public const string Status = nameof(Status);
        public const string StopQuantity = nameof(StopQuantity);
        public const string SupplierCode = nameof(SupplierCode);
        public const string SupplierName = nameof(SupplierName);
        public const string SupplierReference = nameof(SupplierReference);
        public const string TaxiShareMessageCreated = nameof(TaxiShareMessageCreated);
        public const string TaxiShareMessageDateSent = nameof(TaxiShareMessageDateSent);
        public const string TaxiShareMessageSent = nameof(TaxiShareMessageSent);
        public const string Terminal = nameof(Terminal);
    }
}