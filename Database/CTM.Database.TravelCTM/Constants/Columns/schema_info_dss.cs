using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class schema_info_dss
    {
        public const string schema_extended_info = nameof(schema_extended_info);
        public const string schema_major_version = nameof(schema_major_version);
        public const string schema_minor_version = nameof(schema_minor_version);
    }
}