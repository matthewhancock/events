using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_IataCode
    {
        public const string Altitude = nameof(Altitude);
        public const string City = nameof(City);
        public const string Code = nameof(Code);
        public const string Country = nameof(Country);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string Latitude = nameof(Latitude);
        public const string Longitude = nameof(Longitude);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string TimeZone = nameof(TimeZone);
        public const string TimeZoneOffset = nameof(TimeZoneOffset);
        public const string Title = nameof(Title);
    }
}