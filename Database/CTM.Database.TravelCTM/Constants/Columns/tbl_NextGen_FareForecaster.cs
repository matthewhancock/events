using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_FareForecaster
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Destination = nameof(Destination);
        public const string DestinationCountry = nameof(DestinationCountry);
        public const string Enabled = nameof(Enabled);
        public const string FilterId = nameof(FilterId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Origin = nameof(Origin);
        public const string OriginCountry = nameof(OriginCountry);
    }
}