using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Smart_FeatureValue
    {
        public const string DebtorId = nameof(DebtorId);
        public const string Enabled = nameof(Enabled);
        public const string FeatureOptionId = nameof(FeatureOptionId);
        public const string Id = nameof(Id);
        public const string RoleId = nameof(RoleId);
        public const string Value = nameof(Value);
    }
}