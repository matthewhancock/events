using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class aspnet_Paths
    {
        public const string ApplicationId = nameof(ApplicationId);
        public const string LoweredPath = nameof(LoweredPath);
        public const string Path = nameof(Path);
        public const string PathId = nameof(PathId);
    }
}