using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Concur_ReceiptEmailUpload
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string TramadaCardHolder = nameof(TramadaCardHolder);
        public const string TramadaCardNumberDisplay = nameof(TramadaCardNumberDisplay);
        public const string TramadaClientId = nameof(TramadaClientId);
        public const string TramadaCreditCardId = nameof(TramadaCreditCardId);
        public const string TramadaEmail = nameof(TramadaEmail);
    }
}