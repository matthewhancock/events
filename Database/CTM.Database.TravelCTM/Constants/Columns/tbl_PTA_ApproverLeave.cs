using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_ApproverLeave
    {
        public const string ApproverId = nameof(ApproverId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Description = nameof(Description);
        public const string Enabled = nameof(Enabled);
        public const string FromDate = nameof(FromDate);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Title = nameof(Title);
        public const string ToDate = nameof(ToDate);
    }
}