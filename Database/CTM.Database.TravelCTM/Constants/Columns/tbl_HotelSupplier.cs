using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_HotelSupplier
    {
        public const string Address = nameof(Address);
        public const string Area = nameof(Area);
        public const string Attractions = nameof(Attractions);
        public const string ChainCode = nameof(ChainCode);
        public const string CheckInInformation = nameof(CheckInInformation);
        public const string CityCode = nameof(CityCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DefaultHotelImage = nameof(DefaultHotelImage);
        public const string Description = nameof(Description);
        public const string Dining = nameof(Dining);
        public const string Directions = nameof(Directions);
        public const string Enabled = nameof(Enabled);
        public const string Facilities = nameof(Facilities);
        public const string HotelFacilities = nameof(HotelFacilities);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string LastModified = nameof(LastModified);
        public const string Latitude = nameof(Latitude);
        public const string Location = nameof(Location);
        public const string Longitude = nameof(Longitude);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string OriginalImageFileName = nameof(OriginalImageFileName);
        public const string PolicyInformation = nameof(PolicyInformation);
        public const string RichMediaScript = nameof(RichMediaScript);
        public const string RoomInformation = nameof(RoomInformation);
        public const string SabreCode = nameof(SabreCode);
        public const string SerkoPropertyCode = nameof(SerkoPropertyCode);
        public const string Title = nameof(Title);
    }
}