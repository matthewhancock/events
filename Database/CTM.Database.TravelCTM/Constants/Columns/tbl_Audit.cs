using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Audit
    {
        public const string ActionType = nameof(ActionType);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string NewValue = nameof(NewValue);
        public const string Notes = nameof(Notes);
        public const string ObjectId = nameof(ObjectId);
        public const string ObjectName = nameof(ObjectName);
        public const string OldValue = nameof(OldValue);
        public const string Property = nameof(Property);
    }
}