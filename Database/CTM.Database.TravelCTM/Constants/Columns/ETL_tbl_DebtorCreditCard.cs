using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class ETL_tbl_DebtorCreditCard
    {
        public const string BiCreditCardId = nameof(BiCreditCardId);
        public const string Ccv = nameof(Ccv);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorId = nameof(DebtorId);
        public const string DisplayNumber = nameof(DisplayNumber);
        public const string Enabled = nameof(Enabled);
        public const string EncryptedNumber = nameof(EncryptedNumber);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string HolderName = nameof(HolderName);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
    }
}