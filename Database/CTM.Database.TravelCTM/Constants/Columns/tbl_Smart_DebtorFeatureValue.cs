using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Smart_DebtorFeatureValue
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string ElementId = nameof(ElementId);
        public const string Enabled = nameof(Enabled);
        public const string FeatureIdentifier = nameof(FeatureIdentifier);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string Key = nameof(Key);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Type = nameof(Type);
        public const string Value = nameof(Value);
    }
}