using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_PTA_Submission_dss_tracking
    {
        public const string create_scope_local_id = nameof(create_scope_local_id);
        public const string Id = nameof(Id);
        public const string last_change_datetime = nameof(last_change_datetime);
        public const string local_create_peer_key = nameof(local_create_peer_key);
        public const string local_create_peer_timestamp = nameof(local_create_peer_timestamp);
        public const string local_update_peer_key = nameof(local_update_peer_key);
        public const string local_update_peer_timestamp = nameof(local_update_peer_timestamp);
        public const string scope_create_peer_key = nameof(scope_create_peer_key);
        public const string scope_create_peer_timestamp = nameof(scope_create_peer_timestamp);
        public const string scope_update_peer_key = nameof(scope_update_peer_key);
        public const string scope_update_peer_timestamp = nameof(scope_update_peer_timestamp);
        public const string sync_row_is_tombstone = nameof(sync_row_is_tombstone);
        public const string update_scope_local_id = nameof(update_scope_local_id);
    }
}