using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_BedTracker
    {
        public const string BuildEndDateTime = nameof(BuildEndDateTime);
        public const string BuildStartDateTime = nameof(BuildStartDateTime);
        public const string CountryCode = nameof(CountryCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string CurrencyCode = nameof(CurrencyCode);
        public const string Enabled = nameof(Enabled);
        public const string HotelCityCode = nameof(HotelCityCode);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PCC = nameof(PCC);
        public const string Title = nameof(Title);
    }
}