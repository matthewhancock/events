using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_OBT_ItineraryPassengerFlightTicket
    {
        public const string BookingCode = nameof(BookingCode);
        public const string Carrier = nameof(Carrier);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string GdsRecordLocator = nameof(GdsRecordLocator);
        public const string Id = nameof(Id);
        public const string Identifier = nameof(Identifier);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ItineraryPassengerId = nameof(ItineraryPassengerId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string TicketAmount = nameof(TicketAmount);
        public const string TicketNumber = nameof(TicketNumber);
    }
}