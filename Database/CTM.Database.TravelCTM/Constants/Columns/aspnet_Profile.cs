using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class aspnet_Profile
    {
        public const string LastUpdatedDate = nameof(LastUpdatedDate);
        public const string PropertyNames = nameof(PropertyNames);
        public const string PropertyValuesBinary = nameof(PropertyValuesBinary);
        public const string PropertyValuesString = nameof(PropertyValuesString);
        public const string UserId = nameof(UserId);
    }
}