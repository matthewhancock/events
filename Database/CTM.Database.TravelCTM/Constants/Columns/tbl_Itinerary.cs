using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_Itinerary
    {
        public const string BiTravelArrangerEmail = nameof(BiTravelArrangerEmail);
        public const string BiTravelArrangerId = nameof(BiTravelArrangerId);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateFrom = nameof(DateFrom);
        public const string DateTo = nameof(DateTo);
        public const string Debtor = nameof(Debtor);
        public const string DebtorId = nameof(DebtorId);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string PCC = nameof(PCC);
        public const string PNR = nameof(PNR);
        public const string RiskEngineMessagesEmailSent = nameof(RiskEngineMessagesEmailSent);
        public const string RiskEngineMessagesSMSSent = nameof(RiskEngineMessagesSMSSent);
        public const string TravelItineraryXml = nameof(TravelItineraryXml);
    }
}