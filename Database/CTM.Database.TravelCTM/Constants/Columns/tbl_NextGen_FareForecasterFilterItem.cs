using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables.Constants.Columns
{
    public static class tbl_NextGen_FareForecasterFilterItem
    {
        public const string Carrier = nameof(Carrier);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string FareForecasterFilterId = nameof(FareForecasterFilterId);
        public const string FlightNumberFrom = nameof(FlightNumberFrom);
        public const string FlightNumberTo = nameof(FlightNumberTo);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
    }
}