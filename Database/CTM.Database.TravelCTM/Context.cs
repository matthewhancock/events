﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM
{
    public class Context : DbContext
    {
        private readonly IConfiguration _configuration;
        public Context(DbContextOptions<Context> options, IConfiguration configuration = null) : base(options)
        {
            _configuration = configuration;
        }

        public virtual DbSet<Tables.User> Users { get; set; }
        public virtual DbSet<Tables.Debtor> Debtors { get; set; }
        public virtual DbSet<Tables.Role> Roles { get; set; }

        // Asp.Net Auth
        public virtual DbSet<Tables.AspnetRoles> AspnetRoles { get; set; }
        public virtual DbSet<Tables.AspnetUsersInRoles> AspnetUsersInRoles { get; set; }

        // Settings
        public virtual DbSet<Tables.SettingDatum> SettingData { get; set; }

        protected override void OnModelCreating(ModelBuilder ModelBuilder)
        {
            ModelBuilder.ApplyConfiguration(new Configuration.AspnetUsersInRoles());
            ModelBuilder.ApplyConfiguration(new Configuration.User());

            base.OnModelCreating(ModelBuilder);
        }
    }
}
