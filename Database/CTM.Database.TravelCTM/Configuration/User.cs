﻿using CTM.Database.TravelCTM.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Configuration
{
    internal class User : IEntityTypeConfiguration<Tables.User>
    {
        public void Configure(EntityTypeBuilder<Tables.User> builder)
        {
            builder.Property(u => u.NextGenPasswordUpdateDate).HasDefaultValue(DateTime.UtcNow);
        }
    }
}
