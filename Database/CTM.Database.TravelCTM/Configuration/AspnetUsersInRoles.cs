﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Configuration
{
    internal class AspnetUsersInRoles : IEntityTypeConfiguration<Tables.AspnetUsersInRoles>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Tables.AspnetUsersInRoles> Builder)
        {
            Builder.HasKey(e => new { e.RoleId, e.UserId });
        }
    }
}
