using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_TaxiShareMessageItineraryItem)]
    public partial class TaxiShareMessageItineraryItem
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int TaxiShareMessageId { get; set; }
        public int ItineraryItemId { get; set; }
        public string Title { get; set; }
        public string PNR { get; set; }
        public string DebtorIdentifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ArrivalDateTime { get; set; }
        public string Terminal { get; set; }
        public string Airline { get; set; }
        public string AirlineShortCode { get; set; }
        public string Reference { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public bool IsCarRentalBooked { get; set; }
    }
}
