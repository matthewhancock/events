using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_DebtorBiSsoSettings)]
    public partial class DebtorBiSsoSettings
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int DebtorId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int? BIUrl { get; set; }
        public int? BIDocument { get; set; }
        public bool IsDeleted { get; set; }
        public bool Enabled { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
