using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_UserFareForecasterDefault)]
    public partial class UserFareForecasterDefault
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int UserId { get; set; }
        public string DepartureIataCode { get; set; }
        public DateTime? DepartureDate { get; set; }
        public int? DepartureTimeslotId { get; set; }
        public string ReturnIataCode { get; set; }
        public DateTime? ReturnDate { get; set; }
        public int? ReturnTimeslotId { get; set; }
        public string AirlineCarrierCode { get; set; }
        public int? StopId { get; set; }
        public bool? OneWayOrReturn { get; set; }
        public int? CabinId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? Enabled { get; set; }
        public bool? IncludeBaggage { get; set; }
    }
}
