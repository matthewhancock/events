using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_DebtorCreditCard)]
    public partial class DebtorCreditCard
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? DebtorId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? BiCreditCardId { get; set; }
        public string EncryptedNumber { get; set; }
        public string HolderName { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Ccv { get; set; }
        public string DisplayNumber { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
