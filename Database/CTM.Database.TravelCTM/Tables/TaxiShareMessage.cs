using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_TaxiShareMessage)]
    public partial class TaxiShareMessage
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int DebtorId { get; set; }
        public int ItineraryItemId { get; set; }
        public string Title { get; set; }
        public string PNR { get; set; }
        public string DebtorIdentifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ArrivalDateTime { get; set; }
        public string Terminal { get; set; }
        public string Airline { get; set; }
        public string Reference { get; set; }
        public string OriginCountryName { get; set; }
        public string Destination { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string EmailMessage { get; set; }
        public string SMSMessage { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? EmailSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? SMSSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? MessageSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public bool IsCarRentalBooked { get; set; }
    }
}
