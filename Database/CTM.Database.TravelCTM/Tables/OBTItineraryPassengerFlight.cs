using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_OBT_ItineraryPassengerFlight)]
    public partial class OBTItineraryPassengerFlight
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Int64 Id { get; set; }
        public Int64 ItineraryPassengerId { get; set; }
        public int StatusId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        public string Carrier { get; set; }
        public string FlightNumber { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DepartureDateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ArrivalDateTime { get; set; }
        public string RateCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? Amount { get; set; }
        public string LocationCode { get; set; }
        public string Location { get; set; }
        public string OriginLocationCode { get; set; }
        public string OriginLocation { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ReturnFlight { get; set; }
        public string SabreFlightSegmentXml { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? Quantity { get; set; }
        public string SabreFlightReservationXml { get; set; }
        public string PNR { get; set; }
        public Decimal? FlightBenchmarkAmount { get; set; }
        public string FlightBenchmarkCarrier { get; set; }
        public string FlightBenchmarkReference { get; set; }
        public DateTime? FlightBenchmarkDepartureDateTime { get; set; }
        public DateTime? FlightBenchmarkArrivalDateTime { get; set; }
        public Decimal? StandardFlightAmount { get; set; }
        public string StandardFlightCarrier { get; set; }
        public string StandardFlightReference { get; set; }
        public DateTime? StandardFlightDepartureDateTime { get; set; }
        public DateTime? StandardFlightArrivalDateTime { get; set; }
        public Decimal? CheapestFlightAmount { get; set; }
        public string CheapestFlightCarrier { get; set; }
        public string CheapestFlightReference { get; set; }
        public DateTime? CheapestFlightDepartureDateTime { get; set; }
        public DateTime? CheapestFlightArrivalDateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? LostSavingsAmount { get; set; }
        public string PolicyExceptionCode { get; set; }
        public string PolicyExceptionTitle { get; set; }
        public string PolicyExceptionComments { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? TaxAmount { get; set; }
        public string FareBasis { get; set; }
    }
}
