using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.aspnet_PersonalizationAllUsers)]
    public partial class AspnetPersonalizationAllUsers
    {
        [Key] public Guid PathId { get; set; }
        public Byte[] PageSettings { get; set; }
        public DateTime LastUpdatedDate { get; set; }
    }
}
