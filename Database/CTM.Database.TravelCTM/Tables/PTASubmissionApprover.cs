using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_PTA_SubmissionApprover)]
    public partial class PTASubmissionApprover
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int PTASubmissionId { get; set; }
        public int PTASubmissionPaxId { get; set; }
        public int PTAApproverId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ApproverTierSort { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? StatusId { get; set; }
        public string StatusText { get; set; }
    }
}
