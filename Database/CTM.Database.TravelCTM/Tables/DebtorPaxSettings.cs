using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_DebtorPaxSettings)]
    public partial class DebtorPaxSettings
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int DebtorId { get; set; }
        public string Title { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskEngineSMSEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskEngineEmailEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ReviewBatchEmailEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TaxiShareSMSEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TaxiShareEmailEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TaxiShareMobileNumbersShown { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
