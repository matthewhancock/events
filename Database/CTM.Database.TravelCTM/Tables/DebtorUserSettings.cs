using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_DebtorUserSettings)]
    public partial class DebtorUserSettings
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int DebtorId { get; set; }
        public int? UserActiveDays { get; set; }
        public bool DisableUserInactive { get; set; }
        public int? PasswordValidDays { get; set; }
        public bool DisablePasswordExpiry { get; set; }
        public bool IsDeleted { get; set; }
        public bool Enabled { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
