using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_NextGen_FareForecasterItemItem)]
    public partial class NextGenFareForecasterItemItem
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int NextGenFareForecasterItemId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterTimeslotId { get; set; }
        public string OriginLocationCode { get; set; }
        public string LocationCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? FlightArrivalDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? FlightDepartureDate { get; set; }
        public string FlightNumber { get; set; }
        public string Carrier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDirectFlight { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? FareAmount { get; set; }
        public string FinalLegFlightNumber { get; set; }
        public string Pcc { get; set; }
        public string CurrencyCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string CorporateId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string JetStarSourceOrganization { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? BuildStartDateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? BuildEndDateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ProcessId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string ProcessName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
