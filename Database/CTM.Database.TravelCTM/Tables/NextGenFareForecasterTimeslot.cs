using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_NextGen_FareForecasterTimeslot)]
    public partial class NextGenFareForecasterTimeslot
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int? SortOrder { get; set; }
        public string Title { get; set; }
        public int? StartHour { get; set; }
        public int? EndHour { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? Enabled { get; set; }
    }
}
