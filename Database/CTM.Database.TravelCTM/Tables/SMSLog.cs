using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_SMSLog)]
    public partial class SMSLog
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int ProviderId { get; set; }
        public string Message { get; set; }
        public string Mobiles { get; set; }
        public string Country { get; set; }
        public bool? IsSendingSuccessful { get; set; }
        public string ExceptionMessage { get; set; }
        public string ResponseMessage { get; set; }
        public string Reference { get; set; }
        public bool? IsDelivered { get; set; }
        public string CallbackResponseMessage { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        public int? DebtorId { get; set; }
        public string TravellerFullName { get; set; }
        public string Source { get; set; }
    }
}
