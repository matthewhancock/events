using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_Smart_FeatureValue)]
    public partial class SmartFeatureValue
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int FeatureOptionId { get; set; }
        public int DebtorId { get; set; }
        public int RoleId { get; set; }
        public string Value { get; set; }
    }
}
