using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_IataCode)]
    public partial class IataCode
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Code { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Double? Latitude { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Double? Longitude { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? Altitude { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? TimeZone { get; set; }
        public string TimeZoneOffset { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
