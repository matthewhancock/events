using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_SabreNotification)]
    public partial class SabreNotification
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string PNR { get; set; }
        public string PCC { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDownloaded { get; set; }
        public string RequestDataXml { get; set; }
        public string ErrorMessage { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsProcessing { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? ProcessingIdentifier { get; set; }
    }
}
