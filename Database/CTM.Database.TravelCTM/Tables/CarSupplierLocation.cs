using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_CarSupplierLocation)]
    public partial class CarSupplierLocation
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public int CarSupplierId { get; set; }
        public string ExtendedLocationCode { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string NoRateReturned { get; set; }
        public string AddressLine { get; set; }
        public string CityName { get; set; }
        public string StateCode { get; set; }
        public Double? Latitude { get; set; }
        public Double? Longtitude { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
