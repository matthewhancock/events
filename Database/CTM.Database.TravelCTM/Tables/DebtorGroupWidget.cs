using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.DebtorGroupWidget)]
    public partial class DebtorGroupWidget
    {
        [Key] public int DebtorId { get; set; }
        [Key] public string Widget { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string ModifiedBy { get; set; }
        public bool Enabled { get; set; }
        public bool IsDeleted { get; set; }
    }
}
