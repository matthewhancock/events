using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.aspnet_Applications)]
    public partial class AspnetApplications
    {
        [Key] public string ApplicationName { get; set; }
        public string LoweredApplicationName { get; set; }
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? ApplicationId { get; set; }
        public string Description { get; set; }
    }
}
