using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_OBT_ItinerarySearchFlight)]
    public partial class OBTItinerarySearchFlight
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Int64 Id { get; set; }
        public Int64 ItineraryId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        public string OriginLocation { get; set; }
        public string OriginLocationCode { get; set; }
        public string DestinationLocation { get; set; }
        public string DestinationLocationCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? DepartureDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
