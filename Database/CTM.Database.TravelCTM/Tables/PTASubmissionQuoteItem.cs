using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_PTA_SubmissionQuoteItem)]
    public partial class PTASubmissionQuoteItem
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int PTASubmissionQuoteId { get; set; }
        public string Title { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ItineraryItemId { get; set; }
        public string PNR { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateFrom { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateTo { get; set; }
        public string OriginCountryName { get; set; }
        public string OriginCityName { get; set; }
        public string OriginLocationCode { get; set; }
        public string OriginTerminal { get; set; }
        public string LocationCountryName { get; set; }
        public string LocationCityName { get; set; }
        public string LocationCode { get; set; }
        public string LocationTerminal { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ItineraryTypeId { get; set; }
        public string Reference { get; set; }
        public string ItemCode { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string SupplierReference { get; set; }
        public string Address { get; set; }
        public string MarketingCode { get; set; }
        public string MarketingFlightNumber { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RequestOrder { get; set; }
        public string SeatNumber { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? StopQuantity { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? Amount { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string Comments { get; set; }
        public string CodeShareText { get; set; }
    }
}
