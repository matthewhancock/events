using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_user)]
    public partial class User
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Guid MembershipGuid { get; set; }
        public bool ResetPasswordTriggered { get; set; }
        public int? DebtorId { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_BedTracker_DefaultLocationCode)] public string NextGenBedTrackerDefaultLocationCode { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_BedTracker_DefaultCountryCode)] public string NextGenBedTrackerDefaultCountryCode { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_FareForecaster_DefaultRouteId)] public int NextGenFareForecasterDefaultRouteID { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_FareForecaster_DefaultTimeslotId)] public int NextGenFareForecasterDefaultTimeslotID { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_FareForecaster_DefaultReverseTripTimeslotId)] public int NextGenFareForecasterDefaultReverseTripTimeslotID { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_FareForecaster_DefaultReverseTrip)] public bool NextGenFareForecasterDefaultReverseTrip { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_TravelTracker_DefaultTabId)] public int NextGenTravelTrackerDefaultTabID { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_BusinessIntelligence_DefaultReportId)] public int NextGenBusinessIntelligenceDefaultReportID { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_BusinessIntelligence_DefaultReportPeriodTypeId)] public int NextGenBusinessIntelligenceDefaultReportPeriodTypeID { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_BusinessIntelligence_DefaultReportSpendTypeId)] public int NextGenBusinessIntelligenceDefaultReportSpendTypeID { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_BusinessIntelligence_DefaultReportAxisId)] public int NextGenBusinessIntelligenceDefaultReportAxisID { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_UserWidgets)] public string NextGenUserWidgets { get; set; }
        [Column(Constants.Columns.tbl_user.Serko_CorporateAccountCode)] public string SerkoCorporateAccountCode { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        [Column(Constants.Columns.tbl_user.UApprove_Hash)] public string UApproveHash { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_PasswordUpdateDate)] public DateTime? NextGenPasswordUpdateDate { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_PasswordForgotPasswordResetDate)] public DateTime? NextGenPasswordForgotPasswordResetDate { get; set; }
        public string ExternalIdentifier { get; set; }
        [Column(Constants.Columns.tbl_user.BusinessIntelligence_TravellerId)] public int? BusinessIntelligenceTravellerID { get; set; }
        [Column(Constants.Columns.tbl_user.BusinessIntelligence_TravelArrangerId)] public int? BusinessIntelligenceTravelArrangerID { get; set; }
        public bool IsWelcomeEmailSent { get; set; }
        public bool ResetSecurityQuestionTriggered { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_PasswordAttemptTimes)] public int NextGenPasswordAttemptTimes { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_TermConditionsRead)] public bool NextGenTermConditionsRead { get; set; }
        [Column(Constants.Columns.tbl_user.NextGen_WelcomeVideoRead)] public bool NextGenWelcomeVideoRead { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string DebtorIdentifier { get; set; }
        public string UserRoleList { get; set; }
        public bool IsSsoUser { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_TenantId)] public Guid AzureTenantID { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_Authnmethodsreferences)] public string AzureAuthnmethodsreferences { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_ObjectIdentifier)] public Guid AzureObjectIdentifier { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_Upn)] public string AzureUpn { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_Name)] public string AzureName { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_NameIdentifier)] public string AzureNameIdentifier { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_Givenname)] public string AzureGivenname { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_Surname)] public string AzureSurname { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_Chash)] public string AzureChash { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_PasswordExp)] public string AzurePasswordExp { get; set; }
        [Column(Constants.Columns.tbl_user.Azure_PasswordUrl)] public string AzurePasswordUrl { get; set; }
        public bool IsChildUserAccount { get; set; }
        public string WebApiAccessToken { get; set; }
        [Column(Constants.Columns.tbl_user.Sabre_ProfileName)] public string SabreProfileName { get; set; }
        [Column(Constants.Columns.tbl_user.BusinessIntel_PseudoEmployeeId)] public string BusinessIntelPseudoEmployeeID { get; set; }
    }
}