﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    public partial class Debtor
    {
        public virtual ICollection<DebtorUserSettings> DebtorUserSettings { get; set; }
    }
}
