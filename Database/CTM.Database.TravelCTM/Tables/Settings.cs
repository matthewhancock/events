using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_Settings)]
    public partial class Settings
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_QuerySQL)] public string RiskSettingsQuerySQL { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_QueryItinerarySQL)] public string RiskSettingsQueryItinerarySQL { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_QueryDefaultDebtorLinq)] public string RiskSettingsQueryDefaultDebtorLinq { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_QueryDefaultDebtorItineraryLinq)] public string RiskSettingsQueryDefaultDebtorItineraryLinq { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_QueryConnectingFlightMaxHours)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskSettingsQueryConnectingFlightMaxHours { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_QueryMinRiskRating)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskSettingsQueryMinRiskRating { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_MailHostname)] public string RiskSettingsMailHostname { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_MailPort)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskSettingsMailPort { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_MailUsername)] public string RiskSettingsMailUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_MailPassword)] public string RiskSettingsMailPassword { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_MailIsSSL)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskSettingsMailIsSSL { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_EmailFrom)] public string RiskSettingsEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_EmailCC)] public string RiskSettingsEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_EmailBCC)] public string RiskSettingsEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_EmailSubject)] public string RiskSettingsEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_EmailTemplate)] public string RiskSettingsEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_EmailAlertProcessedNotification)] public string RiskSettingsEmailAlertProcessedNotification { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_SMSMaxCharacters)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskSettingsSMSMaxCharacters { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_SMSTemplate)] public string RiskSettingsSMSTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_SMSApiUrl)] public string RiskSettingsSMSApiUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_SMSApiMethod)] public string RiskSettingsSMSApiMethod { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_SMSApiUsername)] public string RiskSettingsSMSApiUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterUsername)] public string RiskSettingsTwitterUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterAccessToken)] public string RiskSettingsTwitterAccessToken { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterConsumerKey)] public string RiskSettingsTwitterConsumerKey { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterConsumerSecret)] public string RiskSettingsTwitterConsumerSecret { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterOAuthToken)] public string RiskSettingsTwitterOAuthToken { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterPostMinRating)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskSettingsTwitterPostMinRating { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterPostLatitude)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Double? RiskSettingsTwitterPostLatitude { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterPostLongitude)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Double? RiskSettingsTwitterPostLongitude { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_TwitterMessageTemplate)] public string RiskSettingsTwitterMessageTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_FlightThresholdEmailFrom)] public string RiskSettingsFlightThresholdEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_FlightThresholdEmailCC)] public string RiskSettingsFlightThresholdEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_FlightThresholdEmailBCC)] public string RiskSettingsFlightThresholdEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_FlightThresholdEmailSubject)] public string RiskSettingsFlightThresholdEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_FlightThresholdEmailTemplate)] public string RiskSettingsFlightThresholdEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.RiskSettings_RisklineNotificationSampleSize)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? RiskSettingsRisklineNotificationSampleSize { get; set; }
        [Column(Constants.Columns.tbl_Settings.SabreWebservice_Url)] public string SabreWebserviceUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.SabreWebservice_CpaId)] public string SabreWebserviceCpaID { get; set; }
        [Column(Constants.Columns.tbl_Settings.SabreWebservice_Username)] public string SabreWebserviceUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.SabreWebservice_Password)] public string SabreWebservicePassword { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_EmailNotificationTo)] public string ReviewsEmailNotificationTo { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_EmailNotificationCC)] public string ReviewsEmailNotificationCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_EmailNotificationBCC)] public string ReviewsEmailNotificationBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_EmailNotificationFrom)] public string ReviewsEmailNotificationFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_EmailNotificationSubject)] public string ReviewsEmailNotificationSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_EmailNotificationTemplate)] public string ReviewsEmailNotificationTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_CalendarQuestionMaxMonths)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ReviewsCalendarQuestionMaxMonths { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_StarRatingText)] public string ReviewsStarRatingText { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_ReviewAddedText)] public string ReviewsReviewAddedText { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_ReviewApprovedText)] public string ReviewsReviewApprovedText { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_BlockedWords)] public string ReviewsBlockedWords { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_BatchEmailCC)] public string ReviewsBatchEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_BatchEmailBCC)] public string ReviewsBatchEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_BatchEmailFrom)] public string ReviewsBatchEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_BatchEmailSubject)] public string ReviewsBatchEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_BatchEmailTemplate)] public string ReviewsBatchEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.Reviews_BatchEmailQuerySQL)] public string ReviewsBatchEmailQuerySQL { get; set; }
        [Column(Constants.Columns.tbl_Settings.Bitly_Username)] public string BitlyUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.Bitly_APIKey)] public string BitlyAPIKey { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ResetPasswordEmailFrom)] public string UserSettingsResetPasswordEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ResetPasswordEmailCC)] public string UserSettingsResetPasswordEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ResetPasswordEmailBCC)] public string UserSettingsResetPasswordEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ResetPasswordEmailSubject)] public string UserSettingsResetPasswordEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ResetPasswordTemplate)] public string UserSettingsResetPasswordTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ForgotPasswordEmailFrom)] public string UserSettingsForgotPasswordEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ForgotPasswordEmailCC)] public string UserSettingsForgotPasswordEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ForgotPasswordEmailBCC)] public string UserSettingsForgotPasswordEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ForgotPasswordEmailSubject)] public string UserSettingsForgotPasswordEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_ForgotPasswordTemplate)] public string UserSettingsForgotPasswordTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_WelcomeEmailFrom)] public string UserSettingsWelcomeEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_WelcomeEmailCC)] public string UserSettingsWelcomeEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_WelcomeEmailBCC)] public string UserSettingsWelcomeEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_WelcomeEmailSubject)] public string UserSettingsWelcomeEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_WelcomeEmailTemplate)] public string UserSettingsWelcomeEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_EmailFrom)] public string TaxiShareEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_EmailCC)] public string TaxiShareEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_EmailBCC)] public string TaxiShareEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_EmailSubject)] public string TaxiShareEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_EmailTemplate)] public string TaxiShareEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_SMSMaxCharacters)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? TaxiShareSMSMaxCharacters { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_SMSTemplate)] public string TaxiShareSMSTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_SMSTemplateShort)] public string TaxiShareSMSTemplateShort { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_SMSApiUrl)] public string TaxiShareSMSApiUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_SMSApiMethod)] public string TaxiShareSMSApiMethod { get; set; }
        [Column(Constants.Columns.tbl_Settings.TaxiShare_SMSApiUsername)] public string TaxiShareSMSApiUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_SabreWebservice_Url)] public string NextGenBedTrackerSabreWebserviceUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_SabreWebservice_CpaId)] public string NextGenBedTrackerSabreWebserviceCpaID { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_SabreWebservice_Username)] public string NextGenBedTrackerSabreWebserviceUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_SabreWebservice_Password)] public string NextGenBedTrackerSabreWebservicePassword { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_SabreWebservice_Timeout)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenBedTrackerSabreWebserviceTimeout { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_GenerateDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenBedTrackerGenerateDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_DailyHotelAvailabilityLimit)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenBedTrackerDailyHotelAvailabilityLimit { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_DemandLevelConfig)] public string NextGenBedTrackerDemandLevelConfig { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_SabreWebservice_Url)] public string NextGenFareForecasterSabreWebserviceUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_SabreWebservice_CpaId)] public string NextGenFareForecasterSabreWebserviceCpaID { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_SabreWebservice_Username)] public string NextGenFareForecasterSabreWebserviceUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_SabreWebservice_Password)] public string NextGenFareForecasterSabreWebservicePassword { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_SabreWebservice_Timeout)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterSabreWebserviceTimeout { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_PassengerType)] public string NextGenFareForecasterPassengerType { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_PaxCount)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterPaxCount { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_CarriersToExclude)] public string NextGenFareForecasterCarriersToExclude { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_GenerateDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterGenerateDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_FareType)] public string NextGenFareForecasterFareType { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_MaximumConnectingFlights)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterMaximumConnectingFlights { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_MinimumFarePrice)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? NextGenFareForecasterMinimumFarePrice { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_MaximumFarePrice)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? NextGenFareForecasterMaximumFarePrice { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_NightStay)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterNightStay { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_IsAllotmentsIncluded)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenFareForecasterIsAllotmentsIncluded { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_IsTaxesAndFeesIncluded)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenFareForecasterIsTaxesAndFeesIncluded { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_JetStarWebservice_Username)] public string NextGenFareForecasterJetStarWebserviceUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_JetStarWebservice_Password)] public string NextGenFareForecasterJetStarWebservicePassword { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_JetStarWebservice_Domain)] public string NextGenFareForecasterJetStarWebserviceDomain { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_JetStarWebservice_Url)] public string NextGenFareForecasterJetStarWebserviceUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_JetStarWebservice_AgentManagerUrl)] public string NextGenFareForecasterJetStarWebserviceAgentManagerUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_JetStarWebservice_BookingManagerUrl)] public string NextGenFareForecasterJetStarWebserviceBookingManagerUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_JetStarWebservice_ContentManagerUrl)] public string NextGenFareForecasterJetStarWebserviceContentManagerUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_JetStar_FareCollectType)] public string NextGenFareForecasterJetStarFareCollectType { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_SMSApiUrl)] public string NextGenTravelTrackerSMSApiUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_SMSApiMethod)] public string NextGenTravelTrackerSMSApiMethod { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_SMSApiUsername)] public string NextGenTravelTrackerSMSApiUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_BingApi)] public string NextGenTravelTrackerBingApi { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_ExcludedCountries)] public string NextGenTravelTrackerExcludedCountries { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_MustGetAllCountries)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenTravelTrackerMustGetAllCountries { get; set; }
        [Column(Constants.Columns.tbl_Settings.Serko_Exponent)] public string SerkoExponent { get; set; }
        [Column(Constants.Columns.tbl_Settings.Serko_Modulus)] public string SerkoModulus { get; set; }
        [Column(Constants.Columns.tbl_Settings.Serko_SSO_URL)] public string SerkoSSOURL { get; set; }
        [Column(Constants.Columns.tbl_Settings.Serko_SSO_Key)] public string SerkoSSOKey { get; set; }
        [Column(Constants.Columns.tbl_Settings.FlightStats_AppId)] public string FlightStatsAppID { get; set; }
        [Column(Constants.Columns.tbl_Settings.FlightStats_AppKey)] public string FlightStatsAppKey { get; set; }
        [Column(Constants.Columns.tbl_Settings.FlightStats_AirportIataUrl)] public string FlightStatsAirportIataUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.FlightStats_FlightStatusUrl)] public string FlightStatsFlightStatusUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.FlightStats_Timeout)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? FlightStatsTimeout { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_SabreWebservice_Url)] public string OBTSabreWebserviceUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_SabreWebservice_CpaId)] public string OBTSabreWebserviceCpaID { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_SabreWebservice_Username)] public string OBTSabreWebserviceUsername { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_SabreWebservice_Password)] public string OBTSabreWebservicePassword { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_SabreWebservice_Timeout)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? OBTSabreWebserviceTimeout { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_FlightFareClasses)] public string OBTFlightFareClasses { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_HotelDefaultRateCode)] public string OBTHotelDefaultRateCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_StaticDateFromDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerStaticDateFromDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_StaticDateToDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerStaticDateToDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_FlightPathDateFromDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerFlightPathDateFromDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_FlightPathDateToDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerFlightPathDateToDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_TravelRiskDateFromDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerTravelRiskDateFromDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_TravelRiskDateToDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerTravelRiskDateToDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.UApprove_AuthUrl)] public string UApproveAuthUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.UApprove_AuthHashUrl)] public string UApproveAuthHashUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.UApprove_Key)] public string UApproveKey { get; set; }
        [Column(Constants.Columns.tbl_Settings.Tramada_TokenRequestUrl)] public string TramadaTokenRequestUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.Tramada_LoginUrl)] public string TramadaLoginUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.UBook_TokenRequestUrl)] public string UBookTokenRequestUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_User_MaxExistingPasswordCount)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenUserMaxExistingPasswordCount { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_User_MaxLockoutMinutes)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenUserMaxLockoutMinutes { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_RolePermissionList)] public string NextGenRolePermissionList { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravellerRolesList)] public string NextGenTravellerRolesList { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelArrangerRolesList)] public string NextGenTravelArrangerRolesList { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_DebtorAdministratorRolesList)] public string NextGenDebtorAdministratorRolesList { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_BingApi)] public string OBTBingApi { get; set; }
        public string BusinessIntelSsoLogonUrl { get; set; }
        public string BusinessIntelSsoLogonXml { get; set; }
        public string BusinessIntelUApproveSsoUrl { get; set; }
        public string BusinessIntelUExplorerSsoUrl { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_GenerationTimeout)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterGenerationTimeout { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserInactiveDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenUserInactiveDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserForcePasswordChangeDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenUserForcePasswordChangeDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_GenerationTimeout)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenBedTrackerGenerationTimeout { get; set; }
        [Column(Constants.Columns.tbl_Settings.OBT_LimitCountries)] public string OBTLimitCountries { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? BusinessIntelAutoCreateTravellerRoleId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? BusinessIntelAutoCreateTravelArrangerRoleId { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_FlightStatsDateFromDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerFlightStatsDateFromDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_FlightStatsDateToDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerFlightStatsDateToDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TravelTracker_MaxNumFlightToDisplay)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenTravelTrackerMaxNumFlightToDisplay { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserSystemPasswordResetMaxCount)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenUserSystemPasswordResetMaxCount { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserSystemPasswordResetDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenUserSystemPasswordResetDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserSystemPasswordValidHours)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenUserSystemPasswordValidHours { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserForgotPasswordEmailFrom)] public string NextGenUserForgotPasswordEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserForgotPasswordEmailCC)] public string NextGenUserForgotPasswordEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserForgotPasswordEmailBCC)] public string NextGenUserForgotPasswordEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserForgotPasswordEmailSubject)] public string NextGenUserForgotPasswordEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserForgotPasswordEmailTemplate)] public string NextGenUserForgotPasswordEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserWelcomeEmailFrom)] public string NextGenUserWelcomeEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserWelcomeEmailCC)] public string NextGenUserWelcomeEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserWelcomeEmailBCC)] public string NextGenUserWelcomeEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserWelcomeEmailSubject)] public string NextGenUserWelcomeEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserWelcomeEmailTemplate)] public string NextGenUserWelcomeEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_GenerationMaxBatchSize)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenBedTrackerGenerationMaxBatchSize { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_GenerationMinuteBatchInterval)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenBedTrackerGenerationMinuteBatchInterval { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_BedTracker_GenerationMaxBatchingProcessing)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenBedTrackerGenerationMaxBatchingProcessing { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_GenerationMaxBatchSize)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterGenerationMaxBatchSize { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_GenerationMinuteBatchInterval)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterGenerationMinuteBatchInterval { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_FareForecaster_GenerationMaxBatchingProcessing)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenFareForecasterGenerationMaxBatchingProcessing { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_NoticeboardNewsDayLimit)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenNoticeboardNewsDayLimit { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_NoticeboardBreakingNewsHourLimit)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenNoticeboardBreakingNewsHourLimit { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_NoticeboardRiskNewsDateFromDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenNoticeboardRiskNewsDateFromDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_NoticeboardRiskNewsDateToDays)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NextGenNoticeboardRiskNewsDateToDays { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_ShowKampyle)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenShowKampyle { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? BusinessIntelMaxTravellerCount { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_RiskWelcomeEmailFrom)] public string UserSettingsRiskWelcomeEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_RiskWelcomeEmailCC)] public string UserSettingsRiskWelcomeEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_RiskWelcomeEmailBCC)] public string UserSettingsRiskWelcomeEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_RiskWelcomeEmailSubject)] public string UserSettingsRiskWelcomeEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_RiskWelcomeEmailTemplate)] public string UserSettingsRiskWelcomeEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_TaxiWelcomeEmailFrom)] public string UserSettingsTaxiWelcomeEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_TaxiWelcomeEmailCC)] public string UserSettingsTaxiWelcomeEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_TaxiWelcomeEmailBCC)] public string UserSettingsTaxiWelcomeEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_TaxiWelcomeEmailSubject)] public string UserSettingsTaxiWelcomeEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_TaxiWelcomeEmailTemplate)] public string UserSettingsTaxiWelcomeEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_HotelReviewsWelcomeEmailFrom)] public string UserSettingsHotelReviewsWelcomeEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_HotelReviewsWelcomeEmailCC)] public string UserSettingsHotelReviewsWelcomeEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_HotelReviewsWelcomeEmailBCC)] public string UserSettingsHotelReviewsWelcomeEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_HotelReviewsWelcomeEmailSubject)] public string UserSettingsHotelReviewsWelcomeEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.UserSettings_HotelReviewsWelcomeEmailTemplate)] public string UserSettingsHotelReviewsWelcomeEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserBedTrackerWelcomeEmailFrom)] public string NextGenUserBedTrackerWelcomeEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserBedTrackerWelcomeEmailCC)] public string NextGenUserBedTrackerWelcomeEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserBedTrackerWelcomeEmailBCC)] public string NextGenUserBedTrackerWelcomeEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserBedTrackerWelcomeEmailSubject)] public string NextGenUserBedTrackerWelcomeEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserBedTrackerWelcomeEmailTemplate)] public string NextGenUserBedTrackerWelcomeEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserFareForecasterWelcomeEmailFrom)] public string NextGenUserFareForecasterWelcomeEmailFrom { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserFareForecasterWelcomeEmailCC)] public string NextGenUserFareForecasterWelcomeEmailCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserFareForecasterWelcomeEmailBCC)] public string NextGenUserFareForecasterWelcomeEmailBCC { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserFareForecasterWelcomeEmailSubject)] public string NextGenUserFareForecasterWelcomeEmailSubject { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_UserFareForecasterWelcomeEmailTemplate)] public string NextGenUserFareForecasterWelcomeEmailTemplate { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_TermConditions)] public string NextGenTermConditions { get; set; }
        [Column(Constants.Columns.tbl_Settings.NextGen_WelcomeVideoEmbed)] public string NextGenWelcomeVideoEmbed { get; set; }
        [Column(Constants.Columns.tbl_Settings.PTA_RolePermissionList)] public string PTARolePermissionList { get; set; }
        [Column(Constants.Columns.tbl_Settings.PTA_TravellerRolesList)] public string PTATravellerRolesList { get; set; }
        [Column(Constants.Columns.tbl_Settings.PTA_TravelArrangerRolesList)] public string PTATravelArrangerRolesList { get; set; }
        [Column(Constants.Columns.tbl_Settings.PTA_ApproverRolesList)] public string PTAApproverRolesList { get; set; }
        [Column(Constants.Columns.tbl_Settings.PTA_SuperApproverRolesList)] public string PTASuperApproverRolesList { get; set; }
    }
}
