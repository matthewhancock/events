using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.World_Borders)]
    public partial class WorldBorders
    {
        public string FIPS { get; set; }
        public string ISO2 { get; set; }
        public string ISO3 { get; set; }
        public int? UN { get; set; }
        public string NAME { get; set; }
        public Int64? POP2005 { get; set; }
        public int? REGION { get; set; }
        public int? SUBREGION { get; set; }
        public Double? LON { get; set; }
        public Double? LAT { get; set; }
        public Object GEOM { get; set; }
        public Object GEOG { get; set; }
    }
}
