using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_NextGen_BedTrackerItem)]
    public partial class NextGenBedTrackerItem
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int NextGenBedTrackerId { get; set; }
        public Decimal? AverageRate { get; set; }
        public Decimal? CombinedAverageChangePercent { get; set; }
        public DateTime? ForecastDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? BuildStartDateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? BuildEndDateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
    }
}
