using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_NextGen_BusinessIntelligence_ReportData)]
    public partial class NextGenBusinessIntelligenceReportData
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int DebtorId { get; set; }
        public int ReportId { get; set; }
        public string Title { get; set; }
        public string DebtorIdentifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? SpendTypeId { get; set; }
        public string SpendTypeTitle { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? PeriodId { get; set; }
        public string PeriodTitle { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ValidFrom { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ValidTo { get; set; }
        public string ReportData { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? SortOrder { get; set; }
    }
}
