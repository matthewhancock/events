using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_PTA_SubmissionQuote)]
    public partial class PTASubmissionQuote
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int PTASubmissionPaxId { get; set; }
        public int PTASubmissionQuoteStatusId { get; set; }
        public string PTASubmissionQuoteStatusTitle { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ItineraryId { get; set; }
        public string PNR { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsFromConsultant { get; set; }
        public Guid? Identifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? TotalAmount { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsProcessed { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string Canx { get; set; }
        public string Changes { get; set; }
        public string Visas { get; set; }
        public DateTime? Ttl { get; set; }
        public string Comment { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsUserSelected { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? LostSavings { get; set; }
        public string ConsultantQuoteName { get; set; }
        public string FareClass { get; set; }
    }
}
