using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_NextGen_HotelYearlyBenchmark)]
    public partial class NextGenHotelYearlyBenchmark
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string PCC { get; set; }
        public string CurrencyCode { get; set; }
        public string Day { get; set; }
        public Decimal? BenchmarkLevelLow { get; set; }
        public Decimal? BenchmarkLevelHigh { get; set; }
        public string CountryCode { get; set; }
        public string HotelCityCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? BuildStartDateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? BuildEndDateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
