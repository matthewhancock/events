using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_OBT_ItineraryPassenger)]
    public partial class OBTItineraryPassenger
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Int64 Id { get; set; }
        public Int64 ItineraryId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? IndexField { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        public int ClientId { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string EmployeeNumber { get; set; }
        public string CompanyName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? DepartmentId { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? CostCentreId { get; set; }
        public string CostCentreCode { get; set; }
        public string CostCentreName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ContactId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string PNR { get; set; }
        public string SabreReservationXml { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? StatusId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? SelectedCostCentreId { get; set; }
        public string SelectedCostCentreCode { get; set; }
        public string SelectedCostCentreName { get; set; }
        [Column(Constants.Columns.tbl_OBT_ItineraryPassenger.BusinessIntelligence_CreditCardId)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? BusinessIntelligenceCreditCardID { get; set; }
        public string CreditCardDisplayNumber { get; set; }
        public string CreditCardEncryptedNumber { get; set; }
        public string CreditCardHolder { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreditCardExpiryDate { get; set; }
        public string CreditCardTypeCode { get; set; }
        public string FrequentFlyerCodes { get; set; }
        public string SabreErrorXml { get; set; }
    }
}
