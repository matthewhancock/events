using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_NextGen_Document)]
    public partial class NextGenDocument
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int? DebtorId { get; set; }
        public int? UserId { get; set; }
        public int CategoryId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? DocumentRoleType { get; set; }
        public string Title { get; set; }
        public string CategoryTitle { get; set; }
        public string FilePath { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? FileSize { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ExpiryDate { get; set; }
        public string DebtorIdentifier { get; set; }
        public string ExternalUrl { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string CountryCode { get; set; }
    }
}
