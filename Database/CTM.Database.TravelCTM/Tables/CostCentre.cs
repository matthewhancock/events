using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_CostCentre)]
    public partial class CostCentre
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string DebtorCode { get; set; }
        public string CostCentreCode { get; set; }
        public string CostCentreName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public string DepartmentCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? CostCentreId { get; set; }
    }
}
