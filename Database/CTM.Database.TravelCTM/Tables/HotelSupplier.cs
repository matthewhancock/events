using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_HotelSupplier)]
    public partial class HotelSupplier
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public string SabreCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Double? Latitude { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Double? Longitude { get; set; }
        public string Address { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string DefaultHotelImage { get; set; }
        public string ChainCode { get; set; }
        public string OriginalImageFileName { get; set; }
        public string RichMediaScript { get; set; }
        public DateTime? LastModified { get; set; }
        public string CityCode { get; set; }
        public string HotelFacilities { get; set; }
        public string Attractions { get; set; }
        public string Description { get; set; }
        public string Dining { get; set; }
        public string Directions { get; set; }
        public string Facilities { get; set; }
        public string Location { get; set; }
        public string SerkoPropertyCode { get; set; }
        public string Area { get; set; }
        public string CheckInInformation { get; set; }
        public string PolicyInformation { get; set; }
        public string RoomInformation { get; set; }
    }
}
