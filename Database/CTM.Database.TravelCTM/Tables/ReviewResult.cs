using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_ReviewResult)]
    public partial class ReviewResult
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public string CompanyIdentifier { get; set; }
        public string DebtorIdentifier { get; set; }
        public string Source { get; set; }
        public string HotelSabreID { get; set; }
        public string HotelID { get; set; }
        public string HotelName { get; set; }
        public string HotelCity { get; set; }
        public string HotelCountryCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ReviewStatusId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? OverallRating { get; set; }
        public string Comments { get; set; }
        public string UserFullName { get; set; }
        public string UserIPAddress { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
