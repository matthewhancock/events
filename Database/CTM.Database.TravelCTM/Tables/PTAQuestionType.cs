using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_PTA_QuestionType)]
    public partial class PTAQuestionType
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public string ViewName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsFixed { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsParentOnly { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsChildOnly { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsChildAvailable { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? DisplayInQAndA { get; set; }
    }
}
