using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_HotelSupplierGds)]
    public partial class HotelSupplierGds
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int HotelSupplierId { get; set; }
        public string GdsProvider { get; set; }
        public string GdsProviderHotelId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
