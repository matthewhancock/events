using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_Concur_ReceiptEmailUpload)]
    public partial class ConcurReceiptEmailUpload
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int DebtorId { get; set; }
        public int TramadaClientId { get; set; }
        public int TramadaCreditCardId { get; set; }
        public string TramadaCardHolder { get; set; }
        public string TramadaCardNumberDisplay { get; set; }
        public string TramadaEmail { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
