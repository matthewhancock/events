using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_RiskMessage)]
    public partial class RiskMessage
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string MessageID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? MessageDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? RiskDate { get; set; }
        public int RiskRatingID { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }
        public string Destinations { get; set; }
        public string ContentText { get; set; }
        public string Source { get; set; }
        public string EmailSubject { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsProcessed { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsTwitterPosted { get; set; }
        public string FormattedEmailMessage { get; set; }
        public string FormattedSMSMessage { get; set; }
        public string ViewerShortUrl { get; set; }
        public string ViewerShortUrlHash { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskProviderId { get; set; }
        public string RegionNames { get; set; }
        public string RegionCodes { get; set; }
    }
}
