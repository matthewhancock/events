using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_OBT_ItinerarySearchHotel)]
    public partial class OBTItinerarySearchHotel
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Int64 Id { get; set; }
        public Int64 ItineraryId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? CheckIn { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? CheckOut { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
