using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_UserPasswordToken)]
    public partial class UserPasswordToken
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int UserId { get; set; }
        public string RedirectUrl { get; set; }
        public DateTimeOffset? RequestDate { get; set; }
        public DateTimeOffset? TokenExpiryDate { get; set; }
        public int TokenDurationInSeconds { get; set; }
        public DateTime? TokenUsedDate { get; set; }
        public Guid? Identifier { get; set; }
        public bool Enabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
