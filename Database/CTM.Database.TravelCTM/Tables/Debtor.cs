using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_Debtor)]
    public partial class Debtor
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public string Identifier { get; set; }
        public string ArnoldCompanyIdentifier { get; set; }
        public string SerkoCompanyIdentifier { get; set; }
        public string SerkoSSOKey { get; set; }
        public string RiskEngineQueryLinq { get; set; }
        public string RiskEngineItineraryLinq { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskEngineEmailEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskEngineSMSEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskEngineMinRating { get; set; }
        public string RiskEngineGroupEmail { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskEngineTravelArrangerGroupEmailEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskEngineFlightThresholdEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskEngineFlightThresholdLimit { get; set; }
        public string RiskEngineFlightThresholdEmail { get; set; }
        public string ReviewApprovalEmails { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ReviewBatchNotificationEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ReviewApprovalNotRequired { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ReviewAllowAnonymous { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TaxiShareEmailEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TaxiShareSMSEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string TaxiShareHourSendTimeStamp { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? TaxiShareItineraryTimeWindow { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? TaxiShareConnectingFlightMaxHours { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TaxiShareMobileNumbersShown { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string SabrePcc { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string SabreCurrencyCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string SabreCorporateId { get; set; }
        public string SabreHotelRateCodes { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string JetStarSourceOrganization { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string JetStarCurrencyCode { get; set; }
        public string FareForecasterPreferredCarriers { get; set; }
        [Column(Constants.Columns.tbl_Debtor.NextGen_ExcludeWidgets)] public string NextGenExcludeWidgets { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_ExcludeRates)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string OBTExcludeRates { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string DebtorLogo { get; set; }
        public string TramadaSSOAgencyId { get; set; }
        public string TramadaSSODebtorId { get; set; }
        public string TramadaSSODebtorAuthCode { get; set; }
        public string BusinessIntelSSOLogin { get; set; }
        public string BusinessIntelSSOPassword { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? CtmSsoApiKey { get; set; }
        public string CtmSsoToken { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_AgencyAddressline)] public string OBTAgencyAddressline { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_AgencyCity)] public string OBTAgencyCity { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_AgencyCountryCode)] public string OBTAgencyCountryCode { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_AgencyPostcode)] public string OBTAgencyPostcode { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_AgencyState)] public string OBTAgencyState { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_AgencyStreet)] public string OBTAgencyStreet { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? BusinessIntelIsAutoUserCreation { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? BusinessIntelAutoUserCreationTypeId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? AdvertisementEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.NextGen_TravelTracker_IsSmsEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenTravelTrackerIsSmsEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.NextGen_TravelTracker_IsEmailEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenTravelTrackerIsEmailEnabled { get; set; }
        public string HomeCountryCode { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_FinishingToolData)] public string OBTFinishingToolData { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string TramadaDatabase { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Approval_EmailReminderHour)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? PTAApprovalEmailReminderHour { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Approval_EmailReminderMaxPerHour)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? PTAApprovalEmailReminderMaxPerHour { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Approval_IsEmailEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? PTAApprovalIsEmailEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Approval_IsSmsEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? PTAApprovalIsSmsEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Approval_FirstSendReceivers)] public string PTAApprovalFirstSendReceivers { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Approval_FollowUpReceivers)] public string PTAApprovalFollowUpReceivers { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Approval_OnApprovedReceivers)] public string PTAApprovalOnApprovedReceivers { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Approval_OnDeclinedReceivers)] public string PTAApprovalOnDeclinedReceivers { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? BusinessIntelAutoApproverCreation { get; set; }
        public string SerkoSSOUrl { get; set; }
        [Column(Constants.Columns.tbl_Debtor.NextGen_OnlineCheckIn_FeaturedAirlineCarriers)] public string NextGenOnlineCheckInFeaturedAirlineCarriers { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_SabreQueueNumber)] public string PTASabreQueueNumber { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_ConsultantEmailTo)] public string PTAConsultantEmailTo { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_Maximum_Risk_Rating)] public int? PTAMaximumRiskRating { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? HotelPolicyFilterType { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? FlightPolicyFilterType { get; set; }
        [Column(Constants.Columns.tbl_Debtor.NextGen_IsGuestTravellerEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenIsGuestTravellerEnabled { get; set; }
        public string SabreFlightRateCodes { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_FinishingHostCommand)] public string OBTFinishingHostCommand { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_SupportPhone)] public string OBTSupportPhone { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_SupportEmail)] public string OBTSupportEmail { get; set; }
        [Column(Constants.Columns.tbl_Debtor.Azure_TenantList)] public string AzureTenantList { get; set; }
        public string PNRHostCommands { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? WelcomeEmailDisabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDebtorGroup { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_GuestTravelerEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? PTAGuestTravelerEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_SabreQueueNumber)] public string OBTSabreQueueNumber { get; set; }
        [Column(Constants.Columns.tbl_Debtor.Concur_InvoiceImageUploadEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ConcurInvoiceImageUploadEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.Concur_FromAddress)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ConcurFromAddress { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_PnrOrderNumber_IsEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? OBTPnrOrderNumberIsEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_PnrOrderNumberCodes)] public string OBTPnrOrderNumberCodes { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? HideFromUserSelection { get; set; }
        public string SabreProfileDebtorCode { get; set; }
        public string SabreProfileAssociationId { get; set; }
        public string SabreProfileAssociationName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? BusinessIntelAutoUserCreationProfileTypeId { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_EmployeeTypeCodeAllowedToBypassApproval)] public string PTAEmployeeTypeCodeAllowedToBypassApproval { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? SabreProfileSync { get; set; }
        [Column(Constants.Columns.tbl_Debtor.NextGen_IsFareForecasterBypassPtaEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenIsFareForecasterBypassPtaEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.NextGen_IsBedTrackerBypassPtaEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? NextGenIsBedTrackerBypassPtaEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskEngineOfficeHourSendOnlyMaxRiskRating { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string RiskEngineRiskSendStartTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string RiskEngineRiskSendEndTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? DeepLinkTypeId { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorAddressline)] public string OBTDebtorAddressline { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorStreet)] public string OBTDebtorStreet { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorCity)] public string OBTDebtorCity { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorCountryCode)] public string OBTDebtorCountryCode { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorPostcode)] public string OBTDebtorPostcode { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorState)] public string OBTDebtorState { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorFirstName)] public string OBTDebtorFirstName { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorLastName)] public string OBTDebtorLastName { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_DebtorCityStationCode)] public string OBTDebtorCityStationCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? SerkoIsDynamicCostCenter { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TramadaProfileSync { get; set; }
        [Column(Constants.Columns.tbl_Debtor.Concur_IsCreditCardHolderEmailEnabled)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ConcurIsCreditCardHolderEmailEnabled { get; set; }
        [Column(Constants.Columns.tbl_Debtor.Concur_CreditCardHolderAddress)] public string ConcurCreditCardHolderAddress { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsActivateSsoViaEmployeeId { get; set; }
        [Column(Constants.Columns.tbl_Debtor.OBT_ExcludeGdsProviders)] public string OBTExcludeGdsProviders { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? TravellerProfileSyncTypeId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? SendDebtorGroupingRiskMessage { get; set; }
        public string DebtorGroupingRiskEmailAddress { get; set; }
        public string ExternalSource { get; set; }
        [Column(Constants.Columns.tbl_Debtor.PTA_GuestTravellerMobileRequired)] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? PTAGuestTravellerMobileRequired { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? LightningInstanceId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? AllowPTADirect { get; set; }
        public Guid? UmbrellaFacesID { get; set; }
    }
}
