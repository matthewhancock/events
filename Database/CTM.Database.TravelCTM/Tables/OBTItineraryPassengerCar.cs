using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_OBT_ItineraryPassengerCar)]
    public partial class OBTItineraryPassengerCar
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public Int64 ItineraryPassengerId { get; set; }
        public int StatusId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateFrom { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateTo { get; set; }
        public string SupplierCode { get; set; }
        public string Supplier { get; set; }
        public string VehicleCode { get; set; }
        public string Vehicle { get; set; }
        public string VehicleClass { get; set; }
        public string RateCode { get; set; }
        public string CurrencyCode { get; set; }
        public Decimal Amount { get; set; }
        public string PickupLocationCode { get; set; }
        public string PickupLocation { get; set; }
        public string ReturnLocationCode { get; set; }
        public string ReturnLocation { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? Quantity { get; set; }
        public string SabreCarSegmentXml { get; set; }
        public string SabreCarReservationXml { get; set; }
        public string PNR { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? LostSavingsAmount { get; set; }
        public string PolicyExceptionCode { get; set; }
        public string PolicyExceptionTitle { get; set; }
        public string PolicyExceptionComments { get; set; }
    }
}
