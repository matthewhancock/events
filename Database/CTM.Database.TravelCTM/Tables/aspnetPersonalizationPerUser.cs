using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.aspnet_PersonalizationPerUser)]
    public partial class AspnetPersonalizationPerUser
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Id { get; set; }
        public Guid? PathId { get; set; }
        public Guid? UserId { get; set; }
        public Byte[] PageSettings { get; set; }
        public DateTime LastUpdatedDate { get; set; }
    }
}
