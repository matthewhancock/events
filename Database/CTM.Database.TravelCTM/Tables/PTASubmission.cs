using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_PTA_Submission)]
    public partial class PTASubmission
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? SubmissionUserId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? SubmissionStatusId { get; set; }
        public int SubmissionDebtorId { get; set; }
        public int? LastApproverId { get; set; }
        public string Title { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        public string DebtorIdentifier { get; set; }
        public string ReasonForTravel { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsRequestForApprovalNotificationSent { get; set; }
        public DateTime? CompletedOn { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? FollowUpNotificationCount { get; set; }
        public DateTime? FollowUpNotificationOn { get; set; }
        public string Destinations { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? FlightRequired { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? HotelRequired { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? CarRequired { get; set; }
        public string LastApprover { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsNonConsultantQuoteProcessed { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsNonConsultantQuote { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsBookingQuoteRequired { get; set; }
        public string ProjectCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsConsultantNotificationSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsTravellerPendingQuoteSelectionSent { get; set; }
        public DateTime? ExpiryDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsAutoDecline { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsAutoDeclineProcessed { get; set; }
        public string TravelArrangementsComments { get; set; }
        public int? PTAPolicyId { get; set; }
        public string SerkoPnr { get; set; }
        public string SerkoProfileCode { get; set; }
        public DateTimeOffset? CompletedOnOffset { get; set; }
        public DateTimeOffset? FollowUpNotificationOnOffset { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? PaxRolesAllowApprovalByPass { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? PolicyAllowsApprovalByPass { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsMoreInformationNotificationSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsRequoteNotificationSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsCancelRequestNotificationSent { get; set; }
    }
}
