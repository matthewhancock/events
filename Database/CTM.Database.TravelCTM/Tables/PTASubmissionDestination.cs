using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_PTA_SubmissionDestination)]
    public partial class PTASubmissionDestination
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int PTASubmissionId { get; set; }
        public string LocationCode { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string OriginLocationCode { get; set; }
        public string OriginCityName { get; set; }
        public string OriginCountryName { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsInternational { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string Comments { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsLandOnly { get; set; }
    }
}
