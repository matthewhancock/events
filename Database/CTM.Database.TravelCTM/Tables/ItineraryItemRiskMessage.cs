using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_ItineraryItemRiskMessage)]
    public partial class ItineraryItemRiskMessage
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public int ItineraryItemId { get; set; }
        public int RiskMessageId { get; set; }
        public int? RiskTravelArrangerGroupId { get; set; }
        public int? RiskDebtorGroupId { get; set; }
        public string PNR { get; set; }
        public string DebtorIdentifier { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? EmailSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? SMSSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
