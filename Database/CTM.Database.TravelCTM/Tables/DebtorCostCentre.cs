using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_DebtorCostCentre)]
    public partial class DebtorCostCentre
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public int DebtorId { get; set; }
        public int? DepartmentId { get; set; }
        public int CostCentreId { get; set; }
        public string CostCentreCode { get; set; }
        public string CostCentreCodeName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskEngineEmailEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? RiskEngineSMSEnabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RiskEngineMinRating { get; set; }
        public string RiskEngineCountries { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
