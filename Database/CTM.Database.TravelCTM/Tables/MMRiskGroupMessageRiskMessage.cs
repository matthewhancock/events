using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_MM_RiskGroupMessage_RiskMessage)]
    public partial class MMRiskGroupMessageRiskMessage
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int id { get; set; }
        [Column(Constants.Columns.tbl_MM_RiskGroupMessage_RiskMessage.group_message_id)] public int groupmessageID { get; set; }
        [Column(Constants.Columns.tbl_MM_RiskGroupMessage_RiskMessage.risk_message_id)] public int riskmessageID { get; set; }
    }
}
