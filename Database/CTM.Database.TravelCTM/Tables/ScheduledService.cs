using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_ScheduledService)]
    public partial class ScheduledService
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public string ServiceName { get; set; }
        public string ServiceAction { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Concurrent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Processing { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ProcessStartTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ProcessEndTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ProcessingMaxMinutes { get; set; }
    }
}
