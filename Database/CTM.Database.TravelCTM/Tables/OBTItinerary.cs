using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_OBT_Itinerary)]
    public partial class OBTItinerary
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Int64 Id { get; set; }
        public int UserId { get; set; }
        public int StatusId { get; set; }
        public string OriginLocation { get; set; }
        public string OriginLocationCode { get; set; }
        public string DestinationLocation { get; set; }
        public string DestinationLocationCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateFrom { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateTo { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? PaxCount { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ReturnTrip { get; set; }
        public string SearchTypes { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? DebtorId { get; set; }
        public string DebtorIdentifier { get; set; }
        public int? AgencyReasonCodeId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? OriginInternationalFlight { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? DestinationInternationalFlight { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        public string OrderNumber { get; set; }
    }
}
