using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_OBT_ItinerarySearchCar)]
    public partial class OBTItinerarySearchCar
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Int64 Id { get; set; }
        public Int64 ItineraryId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        public string PickUpLocation { get; set; }
        public string PickUpLocationCode { get; set; }
        public string DropOffLocation { get; set; }
        public string DropOffLocationCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? PickUpDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? DropOffDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
