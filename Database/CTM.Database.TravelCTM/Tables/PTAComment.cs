using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_PTA_Comment)]
    public partial class PTAComment
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int id { get; set; }
        public int userId { get; set; }
        [Column(Constants.Columns.tbl_PTA_Comment.submission_id)] public int submissionID { get; set; }
        public string comment { get; set; }
        public string username { get; set; }
        public Int64 unix { get; set; }
    }
}
