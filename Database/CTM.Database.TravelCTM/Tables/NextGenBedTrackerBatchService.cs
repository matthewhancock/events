using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_NextGen_BedTrackerBatchService)]
    public partial class NextGenBedTrackerBatchService
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public int BedTrackerId { get; set; }
        public string SabrePcc { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Processing { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ProcessStartTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ProcessEndTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? ProcessingIdentifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsYearlyBenchmark { get; set; }
    }
}
