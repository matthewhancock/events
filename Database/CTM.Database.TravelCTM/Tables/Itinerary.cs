using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_Itinerary)]
    public partial class Itinerary
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string PNR { get; set; }
        public string Debtor { get; set; }
        public string TravelItineraryXml { get; set; }
        public string RiskEngineMessagesEmailSent { get; set; }
        public string RiskEngineMessagesSMSSent { get; set; }
        public int? DebtorId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string PCC { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateFrom { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateTo { get; set; }
        public int? BiTravelArrangerId { get; set; }
        public string BiTravelArrangerEmail { get; set; }
    }
}
