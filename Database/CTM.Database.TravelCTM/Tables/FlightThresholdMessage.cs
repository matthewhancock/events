using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_FlightThresholdMessage)]
    public partial class FlightThresholdMessage
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public int DebtorId { get; set; }
        public string DebtorIdentifier { get; set; }
        public string EmployeeTypeCode { get; set; }
        public string EmailMessage { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? EmailSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ThresholdLimit { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? PaxCount { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string LocationCode { get; set; }
        public string Terminal { get; set; }
        public string OriginCountryName { get; set; }
        public string OriginCityName { get; set; }
        public string OriginLocationCode { get; set; }
        public string OriginTerminal { get; set; }
        public string Airline { get; set; }
        public string FlightReference { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateFrom { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateTo { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
