using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_OBT_ItineraryPassengerHotel)]
    public partial class OBTItineraryPassengerHotel
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Int64 Id { get; set; }
        public Int64 ItineraryPassengerId { get; set; }
        public int StatusId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? Identifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateFrom { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateTo { get; set; }
        public string HotelCode { get; set; }
        public string HotelName { get; set; }
        public string RateCode { get; set; }
        public string RoomName { get; set; }
        public string ChainCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? Amount { get; set; }
        public string LocationCode { get; set; }
        public string Location { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? Quantity { get; set; }
        public string PNR { get; set; }
        public string SabreHotelReservationXml { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Decimal? LostSavingsAmount { get; set; }
        public string PolicyExceptionCode { get; set; }
        public string PolicyExceptionTitle { get; set; }
        public string PolicyExceptionComments { get; set; }
        public string SpecialRequirements { get; set; }
    }
}
