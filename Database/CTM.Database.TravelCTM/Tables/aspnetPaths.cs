using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.aspnet_Paths)]
    public partial class AspnetPaths
    {
        public Guid ApplicationId { get; set; }
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? PathId { get; set; }
        public string Path { get; set; }
        public string LoweredPath { get; set; }
    }
}
