using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.aspnet_Users)]
    public partial class AspnetUsers
    {
        public Guid ApplicationId { get; set; }
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public Guid? UserId { get; set; }
        public string UserName { get; set; }
        public string LoweredUserName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string MobileAlias { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsAnonymous { get; set; }
        public DateTime LastActivityDate { get; set; }
    }
}
