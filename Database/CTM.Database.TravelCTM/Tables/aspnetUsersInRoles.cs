using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.aspnet_UsersInRoles)]
    public partial class AspnetUsersInRoles
    {
        public Guid UserId { get; private set; }
        public Guid RoleId { get; private set; }
    }
}
