using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_ItineraryItem)]
    public partial class ItineraryItem
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int ItineraryId { get; set; }
        public string Identifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ItineraryTypeId { get; set; }
        public string PNR { get; set; }
        public string Debtor { get; set; }
        public string Reference { get; set; }
        public string ItemCode { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string Address { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string LocationCode { get; set; }
        public string Terminal { get; set; }
        public string OriginCountryName { get; set; }
        public string OriginCityName { get; set; }
        public string OriginLocationCode { get; set; }
        public string OriginTerminal { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Phone { get; set; }
        public string RiskEngineMessagesEmailSent { get; set; }
        public string RiskEngineMessagesSMSSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? ReviewBatchEmailSent { get; set; }
        public string ReservationItemXml { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string PCC { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TaxiShareMessageSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? TaxiShareMessageCreated { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ReviewBatchEmailDateSent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? TaxiShareMessageDateSent { get; set; }
        public string Status { get; set; }
        public string MarketingCode { get; set; }
        public string MarketingFlightNumber { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? StopQuantity { get; set; }
        public string SupplierReference { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? RequestOrder { get; set; }
        public string SeatNumber { get; set; }
    }
}
