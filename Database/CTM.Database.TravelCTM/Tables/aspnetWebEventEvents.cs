using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.aspnet_WebEvent_Events)]
    public partial class AspnetWebEventEvents
    {
        [Key] public string EventId { get; set; }
        public DateTime EventTimeUtc { get; set; }
        public DateTime EventTime { get; set; }
        public string EventType { get; set; }
        public Decimal EventSequence { get; set; }
        public Decimal EventOccurrence { get; set; }
        public int EventCode { get; set; }
        public int EventDetailCode { get; set; }
        public string Message { get; set; }
        public string ApplicationPath { get; set; }
        public string ApplicationVirtualPath { get; set; }
        public string MachineName { get; set; }
        public string RequestUrl { get; set; }
        public string ExceptionType { get; set; }
        public string Details { get; set; }
    }
}
