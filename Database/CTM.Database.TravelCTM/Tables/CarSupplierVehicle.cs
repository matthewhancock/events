using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_CarSupplierVehicle)]
    public partial class CarSupplierVehicle
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public int CarSupplierId { get; set; }
        public string VehicleTypeCode { get; set; }
        public string VehicleClass { get; set; }
        public string Image { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? OptionAutomatic { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? OptionAirConditioning { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? OptionPassengerCount { get; set; }
        public int OptionLuggageCount { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? OptionDoorCount { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string VehicleCategory { get; set; }
        public string VehicleSize { get; set; }
    }
}
