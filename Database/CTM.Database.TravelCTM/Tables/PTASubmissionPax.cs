using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTM.Tables
{
    [Table(TravelCTM.Constants.Tables.tbl_PTA_SubmissionPax)]
    public partial class PTASubmissionPax
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int PTASubmissionId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? PTASubmissionPaxStatusId { get; set; }
        public string PTASubmissionPaxStatusTitle { get; set; }
        public int? ClientId { get; set; }
        public string ProfileName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string CostCentreCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? CheckedBaggage { get; set; }
        public string FlightClass { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? IsDeleted { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
        public string CostCentreName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string EmployeeCode { get; set; }
        public int? PTALastApproverId { get; set; }
        public string TravellerTitle { get; set; }
        public string Mobile { get; set; }
        public string BusIntEmployeeCode { get; set; }
    }
}
