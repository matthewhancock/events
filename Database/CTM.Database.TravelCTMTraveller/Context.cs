﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace CTM.Database.TravelCTMTraveller
{
    public class Context : DbContext
    {
        private readonly IConfiguration _configuration;
        public Context(DbContextOptions<Context> options, IConfiguration configuration = null) : base(options)
        {
            _configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder ModelBuilder)
        {
            ModelBuilder.HasSequence(Constants.Sequences.UmbrellaFacesExternalSourceID).StartsAt(-1).IncrementsBy(-1);

            base.OnModelCreating(ModelBuilder);
        }

        //public virtual DbSet<Tables.MaintenanceLog> MaintenanceLogs { get; set; }
        //public virtual DbSet<Tables.MigrationHistory> MigrationHistorys { get; set; }
        //public virtual DbSet<Tables.ClientApplication> ClientApplications { get; set; }
        public virtual DbSet<Tables.CostCentre> CostCentres { get; set; }
        public virtual DbSet<Tables.CostCentreDebtorCreditCard> CostCentreDebtorCreditCards { get; set; }
        public virtual DbSet<Tables.CostCentreDepartment> CostCentreDepartments { get; set; }
        public virtual DbSet<Tables.CreditCard> CreditCards { get; set; }
        public virtual DbSet<Tables.DebtorCreditCard> DebtorCreditCards { get; set; }
        public virtual DbSet<Tables.DebtorProcess> DebtorProcesss { get; set; }
        public virtual DbSet<Tables.DebtorProcessAudit> DebtorProcessAudits { get; set; }
        public virtual DbSet<Tables.Department> Departments { get; set; }
        public virtual DbSet<Tables.DepartmentDebtorCreditCard> DepartmentDebtorCreditCards { get; set; }
        public virtual DbSet<Tables.MiddlewareProvider> MiddlewareProviders { get; set; }
        public virtual DbSet<Tables.MiddlewareWatermark> MiddlewareWatermarks { get; set; }
        //public virtual DbSet<Tables.NLog> NLogs { get; set; }
        //public virtual DbSet<Tables.SchemaVersions> SchemaVersions { get; set; }
        public virtual DbSet<Tables.TravelArranger> TravelArrangers { get; set; }
        public virtual DbSet<Tables.TravelArrangerTraveller> TravelArrangerTravellers { get; set; }
        public virtual DbSet<Tables.Traveller> Travellers { get; set; }
        public virtual DbSet<Tables.TravellerCreditCard> TravellerCreditCards { get; set; }
        public virtual DbSet<Tables.TravellerExtraInfo> TravellerExtraInfos { get; set; }
        public virtual DbSet<Tables.TravellerMembership> TravellerMemberships { get; set; }
        public virtual DbSet<Tables.TravellerPassport> TravellerPassports { get; set; }
        public virtual DbSet<Tables.TravellerVisa> TravellerVisas { get; set; }


        public async Task<int> GetUmbrellaFacesExternalSourceID()
        {
            var connection = (SqlConnection) Database.GetDbConnection();
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                await connection.OpenAsync();
            }

            using (var cmd = new SqlCommand($"SELECT NEXT VALUE FOR {Constants.Sequences.UmbrellaFacesExternalSourceID}", connection))
            {
                return (int)(long) await cmd.ExecuteScalarAsync();
            }
        }
    }
}
