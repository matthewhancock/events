﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Types
{
    public abstract class CreditCard : Entity
    {
        public string CardHolder { get; set; }
        public DateTimeOffset? ExpiryDate { get; set; }
        public string CardNumberDisplay { get; set; }
        public string CardTypeCode { get; set; }
        public string DebtorIdentifier { get; set; }
        public string Token { get; set; }
        public DateTime? TerminationDate { get; set; }
        public int ExternalCreditCardId { get; set; }
        public int ExternalSourceId { get; set; }
    }
}
