﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Types
{
    public abstract class Entity : Common.BaseEntity
    {
        public int SourceTypeId { get; set; }

        public bool IsDeleted { get; set; }
        public bool Enabled { get; set; }
    }
}
