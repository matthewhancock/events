﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Constants.Values
{
    public static class TravellerMembership
    {
        public static class Type
        {
            public const string Air = "AIR";
            public const string Car = "CAR";
            public const string Hotel = "HOTEL";
        }
    }
}
