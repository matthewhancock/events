namespace CTM.Database.TravelCTMTraveller.Constants
{
    public static class Tables
    {
        public const string __MigrationHistory = nameof(__MigrationHistory);
        public const string ClientApplication = nameof(ClientApplication);
        public const string CostCentre = nameof(CostCentre);
        public const string CostCentreDebtorCreditCard = nameof(CostCentreDebtorCreditCard);
        public const string CostCentreDepartment = nameof(CostCentreDepartment);
        public const string CreditCard = nameof(CreditCard);
        public const string DebtorCreditCard = nameof(DebtorCreditCard);
        public const string DebtorProcess = nameof(DebtorProcess);
        public const string DebtorProcessAudit = nameof(DebtorProcessAudit);
        public const string Department = nameof(Department);
        public const string DepartmentDebtorCreditCard = nameof(DepartmentDebtorCreditCard);
        public const string MaintenanceLog = nameof(MaintenanceLog);
        public const string middleware_Provider = nameof(middleware_Provider);
        public const string middleware_Watermark = nameof(middleware_Watermark);
        public const string NLog = nameof(NLog);
        public const string SchemaVersions = nameof(SchemaVersions);
        public const string TravelArranger = nameof(TravelArranger);
        public const string TravelArrangerTraveller = nameof(TravelArrangerTraveller);
        public const string Traveller = nameof(Traveller);
        public const string TravellerCreditCard = nameof(TravellerCreditCard);
        public const string TravellerExtraInfo = nameof(TravellerExtraInfo);
        public const string TravellerMembership = nameof(TravellerMembership);
        public const string TravellerPassport = nameof(TravellerPassport);
        public const string TravellerVisa = nameof(TravellerVisa);
    }
}