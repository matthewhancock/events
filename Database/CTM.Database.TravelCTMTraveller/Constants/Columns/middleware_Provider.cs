using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class Middleware_Provider
    {
        public const string Enabled = nameof(Enabled);
        public const string Name = nameof(Name);
        public const string ProviderId = nameof(ProviderId);
        public const string Settings = nameof(Settings);
        public const string SourceTypeId = nameof(SourceTypeId);
    }
}