using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class TravellerPassport
    {
        public const string CountryOfBirth = nameof(CountryOfBirth);
        public const string CountryOfIssue = nameof(CountryOfIssue);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateOfBirth = nameof(DateOfBirth);
        public const string Enabled = nameof(Enabled);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string ExternalSourceId = nameof(ExternalSourceId);
        public const string ExternalTravellerId = nameof(ExternalTravellerId);
        public const string GenderCode = nameof(GenderCode);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IssueDate = nameof(IssueDate);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Nationality = nameof(Nationality);
        public const string PassportName = nameof(PassportName);
        public const string PassportNumber = nameof(PassportNumber);
        public const string PlaceOfBirth = nameof(PlaceOfBirth);
        public const string PlaceOfIssue = nameof(PlaceOfIssue);
        public const string SourceTypeId = nameof(SourceTypeId);
        public const string TravellerId = nameof(TravellerId);
    }
}