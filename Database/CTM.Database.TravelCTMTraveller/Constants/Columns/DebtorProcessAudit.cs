using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class DebtorProcessAudit
    {
        public const string CompletedProcessDate = nameof(CompletedProcessDate);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string DebtorProcessId = nameof(DebtorProcessId);
        public const string Enabled = nameof(Enabled);
        public const string ErrorMessage = nameof(ErrorMessage);
        public const string ExternalLabel = nameof(ExternalLabel);
        public const string ExternalSourceId = nameof(ExternalSourceId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string NumberOfDeletedProfiles = nameof(NumberOfDeletedProfiles);
        public const string NumberOfNewProfiles = nameof(NumberOfNewProfiles);
        public const string NumberOfProfilesRetrieved = nameof(NumberOfProfilesRetrieved);
        public const string NumberOfProfilesUpdated = nameof(NumberOfProfilesUpdated);
        public const string StartProcessDate = nameof(StartProcessDate);
        public const string StatusId = nameof(StatusId);
        public const string StatusText = nameof(StatusText);
    }
}