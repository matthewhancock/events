using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class __MigrationHistory
    {
        public const string ContextKey = nameof(ContextKey);
        public const string MigrationId = nameof(MigrationId);
        public const string Model = nameof(Model);
        public const string ProductVersion = nameof(ProductVersion);
    }
}