using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class CreditCard
    {
        public const string CardHolder = nameof(CardHolder);
        public const string CardNumber = nameof(CardNumber);
        public const string CardNumberDisplay = nameof(CardNumberDisplay);
        public const string CardSecurityCode = nameof(CardSecurityCode);
        public const string CardTypeCode = nameof(CardTypeCode);
        public const string CardTypeName = nameof(CardTypeName);
        public const string CostCentreCode = nameof(CostCentreCode);
        public const string CostCentreName = nameof(CostCentreName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorCode = nameof(DebtorCode);
        public const string DebtorCostCentreCreditCardId = nameof(DebtorCostCentreCreditCardId);
        public const string DebtorDepartmentCreditCardId = nameof(DebtorDepartmentCreditCardId);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string DepartmentCode = nameof(DepartmentCode);
        public const string DepartmentName = nameof(DepartmentName);
        public const string Enabled = nameof(Enabled);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string ExternalSourceId = nameof(ExternalSourceId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string SourceTypeID = nameof(SourceTypeID);
        public const string TravellerId = nameof(TravellerId);
    }
}