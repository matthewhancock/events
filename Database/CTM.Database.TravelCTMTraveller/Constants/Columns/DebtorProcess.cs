using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class DebtorProcess
    {
        public const string CompletedTravelArrangerProcessDate = nameof(CompletedTravelArrangerProcessDate);
        public const string CompletedTravellerProcessDate = nameof(CompletedTravellerProcessDate);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string DebtorTitle = nameof(DebtorTitle);
        public const string Enabled = nameof(Enabled);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string SabrePcc = nameof(SabrePcc);
        public const string StartTravelArrangerProcessDate = nameof(StartTravelArrangerProcessDate);
        public const string StartTravellerProcessDate = nameof(StartTravellerProcessDate);
    }
}