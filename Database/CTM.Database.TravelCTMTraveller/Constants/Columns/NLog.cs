using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class NLog
    {
        public const string Exception = nameof(Exception);
        public const string Id = nameof(Id);
        public const string Level = nameof(Level);
        public const string Logged = nameof(Logged);
        public const string Logger = nameof(Logger);
        public const string Message = nameof(Message);
    }
}