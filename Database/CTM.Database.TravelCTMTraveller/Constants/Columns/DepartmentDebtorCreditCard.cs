using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class DepartmentDebtorCreditCard
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorCreditCardId = nameof(DebtorCreditCardId);
        public const string DepartmentId = nameof(DepartmentId);
        public const string Enabled = nameof(Enabled);
        public const string ExternalDebtorCreditCardId = nameof(ExternalDebtorCreditCardId);
        public const string ExternalDepartmentId = nameof(ExternalDepartmentId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string SourceTypeId = nameof(SourceTypeId);
    }
}