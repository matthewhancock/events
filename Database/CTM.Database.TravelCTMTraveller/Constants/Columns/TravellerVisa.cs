using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class TravellerVisa
    {
        public const string CountryCode = nameof(CountryCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string ExternalPassportId = nameof(ExternalPassportId);
        public const string ExternalSourceId = nameof(ExternalSourceId);
        public const string ExternalTravellerId = nameof(ExternalTravellerId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IssueDate = nameof(IssueDate);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string SourceTypeID = nameof(SourceTypeID);
        public const string TravellerPassportId = nameof(TravellerPassportId);
        public const string Validity = nameof(Validity);
        public const string VisaNumber = nameof(VisaNumber);
        public const string VisaType = nameof(VisaType);
    }
}