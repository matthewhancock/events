using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class TravellerMembership
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string Enabled = nameof(Enabled);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string ExternalSourceId = nameof(ExternalSourceId);
        public const string ExternalTravellerId = nameof(ExternalTravellerId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string MembershipId = nameof(MembershipId);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Name = nameof(Name);
        public const string Program = nameof(Program);
        public const string SourceTypeId = nameof(SourceTypeId);
        public const string TravellerId = nameof(TravellerId);
        public const string Type = nameof(Type);
        public const string VendorCode = nameof(VendorCode);
    }
}