using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class Traveller
    {
        public const string CompanyName = nameof(CompanyName);
        public const string CostCentreCode = nameof(CostCentreCode);
        public const string CostCentreName = nameof(CostCentreName);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DateOfBirth = nameof(DateOfBirth);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string DepartmentCode = nameof(DepartmentCode);
        public const string DepartmentName = nameof(DepartmentName);
        public const string Email = nameof(Email);
        public const string EmployeeCode = nameof(EmployeeCode);
        public const string EmployeeNumber = nameof(EmployeeNumber);
        public const string Enabled = nameof(Enabled);
        public const string ExternalSourceId = nameof(ExternalSourceId);
        public const string FirstName = nameof(FirstName);
        public const string HomeAirportCode = nameof(HomeAirportCode);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string IsVipClient = nameof(IsVipClient);
        public const string LastName = nameof(LastName);
        public const string MobileNumber = nameof(MobileNumber);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string ProfileName = nameof(ProfileName);
        public const string Salutation = nameof(Salutation);
        public const string SourceTypeId = nameof(SourceTypeId);
        public const string TerminationDate = nameof(TerminationDate);
        public const string Title = nameof(Title);
        public const string VipClient = nameof(VipClient);
    }
}