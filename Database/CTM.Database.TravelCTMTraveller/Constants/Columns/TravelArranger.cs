using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class TravelArranger
    {
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorCode = nameof(DebtorCode);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Email = nameof(Email);
        public const string Enabled = nameof(Enabled);
        public const string ExternalSourceId = nameof(ExternalSourceId);
        public const string HasAllPermission = nameof(HasAllPermission);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string Mobile = nameof(Mobile);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string RoleType = nameof(RoleType);
        public const string SourceTypeId = nameof(SourceTypeId);
        public const string TerminationDate = nameof(TerminationDate);
        public const string Title = nameof(Title);
    }
}