using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class CostCentreDepartment
    {
        public const string CostCentreId = nameof(CostCentreId);
        public const string CreatedOn = nameof(CreatedOn);
        public const string ExternalCostCentreId = nameof(ExternalCostCentreId);
        public const string ExternalDepartmentId = nameof(ExternalDepartmentId);
        public const string Id = nameof(Id);
        public const string SourceTypeId = nameof(SourceTypeId);
    }
}