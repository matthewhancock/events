using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class Middleware_Watermark
    {
        public const string DebtorId = nameof(DebtorId);
        public const string ProviderId = nameof(ProviderId);
        public const string Type = nameof(Type);
        public const string WatermarkDate = nameof(WatermarkDate);
        public const string WatermarkId = nameof(WatermarkId);
    }
}