using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class TravellerCreditCard
    {
        public const string CardHolder = nameof(CardHolder);
        public const string CardNumberDisplay = nameof(CardNumberDisplay);
        public const string CardTypeCode = nameof(CardTypeCode);
        public const string CreatedBy = nameof(CreatedBy);
        public const string CreatedOn = nameof(CreatedOn);
        public const string DebtorIdentifier = nameof(DebtorIdentifier);
        public const string Enabled = nameof(Enabled);
        public const string ExpiryDate = nameof(ExpiryDate);
        public const string ExternalCreditCardId = nameof(ExternalCreditCardId);
        public const string ExternalSourceId = nameof(ExternalSourceId);
        public const string ExternalTravellerId = nameof(ExternalTravellerId);
        public const string Id = nameof(Id);
        public const string IsDeleted = nameof(IsDeleted);
        public const string ModifiedBy = nameof(ModifiedBy);
        public const string ModifiedOn = nameof(ModifiedOn);
        public const string Owner = nameof(Owner);
        public const string SourceTypeId = nameof(SourceTypeId);
        public const string TerminationDate = nameof(TerminationDate);
        public const string Token = nameof(Token);
        public const string TravellerId = nameof(TravellerId);
    }
}