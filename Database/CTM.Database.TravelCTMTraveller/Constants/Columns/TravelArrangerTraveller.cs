using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables.Constants.Columns
{
    public static class TravelArrangerTraveller
    {
        public const string CreatedOn = nameof(CreatedOn);
        public const string ExternalTravelArrangerId = nameof(ExternalTravelArrangerId);
        public const string ExternalTravellerId = nameof(ExternalTravellerId);
        public const string Id = nameof(Id);
        public const string SourceTypeId = nameof(SourceTypeId);
    }
}