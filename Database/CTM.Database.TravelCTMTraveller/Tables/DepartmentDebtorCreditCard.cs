using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.DepartmentDebtorCreditCard)]
    public partial class DepartmentDebtorCreditCard
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int? DebtorCreditCardId { get; set; }
        public int? DepartmentId { get; set; }
        public int ExternalDebtorCreditCardId { get; set; }
        public int ExternalDepartmentId { get; set; }
        public int SourceTypeId { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? Enabled { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
