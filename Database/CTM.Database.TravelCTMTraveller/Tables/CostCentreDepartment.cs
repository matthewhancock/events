using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.CostCentreDepartment)]
    public partial class CostCentreDepartment
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int ExternalDepartmentId { get; set; }
        public int ExternalCostCentreId { get; set; }
        public int SourceTypeId { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public int CostCentreId { get; set; }
    }
}
