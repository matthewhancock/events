using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.Traveller)]
    public partial class Traveller : Types.Entity
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string CostCentreCode { get; set; }
        public string CostCentreName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string MobileNumber { get; set; }
        public string DebtorIdentifier { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string HomeAirportCode { get; set; }
        public bool? IsVipClient { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeeCode { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? TerminationDate { get; set; }
        public string VipClient { get; set; }
        public string ProfileName { get; set; }
        public string Salutation { get; set; }
        public int ExternalSourceId { get; set; }
        public Guid? UmbrellaFacesID { get; set; }
    }
}
