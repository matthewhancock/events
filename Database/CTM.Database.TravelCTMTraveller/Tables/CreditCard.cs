using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.CreditCard)]
    public partial class CreditCard
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string CardHolder { get; set; }
        public string CardNumber { get; set; }
        public string CardSecurityCode { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }
        public string CardNumberDisplay { get; set; }
        public string CardTypeCode { get; set; }
        public string CardTypeName { get; set; }
        public string DebtorCode { get; set; }
        public string DebtorIdentifier { get; set; }
        public string DepartmentCode { get; set; }
        public string CostCentreCode { get; set; }
        public int? TravellerId { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool Enabled { get; set; }
        public string DepartmentName { get; set; }
        public int? DebtorDepartmentCreditCardId { get; set; }
        public string CostCentreName { get; set; }
        public int? DebtorCostCentreCreditCardId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? ExternalSourceId { get; set; }
        public int? SourceTypeID { get; set; }
    }
}
