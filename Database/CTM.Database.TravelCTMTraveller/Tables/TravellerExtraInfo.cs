using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.TravellerExtraInfo)]
    public partial class TravellerExtraInfo
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int TravellerId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool Enabled { get; set; }
    }
}
