using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.DebtorCreditCard)]
    public partial class DebtorCreditCard : Types.CreditCard
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public bool AllDepartmentsAndCostCentres { get; set; }
    }
}
