using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.CostCentre)]
    public partial class CostCentre
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string DebtorCode { get; set; }
        public string CostCentreCode { get; set; }
        public string CostCentreName { get; set; }
        public string DepartmentCode { get; set; }
        public int ExternalSourceId { get; set; }
        public int ExternalDepartmentId { get; set; }
        public int SourceTypeId { get; set; }
        public DateTime? TerminationDate { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? Enabled { get; set; }
        public bool? IsDeleted { get; set; }
        public int DepartmentId { get; set; }
    }
}
