using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.TravelArranger)]
    public partial class TravelArranger : Types.Entity
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Title { get; set; }
        public string DebtorCode { get; set; }
        public string DebtorIdentifier { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public DateTime? TerminationDate { get; set; }
        public bool? HasAllPermission { get; set; }
        public int RoleType { get; set; }
        public int ExternalSourceId { get; set; }
        public Guid? UmbrellaFacesID { get; set; }
    }
}
