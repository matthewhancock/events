using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.DebtorProcessAudit)]
    public partial class DebtorProcessAudit
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int DebtorProcessId { get; set; }
        public DateTimeOffset StartProcessDate { get; set; }
        public DateTimeOffset CompletedProcessDate { get; set; }
        public int NumberOfProfilesRetrieved { get; set; }
        public int NumberOfProfilesUpdated { get; set; }
        public int NumberOfNewProfiles { get; set; }
        public int StatusId { get; set; }
        public string StatusText { get; set; }
        public string ErrorMessage { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool Enabled { get; set; }
        public string DebtorIdentifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public int? NumberOfDeletedProfiles { get; set; }
        public string ExternalSourceId { get; set; }
        public string ExternalLabel { get; set; }
    }
}
