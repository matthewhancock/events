using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.TravellerMembership)]
    public partial class TravellerMembership : Types.Entity
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Type { get; set; }
        public string VendorCode { get; set; }
        public string Program { get; set; }
        public string MembershipId { get; set; }
        public string Name { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int TravellerId { get; set; }
        public int ExternalTravellerId { get; set; }
        public int ExternalSourceId { get; set; }
    }
}
