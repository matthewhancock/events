using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.CostCentreDebtorCreditCard)]
    public partial class CostCentreDebtorCreditCard
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int? DebtorCreditCardId { get; set; }
        public int? CostCentreId { get; set; }
        public int ExternalDebtorCreditCardId { get; set; }
        public int ExternalCostCentreId { get; set; }
        public int SourceTypeId { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? Enabled { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
