using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.middleware_Provider)]
    public partial class MiddlewareProvider
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int ProviderId { get; set; }
        public string Name { get; set; }
        public string Settings { get; set; }
        public int? SourceTypeId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public bool? Enabled { get; set; }
    }
}
