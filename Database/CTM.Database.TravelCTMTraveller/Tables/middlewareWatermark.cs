using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.middleware_Watermark)]
    public partial class MiddlewareWatermark
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int WatermarkId { get; set; }
        public int ProviderId { get; set; }
        public int DebtorId { get; set; }
        public string Type { get; set; }
        public DateTime WatermarkDate { get; set; }
    }
}
