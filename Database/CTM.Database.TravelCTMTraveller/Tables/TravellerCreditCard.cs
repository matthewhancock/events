using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.TravellerCreditCard)]
    public partial class TravellerCreditCard : Types.CreditCard
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Owner { get; set; }
        public int TravellerId { get; set; }
        public int ExternalTravellerId { get; set; }
    }
}
