using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.DebtorProcess)]
    public partial class DebtorProcess
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string DebtorTitle { get; set; }
        public string SabrePcc { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool Enabled { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public string DebtorIdentifier { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? StartTravellerProcessDate { get; set; }
        public DateTimeOffset? CompletedTravellerProcessDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] public DateTimeOffset? StartTravelArrangerProcessDate { get; set; }
        public DateTimeOffset? CompletedTravelArrangerProcessDate { get; set; }
    }
}
