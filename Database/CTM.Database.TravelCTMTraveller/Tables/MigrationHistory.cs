using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.__MigrationHistory)]
    public partial class MigrationHistory
    {
        [Key] public string MigrationId { get; set; }
        [Key] public string ContextKey { get; set; }
        public Byte[] Model { get; set; }
        public string ProductVersion { get; set; }
    }
}
