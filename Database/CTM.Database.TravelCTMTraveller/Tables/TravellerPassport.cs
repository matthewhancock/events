using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.TravellerPassport)]
    public partial class TravellerPassport : Types.Entity
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int TravellerId { get; set; }
        public string PassportNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Nationality { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? IssueDate { get; set; }
        public string PlaceOfIssue { get; set; }
        public string PassportName { get; set; }
        public string CountryOfBirth { get; set; }
        public string GenderCode { get; set; }
        public string CountryOfIssue { get; set; }
        public int ExternalTravellerId { get; set; }
        public string ExternalSourceId { get; set; }
    }
}
