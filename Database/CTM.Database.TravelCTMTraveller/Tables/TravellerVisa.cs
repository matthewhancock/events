using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.TravellerVisa)]
    public partial class TravellerVisa
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public int TravellerPassportId { get; set; }
        public string VisaNumber { get; set; }
        public string VisaType { get; set; }
        public string Validity { get; set; }
        public DateTimeOffset? IssueDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool Enabled { get; set; }
        public string CountryCode { get; set; }
        public string ExternalSourceId { get; set; }
        public int SourceTypeID { get; set; }
        public int ExternalTravellerId { get; set; }
        public int ExternalPassportId { get; set; }
    }
}
