using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CTM.Database.TravelCTMTraveller.Tables
{
    [Table(TravelCTMTraveller.Constants.Tables.TravelArrangerTraveller)]
    public partial class TravelArrangerTraveller
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public int ExternalTravellerId { get; set; }
        public int SourceTypeId { get; set; }
        public int ExternalTravelArrangerId { get; set; }
    }
}
