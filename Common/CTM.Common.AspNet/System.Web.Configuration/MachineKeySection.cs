﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CTM.Common.AspNet.System.Web.Configuration
{
    public static class MachineKeySection
    {
        // This API is obsolete because it is insecure: invalid hex chars are silently replaced with '0',
        // which can reduce the overall security of the system. But unfortunately, some code is dependent
        // on this broken behavior.
        public static byte[] HexStringToByteArray(String str)
        {
            if (((uint)str.Length & 0x1) == 0x1) // must be 2 nibbles per byte
            {
                return null;
            }
            byte[] ahexval = null; // s_ahexval; // initialize a table for faster lookups
            if (ahexval == null)
            {
                ahexval = new byte['f' + 1];
                for (int i = ahexval.Length; --i >= 0;)
                {
                    if ('0' <= i && i <= '9')
                    {
                        ahexval[i] = (byte)(i - '0');
                    }
                    else if ('a' <= i && i <= 'f')
                    {
                        ahexval[i] = (byte)(i - 'a' + 10);
                    }
                    else if ('A' <= i && i <= 'F')
                    {
                        ahexval[i] = (byte)(i - 'A' + 10);
                    }
                }

                //s_ahexval = ahexval;
            }

            byte[] result = new byte[str.Length / 2];
            int istr = 0, ir = 0;
            int n = result.Length;
            while (--n >= 0)
            {
                int c1, c2;
                try
                {
                    c1 = ahexval[str[istr++]];
                }
                catch (ArgumentNullException)
                {
                    c1 = 0;
                    return null;// Inavlid char
                }
                catch (ArgumentException)
                {
                    c1 = 0;
                    return null;// Inavlid char
                }
                catch (IndexOutOfRangeException)
                {
                    c1 = 0;
                    return null;// Inavlid char
                }

                try
                {
                    c2 = ahexval[str[istr++]];
                }
                catch (ArgumentNullException)
                {
                    c2 = 0;
                    return null;// Inavlid char
                }
                catch (ArgumentException)
                {
                    c2 = 0;
                    return null;// Inavlid char
                }
                catch (IndexOutOfRangeException)
                {
                    c2 = 0;
                    return null;// Inavlid char
                }

                result[ir++] = (byte)((c1 << 4) + c2);
            }

            return result;
        }


        //private static byte[] _ValidationKey = Convert.FromBase64String("0518965AC4986E0F89B6C34710C38DF00C641F4C153598B603B972B735844421C3F1057F6E672E464D2C87A7E5F7C71C34C79A336AB74C6C2158DA073946AC41");
        //private static byte[] _DecryptionKey = Convert.FromBase64String("40E2A7EEBD07FAE95FB09FCA02811B817CEB3759243EED062CD1883ACD493FAF");




        //private static bool _UsingCustomEncryption = false;

        //private static int _HashSize = HMACSHA256_HASH_SIZE;
        //private const int HMACSHA256_HASH_SIZE = 32;
        //private static bool _UseHMACSHA = true;


        //public static byte[] EncryptOrDecryptData(bool encrypt, byte[] buffer, bool useLegacyMode)
        //{
        //    return EncryptOrDecryptData(encrypt, buffer, null, 0, buffer.Length, false /* useValidationSymAlgo */, useLegacyMode, 0 /*IVType.None*/, false /*Sign*/);
        //}

        ////[Obsolete(OBSOLETE_CRYPTO_API_MESSAGE)]
        //public static byte[] EncryptOrDecryptData(bool fEncrypt, byte[] buf, byte[] modifier, int start, int length, bool useValidationSymAlgo, bool useLegacyMode, int /*IVType*/ ivType, bool signData)
        //{
        //    /* This algorithm is used to perform encryption or decryption of a buffer, along with optional signing (for encryption)
        //    * or signature verification (for decryption). Possible operation modes are:
        //    * 
        //    * ENCRYPT + SIGN DATA (fEncrypt = true, signData = true)
        //    * Input: buf represents plaintext to encrypt, modifier represents data to be appended to buf (but isn't part of the plaintext itself)
        //    * Output: E(iv + buf + modifier) + HMAC(E(iv + buf + modifier))
        //    * 
        //    * ONLY ENCRYPT DATA (fEncrypt = true, signData = false)
        //    * Input: buf represents plaintext to encrypt, modifier represents data to be appended to buf (but isn't part of the plaintext itself)
        //    * Output: E(iv + buf + modifier)
        //    * 
        //    * VERIFY + DECRYPT DATA (fEncrypt = false, signData = true)
        //    * Input: buf represents ciphertext to decrypt, modifier represents data to be removed from the end of the plaintext (since it's not really plaintext data)
        //    * Input (buf): E(iv + m + modifier) + HMAC(E(iv + m + modifier))
        //    * Output: m
        //    * 
        //    * ONLY DECRYPT DATA (fEncrypt = false, signData = false)
        //    * Input: buf represents ciphertext to decrypt, modifier represents data to be removed from the end of the plaintext (since it's not really plaintext data)
        //    * Input (buf): E(iv + plaintext + modifier)
        //    * Output: m
        //    * 
        //    * The 'iv' in the above descriptions isn't an actual IV. Rather, if ivType = IVType.Random, we'll prepend random bytes ('iv')
        //    * to the plaintext before feeding it to the crypto algorithms. Introducing randomness early in the algorithm prevents users
        //    * from inspecting two ciphertexts to see if the plaintexts are related. If ivType = IVType.None, then 'iv' is simply
        //    * an empty string. If ivType = IVType.Hash, we use a non-keyed hash of the plaintext.
        //    * 
        //    * The 'modifier' in the above descriptions is a piece of metadata that should be encrypted along with the plaintext but
        //    * which isn't actually part of the plaintext itself. It can be used for storing things like the user name for whom this
        //    * plaintext was generated, the page that generated the plaintext, etc. On decryption, the modifier parameter is compared
        //    * against the modifier stored in the crypto stream, and it is stripped from the message before the plaintext is returned.
        //    * 
        //    * In all cases, if something goes wrong (e.g. invalid padding, invalid signature, invalid modifier, etc.), a generic exception is thrown.
        //    */

        //    //try
        //    //{
        //    //EnsureConfig();

        //    //if (!fEncrypt && signData)
        //    //{
        //    //    if (start != 0 || length != buf.Length)
        //    //    {
        //    //        // These transformations assume that we're operating on buf in its entirety and
        //    //        // not on any subset of buf, so we'll just replace buf with the particular subset
        //    //        // we're interested in.
        //    //        byte[] bTemp = new byte[length];
        //    //        Buffer.BlockCopy(buf, start, bTemp, 0, length);
        //    //        buf = bTemp;
        //    //        start = 0;
        //    //    }

        //    //    // buf actually contains E(iv + m + modifier) + HMAC(E(iv + m + modifier)), so we need to verify and strip off the signature
        //    //    buf = GetUnHashedData(buf);
        //    //    // At this point, buf contains only E(iv + m + modifier) if the signature check succeeded.

        //    //    if (buf == null)
        //    //    {
        //    //        // signature verification failed
        //    //        throw new HttpException(SR.GetString(SR.Unable_to_validate_data));
        //    //    }

        //    //    // need to fix up again since GetUnhashedData() returned a different array
        //    //    length = buf.Length;
        //    //}

        //    if (useLegacyMode)
        //        useLegacyMode = _UsingCustomEncryption; // only use legacy mode for custom algorithms

        //    var ms = new global::System.IO.MemoryStream();
        //    var cryptoTransform = GetCryptoTransform(fEncrypt, useValidationSymAlgo, useLegacyMode);
        //    CryptoStream cs = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Write);

        //    // DevDiv Bugs 137864: Add IV to beginning of data to be encrypted.
        //    // IVType.None is used by MembershipProvider which requires compatibility even in SP2 mode (and will set signData = false).
        //    // MSRC 10405: If signData is set to true, we must generate an IV.
        //    bool createIV = false; // signData || ((ivType != IVType.None) && (CompatMode > MachineKeyCompatibilityMode.Framework20SP1));

        //    //if (fEncrypt && createIV)
        //    //{
        //    //    int ivLength = (useValidationSymAlgo ? _IVLengthValidation : _IVLengthDecryption);
        //    //    byte[] iv = null;

        //    //    switch (ivType)
        //    //    {
        //    //        case IVType.Hash:
        //    //            // iv := H(buf)
        //    //            iv = GetIVHash(buf, ivLength);
        //    //            break;

        //    //        case IVType.Random:
        //    //            // iv := [random]
        //    //            iv = new byte[ivLength];
        //    //            RandomNumberGenerator.GetBytes(iv);
        //    //            break;
        //    //    }

        //    //    Debug.Assert(iv != null, "Invalid value for IVType: " + ivType.ToString("G"));
        //    //    cs.Write(iv, 0, iv.Length);
        //    //}

        //    cs.Write(buf, start, length);
        //    if (fEncrypt && modifier != null)
        //    {
        //        cs.Write(modifier, 0, modifier.Length);
        //    }

        //    cs.FlushFinalBlock();
        //    byte[] paddedData = ms.ToArray();

        //    // At this point:
        //    // If fEncrypt = true (encrypting), paddedData := Enc(iv + buf + modifier)
        //    // If fEncrypt = false (decrypting), paddedData := iv + plaintext + modifier

        //    byte[] bData;
        //    cs.Close();

        //    // In ASP.NET 2.0, we pool ICryptoTransform objects, and this returns that ICryptoTransform
        //    // to the pool. In ASP.NET 4.0, this just disposes of the ICryptoTransform object.
        //    ReturnCryptoTransform(fEncrypt, cryptoTransform, useValidationSymAlgo, useLegacyMode);

        //    // DevDiv Bugs 137864: Strip IV from beginning of unencrypted data
        //    //if (!fEncrypt && createIV)
        //    //{
        //    //    // strip off the first bytes that were random bits
        //    //    int ivLength = (useValidationSymAlgo ? _IVLengthValidation : _IVLengthDecryption);
        //    //    int bDataLength = paddedData.Length - ivLength;
        //    //    if (bDataLength < 0)
        //    //    {
        //    //        throw new HttpException(SR.GetString(SR.Unable_to_validate_data));
        //    //    }

        //    //    bData = new byte[bDataLength];
        //    //    Buffer.BlockCopy(paddedData, ivLength, bData, 0, bDataLength);
        //    //}
        //    //else
        //    //{
        //    bData = paddedData;
        //    //}

        //    // At this point:
        //    // If fEncrypt = true (encrypting), bData := Enc(iv + buf + modifier)
        //    // If fEncrypt = false (decrypting), bData := plaintext + modifier

        //    if (!fEncrypt && modifier != null && modifier.Length > 0)
        //    {
        //        // MSRC 10405: Crypto board suggests blinding where signature failed
        //        // to prevent timing attacks.
        //        //bool modifierCheckFailed = false;
        //        //for (int iter = 0; iter < modifier.Length; iter++)
        //        //    if (bData[bData.Length - modifier.Length + iter] != modifier[iter])
        //        //        modifierCheckFailed = true;
        //        //if (modifierCheckFailed)
        //        //{
        //        //    throw new HttpException(SR.GetString(SR.Unable_to_validate_data));
        //        //}

        //        byte[] bData2 = new byte[bData.Length - modifier.Length];
        //        Buffer.BlockCopy(bData, 0, bData2, 0, bData2.Length);
        //        bData = bData2;
        //    }

        //    // At this point:
        //    // If fEncrypt = true (encrypting), bData := Enc(iv + buf + modifier)
        //    // If fEncrypt = false (decrypting), bData := plaintext

        //    //if (fEncrypt && signData)
        //    //{
        //    //    byte[] hmac = HashData(bData, null, 0, bData.Length);
        //    //    byte[] bData2 = new byte[bData.Length + hmac.Length];

        //    //    Buffer.BlockCopy(bData, 0, bData2, 0, bData.Length);
        //    //    Buffer.BlockCopy(hmac, 0, bData2, bData.Length, hmac.Length);
        //    //    bData = bData2;
        //    //}

        //    // At this point:
        //    // If fEncrypt = true (encrypting), bData := Enc(iv + buf + modifier) + HMAC(Enc(iv + buf + modifier))
        //    // If fEncrypt = false (decrypting), bData := plaintext

        //    // And we're done
        //    return bData;
        //    //}
        //    //catch
        //    //{
        //    //    // It's important that we don't propagate the original exception here as we don't want a production
        //    //    // server which has unintentionally left YSODs enabled to leak cryptographic information.
        //    //    throw new HttpException(SR.GetString(SR.Unable_to_validate_data));
        //    //}
        //}



        ////[Obsolete(OBSOLETE_CRYPTO_API_MESSAGE)]
        //internal static byte[] GetUnHashedData(byte[] bufHashed)
        //{
        //    //if (!VerifyHashedData(bufHashed))
        //    //    return null;

        //    byte[] buf2 = new byte[bufHashed.Length - _HashSize];
        //    Buffer.BlockCopy(bufHashed, 0, buf2, 0, buf2.Length);
        //    return buf2;

        //}
        ////[Obsolete(OBSOLETE_CRYPTO_API_MESSAGE)]
        ////internal static byte[] HashData(byte[] buf, byte[] modifier, int start, int length)
        ////{
        ////    //EnsureConfig();

        ////    //if (s_config.Validation == MachineKeyValidation.MD5)
        ////    //    return HashDataUsingNonKeyedAlgorithm(null, buf, modifier, start, length, s_validationKey);
        ////    if (_UseHMACSHA)
        ////    {
        ////        byte[] hash = GetHMACSHA1Hash(buf, modifier, start, length);
        ////        if (hash != null)
        ////            return hash;
        ////    }
        ////    if (_CustomValidationTypeIsKeyed)
        ////    {
        ////        return HashDataUsingKeyedAlgorithm(KeyedHashAlgorithm.Create(_CustomValidationName),
        ////                                           buf, modifier, start, length, s_validationKey);
        ////    }
        ////    else
        ////    {
        ////        return HashDataUsingNonKeyedAlgorithm(HashAlgorithm.Create(_CustomValidationName),
        ////                                              buf, modifier, start, length, s_validationKey);
        ////    }
        ////}


        ////private static byte[] GetHMACSHA1Hash(byte[] buf, byte[] modifier, int start, int length)
        ////{

        ////    //if (start < 0 || start > buf.Length)

        ////    //    throw new ArgumentException(SR.GetString(SR.InvalidArgumentValue, "start"));

        ////    //if (length < 0 || buf == null || (start + length) > buf.Length)

        ////    //    throw new ArgumentException(SR.GetString(SR.InvalidArgumentValue, "length"));

        ////    byte[] hash = new byte[_HashSize];

        ////    int hr = UnsafeNativeMethods.GetHMACSHA1Hash(buf, start, length,

        ////                                                 modifier, (modifier == null) ? 0 : modifier.Length,

        ////                                                 s_inner, s_inner.Length, s_outer, s_outer.Length,

        ////                                                 hash, hash.Length);

        ////    if (hr == 0)

        ////        return hash;

        ////    _UseHMACSHA = false;

        ////    return null;

        ////}


        //private static void ReturnCryptoTransform(bool fEncrypt, ICryptoTransform ct, bool useValidationSymAlgo, bool legacyMode)
        //{
        //    ct.Dispose();
        //}
        //private static ICryptoTransform GetCryptoTransform(bool fEncrypt, bool useValidationSymAlgo, bool legacyMode)
        //{
        //    SymmetricAlgorithm algo = (legacyMode ? /*s_oSymAlgoLegacy*/ new AesCryptoServiceProvider() /*CryptoAlgorithms.CreateAes()*/ : (useValidationSymAlgo ? /*s_oSymAlgoValidation*/  new AesCryptoServiceProvider() /*CryptoAlgorithms.CreateAes()*/ : /* s_oSymAlgoDecryption */  new AesCryptoServiceProvider() /*CryptoAlgorithms.CreateAes()*/));
        //    lock (algo)
        //        return (fEncrypt ? algo.CreateEncryptor() : algo.CreateDecryptor());
        //}
    }
}
