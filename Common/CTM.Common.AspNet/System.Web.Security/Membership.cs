﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace CTM.Common.AspNet.System.Web.Security
{
    // https://github.com/Microsoft/referencesource/blob/master/System.Web/Security/Membership.cs
    public static class Membership
    {
        private static char[] punctuations = "!@#$%^&*()_-+=[{]};:>|./?".ToCharArray();
        public static string GeneratePassword(int length, int numberOfNonAlphanumericCharacters)
        {
            //if (length < 1 || length > 128)
            //{
            //    throw new ArgumentException(SR.GetString(SR.Membership_password_length_incorrect));
            //}

            //if (numberOfNonAlphanumericCharacters > length || numberOfNonAlphanumericCharacters < 0)
            //{
            //    throw new ArgumentException(SR.GetString(SR.Membership_min_required_non_alphanumeric_characters_incorrect,
            //    "numberOfNonAlphanumericCharacters"));
            //}

            string password;
            //int index;
            byte[] buf;
            char[] cBuf;
            int count;

            //do
            //{
            buf = new byte[length];
            cBuf = new char[length];
            count = 0;

            (new RNGCryptoServiceProvider()).GetBytes(buf);

            for (int iter = 0; iter < length; iter++)
            {
                int i = (int)(buf[iter] % 87);
                if (i < 10)
                    cBuf[iter] = (char)('0' + i);
                else if (i < 36)
                    cBuf[iter] = (char)('A' + i - 10);
                else if (i < 62)
                    cBuf[iter] = (char)('a' + i - 36);
                else
                {
                    cBuf[iter] = punctuations[i - 62];
                    count++;
                }
            }

            if (count < numberOfNonAlphanumericCharacters)
            {
                int j, k;
                Random rand = new Random();

                for (j = 0; j < numberOfNonAlphanumericCharacters - count; j++)
                {
                    do
                    {
                        k = rand.Next(0, length);
                    }
                    while (!Char.IsLetterOrDigit(cBuf[k]));

                    cBuf[k] = punctuations[rand.Next(0, punctuations.Length)];
                }
            }

            password = new string(cBuf);
            //}
            //while (CrossSiteScriptingValidation.IsDangerousString(password, out index));

            return password;
        }

    }
}
