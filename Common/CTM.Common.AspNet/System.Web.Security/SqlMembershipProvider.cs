﻿using CTM.Common.AspNet.System.Web.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CTM.Common.AspNet.System.Web.Security
{
    public class SqlMembershipProvider
    {

        internal const int SALT_SIZE = 16;
        public static string GenerateSalt()
        {
            byte[] buf = new byte[SALT_SIZE];
            (new RNGCryptoServiceProvider()).GetBytes(buf);
            return Convert.ToBase64String(buf);

        }
        public static string EncodePassword(string Password, string Salt)
        {
            var bPassword = Encoding.Unicode.GetBytes(Password);
            var bSalt = Convert.FromBase64String(Salt);

            byte[] bAll = new byte[bSalt.Length + bPassword.Length];
            Buffer.BlockCopy(bSalt, 0, bAll, 0, bSalt.Length);
            Buffer.BlockCopy(bPassword, 0, bAll, bSalt.Length, bPassword.Length);

            using (var encryptor = Aes.Create())
            {
                encryptor.Key = MachineKeySection.HexStringToByteArray("40E2A7EEBD07FAE95FB09FCA02811B817CEB3759243EED062CD1883ACD493FAF");
                encryptor.IV = bSalt;
                //Array.Copy(bPassword, encryptor.IV, 16);

                encryptor.Mode = CipherMode.CBC;
                encryptor.Padding = PaddingMode.PKCS7;

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bAll, 0, bAll.Length);
                        cs.Close();
                    }

                    return Convert.ToBase64String(ms.ToArray());
                }
            }
        }


        public static Guid CreateUser(SqlConnection Connection, string Username, string EmailAddress, bool IsApproved)
        {
            var membershipUserID = Guid.NewGuid();
            var password = Membership.GeneratePassword(8, 1);
            var salt = SqlMembershipProvider.GenerateSalt();

            // aspnet Membership
            using (var cmd = new SqlCommand("dbo.aspnet_Membership_CreateUser", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ApplicationName", "/");
                cmd.Parameters.AddWithValue("@UserName", Username);
                cmd.Parameters.AddWithValue("@Password", EncodePassword(password, salt));
                cmd.Parameters.AddWithValue("@PasswordSalt", salt);
                cmd.Parameters.AddWithValue("@Email", EmailAddress);
                cmd.Parameters.AddWithValue("@PasswordQuestion", "What Website is this?");
                cmd.Parameters.AddWithValue("@PasswordAnswer", EncodePassword("TravelCTM".ToLower(CultureInfo.InvariantCulture), salt));
                cmd.Parameters.AddWithValue("@IsApproved", IsApproved);
                cmd.Parameters.AddWithValue("@UniqueEmail", true);
                cmd.Parameters.AddWithValue("@PasswordFormat", 2); // encrypted
                cmd.Parameters.AddWithValue("@CurrentTimeUtc", DateTime.UtcNow);
                var p = new SqlParameter("@UserId", membershipUserID);
                p.Direction = ParameterDirection.InputOutput;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();
            }

            return membershipUserID;
        }


        //        private HashAlgorithm GetHashAlgorithm()
        //        {
        //            //if (s_HashAlgorithm != null)
        //            //    return HashAlgorithm.Create(s_HashAlgorithm);

        //            //string temp = Membership.HashAlgorithmType;
        //            //if (_LegacyPasswordCompatibilityMode == MembershipPasswordCompatibilityMode.Framework20 && !Membership.IsHashAlgorithmFromMembershipConfig && temp != "MD5")

        //            //{

        //            //    temp = "SHA1";

        //            //}

        //            //HashAlgorithm hashAlgo = HashAlgorithm.Create(temp);

        //            //if (hashAlgo == null)

        //            //    RuntimeConfig.GetAppConfig().Membership.ThrowHashAlgorithmException();

        //            //s_HashAlgorithm = temp;

        //            //return hashAlgo;

        //            return HashAlgorithm.Create("SHA1");
        //        }



        //        public static string EncodePassword(string pass, int passwordFormat, string salt)
        //        {
        //            if (passwordFormat == 0) // MembershipPasswordFormat.Clear
        //                return pass;

        //            byte[] bIn = Encoding.Unicode.GetBytes(pass);
        //            byte[] bSalt = Convert.FromBase64String(salt);
        //            byte[] bRet = null;

        //            //if (passwordFormat == 1)
        //            //{ // MembershipPasswordFormat.Hashed
        //            //    HashAlgorithm hm = GetHashAlgorithm();
        //            //    if (hm is KeyedHashAlgorithm)
        //            //    {
        //            //        KeyedHashAlgorithm kha = (KeyedHashAlgorithm)hm;
        //            //        if (kha.Key.Length == bSalt.Length)
        //            //        {
        //            //            kha.Key = bSalt;
        //            //        }
        //            //        else if (kha.Key.Length < bSalt.Length)
        //            //        {
        //            //            byte[] bKey = new byte[kha.Key.Length];
        //            //            Buffer.BlockCopy(bSalt, 0, bKey, 0, bKey.Length);
        //            //            kha.Key = bKey;
        //            //        }
        //            //        else
        //            //        {
        //            //            byte[] bKey = new byte[kha.Key.Length];
        //            //            for (int iter = 0; iter < bKey.Length;)
        //            //            {
        //            //                int len = Math.Min(bSalt.Length, bKey.Length - iter);
        //            //                Buffer.BlockCopy(bSalt, 0, bKey, iter, len);
        //            //                iter += len;
        //            //            }
        //            //            kha.Key = bKey;
        //            //        }
        //            //        bRet = kha.ComputeHash(bIn);
        //            //    }
        //            //    else
        //            //    {
        //            //        byte[] bAll = new byte[bSalt.Length + bIn.Length];
        //            //        Buffer.BlockCopy(bSalt, 0, bAll, 0, bSalt.Length);
        //            //        Buffer.BlockCopy(bIn, 0, bAll, bSalt.Length, bIn.Length);
        //            //        bRet = hm.ComputeHash(bAll);
        //            //    }
        //            //}
        //            //else
        //            //{
        //                byte[] bAll = new byte[bSalt.Length + bIn.Length];
        //                Buffer.BlockCopy(bSalt, 0, bAll, 0, bSalt.Length);
        //                Buffer.BlockCopy(bIn, 0, bAll, bSalt.Length, bIn.Length);
        //                //bRet = EncryptPassword(bAll, MembershipPasswordCompatibilityMode.Framework20);
        //                bRet = EncryptOrDecryptData(true, bAll, true);
        //            //}

        //            return Convert.ToBase64String(bRet);
        //        }


        //        public static byte[] EncryptOrDecryptData(bool encrypt, byte[] buffer, bool useLegacyMode)
        //        {
        //            // DevDiv Bugs 137864: Use IVType.None for compatibility with stored passwords even after SP20 compat mode enabled.
        //            // This is the ONLY case IVType.None should be used.

        //            // We made changes to how encryption takes place in response to MSRC 10405. Membership needs to opt-out of
        //            // these changes (by setting signData to false) to preserve back-compat with existing databases.
        //#pragma warning disable 618 // calling obsolete methods
        //            return MachineKeySection.EncryptOrDecryptData(encrypt, buffer, (byte[])null, 0, buffer.Length, false /* useValidationSymAlgo */, useLegacyMode, 0 /*IVType.None*/, false /*Sign*/);
        //#pragma warning restore 618 // calling obsolete methods
        //        }


        //    public override MembershipUser CreateUser(string username,
        //string password,
        //string email,
        //string passwordQuestion,
        //string passwordAnswer,
        //bool isApproved,
        //object providerUserKey) //,
        //                        //out MembershipCreateStatus status)
        //    {
        //        //if (!SecUtility.ValidateParameter(ref password, true, true, false, 128))
        //        //{
        //        //    status = MembershipCreateStatus.InvalidPassword;
        //        //    return null;
        //        //}

        //        string salt = GenerateSalt();
        //        string pass = EncodePassword(password, 2, salt); // MembershipPasswordFormat.Encrypted // (int)_PasswordFormat, salt);
        //        //if (pass.Length > 128)
        //        //{
        //        //    status = MembershipCreateStatus.InvalidPassword;
        //        //    return null;
        //        //}

        //        //string encodedPasswordAnswer;
        //        //if (passwordAnswer != null)
        //        //{
        //        //    passwordAnswer = passwordAnswer.Trim();
        //        //}

        //        //if (!string.IsNullOrEmpty(passwordAnswer))
        //        //{
        //        //    if (passwordAnswer.Length > 128)
        //        //    {
        //        //        status = MembershipCreateStatus.InvalidAnswer;
        //        //        return null;
        //        //    }
        //        //    encodedPasswordAnswer = EncodePassword(passwordAnswer.ToLower(CultureInfo.InvariantCulture), (int)_PasswordFormat, salt);
        //        //}
        //        //else
        //        //    encodedPasswordAnswer = passwordAnswer;
        //        //if (!SecUtility.ValidateParameter(ref encodedPasswordAnswer, RequiresQuestionAndAnswer, true, false, 128))
        //        //{
        //        //    status = MembershipCreateStatus.InvalidAnswer;
        //        //    return null;
        //        //}

        //        //if (!SecUtility.ValidateParameter(ref username, true, true, true, 256))
        //        //{
        //        //    status = MembershipCreateStatus.InvalidUserName;
        //        //    return null;
        //        //}

        //        //if (!SecUtility.ValidateParameter(ref email,
        //        //RequiresUniqueEmail,
        //        //RequiresUniqueEmail,
        //        //false,
        //        //256))
        //        //{
        //        //    status = MembershipCreateStatus.InvalidEmail;
        //        //    return null;
        //        //}

        //        //if (!SecUtility.ValidateParameter(ref passwordQuestion, RequiresQuestionAndAnswer, true, false, 256))
        //        //{
        //        //    status = MembershipCreateStatus.InvalidQuestion;
        //        //    return null;
        //        //}

        //        //if (providerUserKey != null)
        //        //{
        //        //    if (!(providerUserKey is Guid))
        //        //    {
        //        //        status = MembershipCreateStatus.InvalidProviderUserKey;
        //        //        return null;
        //        //    }
        //        //}

        //        //if (password.Length < MinRequiredPasswordLength)
        //        //{
        //        //    status = MembershipCreateStatus.InvalidPassword;
        //        //    return null;
        //        //}

        //        //int count = 0;

        //        //for (int i = 0; i < password.Length; i++)
        //        //{
        //        //    if (!char.IsLetterOrDigit(password, i))
        //        //    {
        //        //        count++;
        //        //    }
        //        //}

        //        //if (count < MinRequiredNonAlphanumericCharacters)
        //        //{
        //        //    status = MembershipCreateStatus.InvalidPassword;
        //        //    return null;
        //        //}

        //        //if (PasswordStrengthRegularExpression.Length > 0)
        //        //{
        //        //    if (!RegexUtil.IsMatch(password, PasswordStrengthRegularExpression, RegexOptions.None, _passwordStrengthRegexTimeout))
        //        //    {
        //        //        status = MembershipCreateStatus.InvalidPassword;
        //        //        return null;
        //        //    }
        //        //}

        //        //ValidatePasswordEventArgs e = new ValidatePasswordEventArgs(username, password, true);
        //        //OnValidatingPassword(e);

        //        //if (e.Cancel)
        //        //{
        //        //    status = MembershipCreateStatus.InvalidPassword;
        //        //    return null;
        //        //}

        //        try
        //        {
        //            SqlConnectionHolder holder = null;
        //            try
        //            {
        //                holder = SqlConnectionHelper.GetConnection(_sqlConnectionString, true);
        //                CheckSchemaVersion(holder.Connection);

        //                DateTime dt = RoundToSeconds(DateTime.UtcNow);
        //                SqlCommand cmd = new SqlCommand("dbo.aspnet_Membership_CreateUser", holder.Connection);

        //                cmd.CommandTimeout = CommandTimeout;
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.Add(CreateInputParam("@ApplicationName", SqlDbType.NVarChar, ApplicationName));
        //                cmd.Parameters.Add(CreateInputParam("@UserName", SqlDbType.NVarChar, username));
        //                cmd.Parameters.Add(CreateInputParam("@Password", SqlDbType.NVarChar, pass));
        //                cmd.Parameters.Add(CreateInputParam("@PasswordSalt", SqlDbType.NVarChar, salt));
        //                cmd.Parameters.Add(CreateInputParam("@Email", SqlDbType.NVarChar, email));
        //                cmd.Parameters.Add(CreateInputParam("@PasswordQuestion", SqlDbType.NVarChar, passwordQuestion));
        //                cmd.Parameters.Add(CreateInputParam("@PasswordAnswer", SqlDbType.NVarChar, encodedPasswordAnswer));
        //                cmd.Parameters.Add(CreateInputParam("@IsApproved", SqlDbType.Bit, isApproved));
        //                cmd.Parameters.Add(CreateInputParam("@UniqueEmail", SqlDbType.Int, RequiresUniqueEmail ? 1 : 0));
        //                cmd.Parameters.Add(CreateInputParam("@PasswordFormat", SqlDbType.Int, (int)PasswordFormat));
        //                cmd.Parameters.Add(CreateInputParam("@CurrentTimeUtc", SqlDbType.DateTime, dt));
        //                SqlParameter p = CreateInputParam("@UserId", SqlDbType.UniqueIdentifier, providerUserKey);
        //                p.Direction = ParameterDirection.InputOutput;
        //                cmd.Parameters.Add(p);

        //                p = new SqlParameter("@ReturnValue", SqlDbType.Int);
        //                p.Direction = ParameterDirection.ReturnValue;
        //                cmd.Parameters.Add(p);

        //                try
        //                {
        //                    cmd.ExecuteNonQuery();
        //                }
        //                catch (SqlException sqlEx)
        //                {
        //                    if (sqlEx.Number == 2627 || sqlEx.Number == 2601 || sqlEx.Number == 2512)
        //                    {
        //                        status = MembershipCreateStatus.DuplicateUserName;
        //                        return null;
        //                    }
        //                    throw;
        //                }
        //                int iStatus = ((p.Value != null) ? ((int)p.Value) : -1);
        //                if (iStatus < 0 || iStatus > (int)MembershipCreateStatus.ProviderError)
        //                    iStatus = (int)MembershipCreateStatus.ProviderError;
        //                status = (MembershipCreateStatus)iStatus;
        //                if (iStatus != 0) // !success
        //                    return null;

        //                providerUserKey = new Guid(cmd.Parameters["@UserId"].Value.ToString());
        //                dt = dt.ToLocalTime();
        //                return new MembershipUser(this.Name,
        //                username,
        //                providerUserKey,
        //                email,
        //                passwordQuestion,
        //                null,
        //                isApproved,
        //                false,
        //                dt,
        //                dt,
        //                dt,
        //                dt,
        //                new DateTime(1754, 1, 1));
        //            }
        //            finally
        //            {
        //                if (holder != null)
        //                {
        //                    holder.Close();
        //                    holder = null;
        //                }
        //            }
        //        }
        //        catch
        //        {
        //            throw;
        //        }
        //    }


        //    private SqlParameter CreateInputParam(string paramName,

        //                                           SqlDbType dbType,

        //                                           object objValue)

        //    {



        //        SqlParameter param = new SqlParameter(paramName, dbType);



        //        if (objValue == null)

        //        {

        //            param.IsNullable = true;

        //            param.Value = DBNull.Value;

        //        }

        //        else

        //        {

        //            param.Value = objValue;

        //        }



        //        return param;

        //    }
    }
}
