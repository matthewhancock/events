﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CTM.Common.AspNet.System.Web.Security
{
    public static class SqlRoleProvider
    {
        // https://github.com/Microsoft/referencesource/blob/3b1eaf5203992df69de44c783a3eda37d3d4cd10/System.Web/Security/SQLRoleProvider.cs
        public static void AddUsersToRolesCore(SqlConnection Connection, string Usernames, string Roles)
        {
            using (var cmd = new SqlCommand("dbo.aspnet_UsersInRoles_AddUsersToRoles", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ApplicationName", "/");
                cmd.Parameters.AddWithValue("@RoleNames", Roles);
                cmd.Parameters.AddWithValue("@UserNames", Usernames);
                cmd.Parameters.AddWithValue("@CurrentTimeUtc", DateTime.UtcNow);
                //var p = new SqlParameter("@ReturnValue", SqlDbType.Int) { Direction = ParameterDirection.ReturnValue };
                //cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();
            }
        }
        //public static void AddUsersToRolesCore(DbContext Context, string Usernames, string Roles)
        //{
        //    Context.Database.ExecuteSqlCommand("dbo.aspnet_UsersInRoles_AddUsersToRoles @ApplicationName, @RoleNames, @UserNames, @CurrentTimeUtc", new object[] { "/", Roles, Usernames, DateTime.UtcNow });
        //}
    }
}
