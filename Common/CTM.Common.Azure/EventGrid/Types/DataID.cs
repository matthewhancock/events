﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Common.Azure.EventGrid.Types
{
    public class DataID
    {
        public string ID { get; set; }
    }
}
