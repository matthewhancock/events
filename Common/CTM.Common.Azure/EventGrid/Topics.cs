﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Common.Azure.EventGrid
{
    public static class Topics
    {
        public static string Parse(string FullPath)
        {
            var topic = FullPath.Substring(FullPath.LastIndexOf('/') + 1);
            topic = topic.Substring(0, topic.LastIndexOf('-'));

            return topic;
        }

        public static class External
        {
            public const string UmbrellaFaces = "external-umbrellafaces";
            public const string Riskline = "external-riskline";
        }
    }
}
