﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Common.Azure.EventGrid
{
    // https://cecilphillip.com/sending-customevents-to-azureeventgrid/
    public class Event<T> where T : class
    {
        public string ID { get; set; }
        public string Subject { get; set; }
        public string EventType { get; set; }
        public T Data { get; set; }
        public DateTime EventTime { get; set; }
    }
}
