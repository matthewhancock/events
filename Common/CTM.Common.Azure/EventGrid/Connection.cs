﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CTM.Common.Azure.EventGrid
{
    public class Connection<T> where T : class
    {
        private string _endpoint;
        private string _key;
        public Connection(string Endpoint, string Key)
        {
            _endpoint = Endpoint;
            _key = Key;
        }

        public async Task Send(string Subject, string EventType, T Data)
        {
            var e = new Event<T>()
            {
                ID = Guid.NewGuid().ToString(),
                EventTime = DateTime.UtcNow,

                Subject = Subject,
                EventType = EventType,
                Data = Data
            };

            await Send(e);
        }
        public async Task Send(Event<T> Event)
        {
            await Send(new[] { Event });
        }

        public async Task Send(IEnumerable<Event<T>> Events)
        {
            using (var c = new HttpClient())
            {
                c.DefaultRequestHeaders.Add(Constants.Headers.SasKey, _key);

                var request = new HttpRequestMessage(HttpMethod.Post, _endpoint)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(Events), Encoding.UTF8, Util.Http.MimeTypes.Json)
                };

                var response = await c.SendAsync(request);
            }
        }
    }
}
