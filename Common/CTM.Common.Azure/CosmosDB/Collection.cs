﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CTM.Common.Azure.CosmosDB
{
    public class Collection<T> where T : Document
    {
        public Collection(string Name, Database Database)
        {
            this.Name = Name;
            Client = Database.Client;

            Database.Client.CreateDocumentCollectionIfNotExistsAsync(Database.Uri, new DocumentCollection() { Id = Name }).Wait();

            DocumentCollectionUri = UriFactory.CreateDocumentCollectionUri(Database.Name, Name);
            DocumentUri = (id) => UriFactory.CreateDocumentUri(Database.Name, Name, id);
        }
        private DocumentClient Client { get; }
        private Uri DocumentCollectionUri { get; }
        private Func<string, Uri> DocumentUri { get; }
        public string Name { get; }


        public async Task Insert(IEnumerable<T> Items)
        {
            foreach (var i in Items)
            {
                await Client.CreateDocumentAsync(DocumentCollectionUri, i);
            }
        }
        public async Task Insert(T Item)
        {
            await Client.CreateDocumentAsync(DocumentCollectionUri, Item);
        }

        public async Task Update(T Item)
        {
            await Client.ReplaceDocumentAsync(DocumentUri(Item.Id), Item);
        }

        public async Task Delete(string ID)
        {
            try
            {
                await Client.DeleteDocumentAsync(DocumentUri(ID));
            }
            catch (Exception)
            {
                // will fail if file doesn't exist
            }
        }

        public async Task Upsert(IEnumerable<T> Items)
        {
            foreach (var i in Items)
            {
                // This code is failing for some reason, need to replicate
                //await Client.UpsertDocumentAsync(DocumentUri(i.Id), i);

                try
                {
                    // throws error if document doesn't exist...
                    await Client.ReadDocumentAsync(DocumentUri(i.Id));
                    await Update(i);
                }
                catch
                {
                    await Insert(i);
                }
            }
        }
        public async Task Upsert(T Item)
        {
            try
            {
                // throws error if document doesn't exist...
                await Client.ReadDocumentAsync(DocumentUri(Item.Id));
                await Update(Item);
            }
            catch
            {
                await Insert(Item);
            }
        }
    }
}