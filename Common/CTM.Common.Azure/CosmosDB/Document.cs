﻿using Microsoft.Azure.Documents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Common.Azure.CosmosDB
{
    public class Document<T> : Document
    {
        public Document(string ID, T Data)
        {
            Id = ID;
            this.Data = Data;
        }
        [JsonProperty(PropertyName = "data")] public T Data { get; set; }
    }
}
