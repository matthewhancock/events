﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Common.Azure.CosmosDB
{
    public class Database
    {
        public Database(string Url, string AuthKey, string Name)
        {
            this.Name = Name;
            Client = new DocumentClient(new Uri(Url), AuthKey);

            Client.CreateDatabaseIfNotExistsAsync(new Microsoft.Azure.Documents.Database { Id = Name }).Wait();
            Uri = UriFactory.CreateDatabaseUri(Name);
        }

        public DocumentClient Client { get; }
        public Uri Uri { get;  }
        public string Name { get; }
    }
}
