﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CTM.Common.External
{
    public interface IEmailGateway
    {
        Task<object> SendMessage(string From, string[] To, string Subject, string Message, string[] CC = null, string[] BCC = null, bool HtmlContent = true);
    }
}
