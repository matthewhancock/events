﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CTM.Common.External
{
    public interface ISmsGateway
    {
        Task<object> SendMessage(string From, string[] To, string Message);
    }
}
