﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Smart.Common.Import
{
    public interface IImport
    {
        Task<object> Run();
    }
    public interface IImport<ResultType> : IImport
    {
        Task<ResultType> Run();
    }
}
