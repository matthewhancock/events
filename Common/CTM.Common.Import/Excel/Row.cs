﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Common.Import
{
    public static class Excel
    {
        public interface IExcelRow : Import.IRow
        {
        }
        public class Row : IExcelRow
        {
            [AttributeUsage(AttributeTargets.Property)]
            public class MappingAttribute : Attribute
            {
                public MappingAttribute(string Column)
                {
                    this.Column = Column;
                }
                public string Column { private set; get; }
            }
        }
    }
}
