﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Smart.Common.Import
{
    public abstract class Excel<RowType, ResultType, RowResultType> : BaseImport<RowType, ResultType, RowResultType> where RowType : Excel.IExcelRow, new() where ResultType : IResult<RowResultType>
    {
        protected abstract int RowStart { get; }
        protected abstract System.IO.Stream Stream { get; }
        protected abstract string Filename { get; }

        private IEnumerable<(string ColumnName, Type TargetType, Action<RowType, object> Setter)> _mappings;
        private IEnumerable<(string ColumnName, Type TargetType, Action<RowType, object> Setter)> Mappings
        {
            get
            {
                if (_mappings == null)
                {
                    var l = new List<(string ColumnName, Type TargetType, Action<RowType, object> Setter)>();

                    var props = Util.Reflection.GetPropertiesWithAttribute<RowType, Excel.Row.MappingAttribute>();
                    foreach (var i in props)
                    {
                        var a = i.GetCustomAttribute<Excel.Row.MappingAttribute>();
                        Action<RowType, object> setter;

                        Type targetType = i.PropertyType;

                        if (targetType.Equals(typeof(string)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.String(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(double)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Double(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(float)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Float(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(int)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Integer(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(bool)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Boolean(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(byte)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Byte(i.SetValue, r, v);
                        }
                        else
                        {
                            // will fail for unmapped types
                            setter = (r, v) => { if (v is null) { return; } i.SetValue(r, v); };
                        }

                        l.Add((a.Column, targetType, setter));
                    }
                    _mappings = l;
                }
                return _mappings;
            }
        }

        protected override IEnumerable<RowType> ParseRows()
        {
            // .xls is binary, .xlsx is Open XML
            using (var r = Filename.EndsWith(Util.Excel.Extensions.Xls) ? ExcelReaderFactory.CreateBinaryReader(Stream) : ExcelReaderFactory.CreateOpenXmlReader(Stream))
            {
                var rowIndex = 0;
                var rowStartIndex = RowStart - 1;
                while (r.Read())
                {
                    if (rowIndex < rowStartIndex)
                    {
                        rowIndex++;
                        continue;
                    }
                    var row = new RowType();
                    foreach (var mapping in Mappings)
                    {
                        var columnIndex = Util.Excel.ColumnNameToIndex(mapping.ColumnName);
                        var value = r.GetValue(columnIndex);
                        if (value != null)
                        {
                            mapping.Setter(row, value);
                        }
                    }
                    yield return row;

                    rowIndex++;
                }
            }
        }
    }
}
