﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Smart.Common.Import
{
    public abstract class BaseImport<RowType, ResultType, RowResultType> : IImport where RowType : IRow where ResultType : IResult<RowResultType>
    {
        public async Task<ResultType> Run()
        {
            var rows = ParseRows();
            var row_index = 0;
            foreach (var i in rows)
            {
                await ProcessRow(i, row_index);
                row_index++;
            }

            return GenerateResult();
        }
        protected abstract IEnumerable<RowType> ParseRows();
        protected abstract Task<RowResultType> ProcessRow(RowType Row, int RowIndex);
        protected abstract ResultType GenerateResult();

        async Task<object> IImport.Run()
        {
            return await Run();
        }
    }
}
