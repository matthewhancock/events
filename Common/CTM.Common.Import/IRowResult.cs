﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Common.Import
{
    public interface IRowResult
    {
        bool Success { get; }
        string Message { get; }
    }
}
