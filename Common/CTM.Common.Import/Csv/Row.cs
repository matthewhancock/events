﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Common.Import
{
    public partial class Csv
    {
        public interface ICsvRow : Import.IRow
        {
        }
        public class Row : ICsvRow
        {
            [AttributeUsage(AttributeTargets.Property)]
            public class MappingAttribute : Attribute
            {
                public MappingAttribute(int ColumnIndex)
                {
                    this.ColumnIndex = ColumnIndex;
                }
                public int ColumnIndex { private set; get; }
            }
        }
    }
}
