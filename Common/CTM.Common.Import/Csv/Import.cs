﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Smart.Common.Import
{
    public abstract partial class Csv<RowType, ResultType, RowResultType> : BaseImport<RowType, ResultType, RowResultType> where RowType : Csv.ICsvRow, new() where ResultType : IResult<RowResultType>
    {
        protected abstract bool HasHeaderRow { get; }
        protected abstract System.IO.Stream Stream { get; }
        protected abstract string Filename { get; }
        protected override IEnumerable<RowType> ParseRows()
        {
            // .xls is binary, .xlsx is Open XML
            using (var r = ExcelReaderFactory.CreateCsvReader(Stream))
            {
                var rowIndex = 0;
                var rowStartIndex = HasHeaderRow ? 1 : 0;
                while (r.Read())
                {
                    if (rowIndex < rowStartIndex)
                    {
                        rowIndex++;
                        continue;
                    }
                    var row = new RowType();
                    foreach (var mapping in Mappings)
                    {
                        var value = r.GetValue(mapping.ColumnIndex);
                        if (value != null)
                        {
                            mapping.Setter(row, value);
                        }
                    }
                    yield return row;

                    rowIndex++;
                }
            }
        }



        private IEnumerable<(int ColumnIndex, Type TargetType, Action<RowType, object> Setter)> _mappings;
        private IEnumerable<(int ColumnIndex, Type TargetType, Action<RowType, object> Setter)> Mappings
        {
            get
            {
                if (_mappings == null)
                {
                    var l = new List<(int ColumnIndex, Type TargetType, Action<RowType, object> Setter)>();

                    var props = Util.Reflection.GetPropertiesWithAttribute<RowType, Csv.Row.MappingAttribute>();
                    foreach (var i in props)
                    {
                        var a = i.GetCustomAttribute<Csv.Row.MappingAttribute>();
                        Action<RowType, object> setter;

                        Type targetType = i.PropertyType;

                        if (targetType.Equals(typeof(string)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.String(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(double)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Double(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(float)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Float(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(int)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Integer(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(bool)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Boolean(i.SetValue, r, v);
                        }
                        else if (targetType.IsAssignableFrom(typeof(byte)))
                        {
                            setter = (r, v) => Util.Reflection.Setters.Byte(i.SetValue, r, v);
                        }
                        else
                        {
                            // will fail for unmapped types
                            setter = (r, v) => { if (v is null) { return; } i.SetValue(r, v); };
                        }

                        l.Add((a.ColumnIndex, targetType, setter));
                    }
                    _mappings = l;
                }
                return _mappings;
            }
        }
    }
}
