﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Common.Util
{
    public static class Logging
    {
        public class ExcludeFromLoggingAttribute : Attribute {
            public ExcludeFromLoggingAttribute(string ReplacementValue = null)
            {
                this.ReplacementValue = ReplacementValue;
            }
            public string ReplacementValue { get; }
        }

        public static string ToLoggableString(JObject Json)
        {
            return Json.ToString();
        }
    }
}
