﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Common.Util.Http
{
    public static class Methods
    {
        public const string Get = "GET";
        public const string Post = "POST";
        public const string Put = "PUT";
        public const string Delete = "DELETE";
    }
}
