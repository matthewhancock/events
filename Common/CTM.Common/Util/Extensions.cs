﻿using System;
using System.Collections.Generic;
using System.Text;
using static CTM.Common.Util.Reflection;

namespace CTM.Common.Util
{
    public static class Extensions
    {
        public static bool LoadFrom<S, T>(this T Target, S Source, IEnumerable<PropertyMapping<S, T>> PropertyMappings)
        {
            return CopyProperties(Source, Target, PropertyMappings);
        }
    }
}
