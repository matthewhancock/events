﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace CTM.Common.Util
{
    public static class Reflection
    {
        public static PropertyInfo GetPropertyFromExpression<T>(Expression<Func<T, object>> Expression)
        {
            return GetPropertyFromExpression<T, object>(Expression);
        }
        public static PropertyInfo GetPropertyFromExpression<T, V>(Expression<Func<T, V>> Expression)
        {
            if (Expression.Body is MemberExpression m)
            {
                return (PropertyInfo)m.Member;
            }
            else if (Expression.Body is UnaryExpression u && u.Operand is MemberExpression u_m)
            {
                return (PropertyInfo)u_m.Member;
            }

            return null;
        }

        public static bool CopyProperties<S, T>(S Source, T Target, IEnumerable<PropertyMapping<S, T>> Properties)
        {
            var changed = false;
            foreach (var i in Properties)
            {
                var value_source = i.Properties.GetSourceValue(Source);
                var value_target = i.Properties.Target.GetValue(Target);

                if (!value_source?.Equals(value_target) ?? !value_target?.Equals(value_source) ?? false)
                {
                    // nullable conversion doesn't work implicitly, unwrap nullable type
                    if (i.Properties.Target.PropertyType.IsGenericType && i.Properties.Target.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    {
                        value_source = Convert.ChangeType(value_source, Nullable.GetUnderlyingType(i.Properties.Target.PropertyType));
                    }

                    i.Properties.Target.SetValue(Target, value_source);

                    if (!changed)
                    {
                        changed = true;
                    }
                }
            }

            return changed;
        }

        public class PropertyMapping<S, T>
        {
            protected PropertyMapping(Func<S, object> SourceProperty, PropertyInfo TargetProperty)
            {
                Properties = (SourceProperty, TargetProperty);
            }
            public (Func<S, object> GetSourceValue, PropertyInfo Target) Properties { get; }
            public static implicit operator PropertyMapping<S, T>((Func<S, object> Source, Expression<Func<T, object>> Target) PropertyExpressions)
            {
                return new PropertyMapping<S, T>(PropertyExpressions.Source, GetPropertyFromExpression(PropertyExpressions.Target));
            }
        }
        //public class PropertyValueMapping<S, T, V> : PropertyMapping<S, T> where V : class
        //{
        //    private PropertyValueMapping(Func<S, V> SourceProperty, PropertyInfo TargetProperty) : base(SourceProperty, TargetProperty) { }

        //    public static implicit operator PropertyValueMapping<S, T, V>((Func<S, V> Source, Expression<Func<T, V>> Target) PropertyExpressions)
        //    {
        //        return new PropertyValueMapping<S, T, V>(PropertyExpressions.Source, GetPropertyFromExpression(PropertyExpressions.Target));
        //    }
        //}
    }
}
