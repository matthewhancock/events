﻿using CTM.Common.Util;
using CTM.External.UmbrellaFaces.Entities;
using System;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //var topic_full = "/subscriptions/667ffdee-bccd-4d8d-b332-c3144d246927/resourceGroups/CTMNA-EVENTS-SANDBOX/providers/Microsoft.EventGrid/topics/external-umbrellafaces-sandbox";
            //var topic = topic_full.Substring(topic_full.LastIndexOf('/') + 1);
            //topic = topic.Substring(0, topic.LastIndexOf('-'));

            //var property = Reflection.GetPropertyFromExpression<Company>(c => c.GeneralData);
            //var property2 = Reflection.GetPropertyFromExpression<CTM.External.UmbrellaFaces.Types.CreditCard>(s => s.Number.Substring(0, 4) + "XXXXXXXX" + s.Number.Substring(12, 4));
            var property3 = Reflection.GetPropertyFromExpression<Company>(c => DateTime.UtcNow);
            try
            {
                //MainAsync(args).Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            Console.ReadLine();
        }
        static async Task MainAsync(string[] args)
        {
            var connection = new CTM.Common.Azure.EventGrid.Connection<Traveller>("https://external-umbrellafaces-sandbox.westus-1.eventgrid.azure.net/api/events", "2EOAeRwWMdQ6zwybLpR/FRiwnPBwnpuqNwQmgiXs1Pc=");
            await connection.Send("Traveller", "Save", new Traveller() { ID = "Test", RemoteID = Guid.NewGuid() });
        }
    }
}
