using Microsoft.Azure.EventGrid.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace EventListener
{
    public static class Function
    {
        [FunctionName(nameof(EventListener))]
        public static void Run([EventGridTrigger] JObject Event, ILogger Log)
        {
            Log.LogInformation(Event.ToString());
        }
    }
}
