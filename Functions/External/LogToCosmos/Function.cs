
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.EventGrid.Models;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using CTM.Common.Azure.EventGrid;
using CTM.Common.Azure.CosmosDB;
using Newtonsoft.Json.Linq;
using CTM.External.UmbrellaFaces.Entities;
using CTM.External.Riskline.Entities;

namespace CTM.Functions.External.LogToCosmos
{
    public static class Function
    {
        [FunctionName(nameof(LogToCosmos) + nameof(WebHook))]
        public static async Task<IActionResult> WebHook([HttpTrigger(AuthorizationLevel.Anonymous, Common.Util.Http.Methods.Post, Route = nameof(LogToCosmos) + nameof(WebHook))]HttpRequest Request, ILogger Log)
        {
            await Run(JsonConvert.DeserializeObject<EventGridEvent>(await new StreamReader(Request.Body).ReadToEndAsync()), Log);
            return new OkResult();
        }

        [FunctionName(nameof(LogToCosmos))]
        public static async Task Run([EventGridTrigger] EventGridEvent Event, ILogger Log)
        {
            try
            {
                Log.LogInformation(Event.Topic);

                var topic = Topics.Parse(Event.Topic);
                var data = (JObject)Event.Data;

                switch (topic)
                {
                    case Topics.External.UmbrellaFaces:
                        var db = new Database(
                            Environment.GetEnvironmentVariable(Constants.Configuration.Database.Cosmos.Url),
                            Environment.GetEnvironmentVariable(Constants.Configuration.Database.Cosmos.Key),
                            Constants.Databases.UmbrellaFaces.Name);

                        var profile = data.ToObject<CTM.External.UmbrellaFaces.Types.PublishRequest>();

                        if (profile is Traveller traveller)
                        {
                            var connection = new Collection<Document<Traveller>>(Constants.Databases.UmbrellaFaces.Collections.Travellers, db);
                            await connection.Upsert(new Document<Traveller>(traveller.RemoteID.ToString(), traveller));
                        }
                        else if (profile is Company company)
                        {
                            var connection = new Collection<Document<Company>>(Constants.Databases.UmbrellaFaces.Collections.Companies, db);
                            await connection.Upsert(new Document<Company>(company.RemoteID.ToString(), company));
                        }

                        break;
                    case Topics.External.Riskline:
                        await ProcessRiskline(Event, data);
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                Log.LogError(ex.StackTrace);
            }
        }

        private static async Task ProcessRiskline(EventGridEvent Event, JObject Data)
        {
            var db = new Database(
                Environment.GetEnvironmentVariable(Constants.Configuration.Database.Cosmos.Url),
                Environment.GetEnvironmentVariable(Constants.Configuration.Database.Cosmos.Key),
                Constants.Databases.Riskline.Name);

            switch (Event.Subject)
            {
                case nameof(Alert):
                    switch (Event.EventType)
                    {
                        case CTM.External.Riskline.Constants.RequestType.Delete:
                            var id = Data.ToObject<Common.Azure.EventGrid.Types.DataID>().ID;
                            await Delete<Alert>(Constants.Databases.Riskline.Collections.Alerts, id);
                            break;
                        case CTM.External.Riskline.Constants.RequestType.Post:
                        case CTM.External.Riskline.Constants.RequestType.Put:
                            await Upsert<Alert>(Constants.Databases.Riskline.Collections.Alerts, (a) => a.ID.ToString());
                            break;
                    }
                    break;
                case nameof(Advisory):
                    switch (Event.EventType)
                    {
                        case CTM.External.Riskline.Constants.RequestType.Delete:
                            var id = Data.ToObject<Common.Azure.EventGrid.Types.DataID>().ID;
                            await Delete<Advisory>(Constants.Databases.Riskline.Collections.Advisories, id);
                            break;
                        case CTM.External.Riskline.Constants.RequestType.Post:
                        case CTM.External.Riskline.Constants.RequestType.Put:
                            await Upsert<Advisory>(Constants.Databases.Riskline.Collections.Advisories, (a) => a.ID.ToString());
                            break;
                    }
                    break;
                case nameof(City):
                    switch (Event.EventType)
                    {
                        case CTM.External.Riskline.Constants.RequestType.Post:
                            await Upsert<City>(Constants.Databases.Riskline.Collections.Cities, (c) => c.Code);
                            break;
                    }
                    break;
                case nameof(Country):
                    switch (Event.EventType)
                    {
                        case CTM.External.Riskline.Constants.RequestType.Post:
                            await Upsert<Country>(Constants.Databases.Riskline.Collections.Countries, (c) => c.Code);
                            break;
                    }
                    break;
                case nameof(PreTravelAdvisory):
                    switch (Event.EventType)
                    {
                        case CTM.External.Riskline.Constants.RequestType.Post:
                            await Upsert<PreTravelAdvisory>(Constants.Databases.Riskline.Collections.PreTravelAdvisories, (pta) => pta.Country);
                            break;
                    }
                    break;
                case nameof(Summary):
                    switch (Event.EventType)
                    {
                        case CTM.External.Riskline.Constants.RequestType.Post:
                            await Upsert<Summary>(Constants.Databases.Riskline.Collections.Summaries, (s) => s.Country);
                            break;
                    }
                    break;
            }

            async Task Upsert<T>(string CollectionName, Func<T, string> GetID)
            {
                var entity = Data.ToObject<T>();

                var connection = new Collection<Document<T>>(CollectionName, db);
                await connection.Upsert(new Document<T>(GetID(entity), entity));
            }
            async Task Delete<T>(string CollectionName, string ID)
            {
                var connection = new Collection<Document<T>>(CollectionName, db);
                await connection.Delete(ID);
            }
        }
    }
}
