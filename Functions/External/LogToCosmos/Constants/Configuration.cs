﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Functions.External.LogToCosmos.Constants
{
    public static class Configuration
    {
        public static class Database
        {
            public static class Cosmos
            {
                public const string Key = "db:cosmos:external:key";
                public const string Url = "db:cosmos:external:url";
            }
        }
    }
}
