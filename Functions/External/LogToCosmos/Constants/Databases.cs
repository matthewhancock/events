﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Functions.External.LogToCosmos.Constants
{
    public static class Databases
    {
        public static class Riskline
        {
            public const string Name = nameof(Riskline);
            public static class Collections
            {
                public const string Advisories = nameof(Advisories);
                public const string Alerts = nameof(Alerts);
                public const string Cities = nameof(Cities);
                public const string Countries = nameof(Countries);
                public const string PreTravelAdvisories = nameof(PreTravelAdvisories);
                public const string Summaries = nameof(Summaries);
            }
        }
        public static class UmbrellaFaces
        {
            public const string Name = nameof(UmbrellaFaces);
            public static class Collections
            {
                public const string Companies = nameof(Companies);
                public const string Travellers = nameof(Travellers);
            }
        }
    }
}
