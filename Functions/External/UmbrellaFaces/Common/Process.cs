﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTM.Common.Util;
using CTM.Database.Common;

namespace CTM.Functions.External.UmbrellaFaces.Common
{
    public static class Process
    {
        public static async Task<(T Entity, bool Inserted, bool Updated)> Entity<S, T>(S Source, IQueryable<T> LookupTarget, Action<T> TargetInitializer, DbContext Database, string DatabaseAuditName, IEnumerable<Reflection.PropertyMapping<S, T>> Mapping) where T : BaseEntity, new()
        {
            return await Entity(Source, LookupTarget, TargetInitializer, Database,
                (t) =>
                {
                    t.CreatedBy = DatabaseAuditName;
                    t.CreatedOn = DateTime.UtcNow;
                },
                (t) =>
                {
                    t.ModifiedBy = DatabaseAuditName;
                    t.ModifiedOn = DateTime.UtcNow;
                },
                Mapping
                );
        }
        public static async Task<(T Entity, bool Inserted, bool Updated)> Entity<S, T>(S Source, IQueryable<T> LookupTarget, Action<T> TargetInitializer, DbContext Database, Action<T> AuditingInsert, Action<T> AuditingUpdate, IEnumerable<Reflection.PropertyMapping<S, T>> Mapping) where T : new()
        {
            var target = await LookupTarget.FirstOrDefaultAsync();

            // Determine if new Debtor needs to be inserted
            var insert = false;

            if (target == null)
            {
                target = new T();
                TargetInitializer(target);
                insert = true;
            }

            var changed = target.LoadFrom(Source, Mapping);

            if (insert)
            {
                AuditingInsert(target);
                Database.Entry(target).State = EntityState.Added;
                await Database.SaveChangesAsync(); // need to insert to trigger and receive Identity column value
            }
            else if (changed)
            {
                AuditingUpdate(target);
                Database.Entry(target).State = EntityState.Modified;
            }

            return (target, insert, changed);
        }

        public static async Task ChildEntities<S, T>(IEnumerable<S> Source, IQueryable<T> ExistingLookup, Func<S, T, bool> MatchSourceToTarget, Action<S, T> TargetInitialization, Func<T, T, bool> MatchTargetToSameType, DbContext Database, string DatabaseAuditName, bool NewParent, IEnumerable<Reflection.PropertyMapping<S, T>> Mapping = null) where T : Database.TravelCTMTraveller.Types.Entity, new()
        {
            var anySource = Source?.Any() ?? false;

            // Exit out if new parent and no child data
            if (NewParent && !anySource)
            {
                return;
            }

            var existing = ExistingLookup;

            // Exit out if there are no data in either Database or Faces
            if (!await existing.AnyAsync() && !anySource)
            {
                return;
            }

            var processed = new List<T>();

            if (anySource)
            {
                foreach (var i in Source)
                {
                    var target = await existing.Where(q => MatchSourceToTarget(i, q)).FirstOrDefaultAsync();
                    var insert = false;
                    if (target == null)
                    {
                        target = new T();
                        TargetInitialization(i, target);

                        Database.Entry(target).State = EntityState.Added;
                        insert = true;
                    }

                    if (Mapping != null)
                    {
                        var changed = target.LoadFrom(i, Mapping);
                        if (changed && !insert) // only mark as modified if not being inserted
                        {
                            target.ModifiedBy = DatabaseAuditName;
                            target.ModifiedOn = DateTime.UtcNow;

                            Database.Entry(target).State = EntityState.Modified;
                        }
                    }

                    processed.Add(target);
                }
            }

            foreach (var delete in existing.Where(q => !processed.Any(a => MatchTargetToSameType(q, a))))
            {
                delete.IsDeleted = true;
                delete.Enabled = false;
                delete.ModifiedBy = DatabaseAuditName;
                delete.ModifiedOn = DateTime.UtcNow;
                Database.Entry(delete).State = EntityState.Modified;
            }
        }



        public static async Task CreditCard<S, T, P>(IEnumerable<S> Source, IQueryable<T> TargetExistingSelector, P ParentEntity, bool NewParentEntity, Database.TravelCTMTraveller.Context Database, string DatabaseAuditName, Action<T> TargetInitialization = null, string CardholderFallback = null) where S : CTM.External.UmbrellaFaces.Types.CreditCard where T : Database.TravelCTMTraveller.Types.CreditCard, new() where P : class
        {
            IEnumerable<T> existingCards = null;
            if (!NewParentEntity)
            {
                existingCards = await TargetExistingSelector.ToListAsync();
            }

            var processedCards = new List<T>();
            if (Source != null)
            {
                foreach (var i in Source)
                {
                    var token = await CTM.External.TokenEx.CreditCard.Tokenize(i.Number);

                    var cc = existingCards?.Where(q => q.Token == token).FirstOrDefault();
                    // Determine if new Credit Card needs to be inserted
                    var insert = false;

                    if (cc == null)
                    {
                        cc = new T()
                        {
                            Token = token,
                            SourceTypeId = Constants.Database.SourceTypeID,
                            ExternalCreditCardId = -1
                        };
                        TargetInitialization?.Invoke(cc);

                        insert = true;
                    }

                    var changed = cc.LoadFrom(i, new Reflection.PropertyMapping<S, T>[]
                    {
                        (s => s.Remark ?? CardholderFallback, t => t.CardHolder), // If Remark is null in Faces, default to a valid value
                        (s => s.Type, t => t.CardTypeCode),
                        (s => s.Number.Substring(0, 4) + string.Empty.PadLeft(s.Number.Length / 2, 'X') + s.Number.Substring(s.Number.Length - 4, 4), t => t.CardNumberDisplay),
                        (s => (DateTimeOffset?) s.Expiration, t => t.ExpiryDate),
                        (s => s.PublishAsFop, t => t.Enabled),
                        (s => false, t => t.IsDeleted)
                    });

                    if (insert)
                    {
                        cc.CreatedOn = DateTime.UtcNow;
                        cc.CreatedBy = DatabaseAuditName;
                        cc.SourceTypeId = Constants.Database.SourceTypeID;
                        Database.Entry(cc).State = EntityState.Added;
                    }
                    else if (changed)
                    {
                        cc.ModifiedOn = DateTime.UtcNow;
                        cc.ModifiedBy = DatabaseAuditName;
                        cc.SourceTypeId = Constants.Database.SourceTypeID;
                        Database.Entry(cc).State = EntityState.Modified;
                    }

                    processedCards.Add(cc);
                }
            }

            if (existingCards?.Any() ?? false)
            {
                foreach (var delete in existingCards.Where(q => !processedCards.Any(a => a.Token == q.Token)))
                {
                    delete.IsDeleted = true;
                    delete.Enabled = false;
                    delete.ModifiedBy = DatabaseAuditName;
                    delete.ModifiedOn = DateTime.UtcNow;
                    Database.Entry(delete).State = EntityState.Modified;
                }
            }
        }
    }
}
