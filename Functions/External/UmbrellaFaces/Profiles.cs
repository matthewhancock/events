
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CTM.External.UmbrellaFaces.Entities;
using CTM.External.UmbrellaFaces.Types;
using System.Linq;
using CTM.External.UmbrellaFaces;
using CTM.External.UmbrellaFaces.System;

namespace CTM.Functions.External.UmbrellaFaces
{
    public static class Profiles
    {
        [FunctionName(nameof(Profiles) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Post, Route = Constants.Routes.Profile)]HttpRequest Request, ILogger Log)
        {
            try
            {
                var json = await new StreamReader(Request.Body).ReadToEndAsync();
                Log.LogInformation(json);

                var apiKey = Environment.GetEnvironmentVariable(Constants.Configuration.ApiKey);
                var authHeader = Request.Headers[Authentication.Constants.Headers.Authorization];
                Log.LogInformation($"Auth:{authHeader}");

                // allow bypassing authorization by removing key
                if (apiKey != null && !Authentication.Authorize(json, apiKey, authHeader))
                {
                    return new StatusCodeResult(StatusCodes.Status401Unauthorized);
                }

                var profile = JsonConvert.DeserializeObject<PublishRequest>(json);
                var connection = new CTM.Common.Azure.EventGrid.Connection<PublishRequest>(
                    Environment.GetEnvironmentVariable(Constants.Configuration.EventGrid.Endpoint),
                    Environment.GetEnvironmentVariable(Constants.Configuration.EventGrid.Key));
                               
                if (profile is Traveller traveller)
                {
                    await connection.Send(nameof(Traveller), profile.Action, traveller);
                }
                else if (profile is Company company)
                {
                    await connection.Send(nameof(Company), profile.Action, company);
                }

                var response = new PublishResponse() { Status = CTM.External.UmbrellaFaces.Constants.Values.Status.Ok };

                if (profile.Action == CTM.External.UmbrellaFaces.Constants.Values.Actions.Save)
                {
                    response.Locator = profile.ID;
                }

                return new JsonResult(response);
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                Log.LogError(ex.StackTrace);

                return new StatusCodeResult(500);
            }
        }
    }
}
