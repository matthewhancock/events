using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.EventGrid.Models;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using CTM.External.UmbrellaFaces.Entities;
using CTM.Database;
using CTM.Database.TravelCTM.Tables;
using CTM.Database.TravelCTMTraveller.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using CTM.Common.Util;
using static CTM.Common.Util.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using System.IO;
using Newtonsoft.Json;

namespace CTM.Functions.External.UmbrellaFaces
{
    public static class ProcessCompany
    {
        //[FunctionName(nameof(ProcessCompany) + nameof(WebHook))]
        //public static async Task<IActionResult> WebHook([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Post, Route = nameof(ProcessCompany) + nameof(WebHook))]HttpRequest Request, ILogger Log)
        //{
        //    await Run(JsonConvert.DeserializeObject<EventGridEvent>(await new StreamReader(Request.Body).ReadToEndAsync()), Log);
        //    return new OkResult();
        //}

        [FunctionName(nameof(ProcessCompany))]
        public static async Task Run([EventGridTrigger]EventGridEvent Event, ILogger Log)
        {
            var company = ((JObject)Event.Data).ToObject<Company>();
            if (company.DebtorIdentifier == null)
            {
                throw new Exception($"[{company.RemoteID}] Debtor Identifier Not Provided");
            }

            var options_travel = new DbContextOptionsBuilder<Database.TravelCTM.Context>().UseSqlServer(Environment.GetEnvironmentVariable(Constants.Configuration.Database.Sql.TravelCTM));
            var options_traveller = new DbContextOptionsBuilder<Database.TravelCTMTraveller.Context>().UseSqlServer(Environment.GetEnvironmentVariable(Constants.Configuration.Database.Sql.TravelCTMTraveller));

            using (var db_travel = new Database.TravelCTM.Context(options_travel.Options))
            {
                using (var db_traveller = new Database.TravelCTMTraveller.Context(options_traveller.Options))
                {
                    await Process(company, db_travel, db_traveller);
                }
            }
        }

        private static async Task Process(Company Company, Database.TravelCTM.Context Database, Database.TravelCTMTraveller.Context TravellerDatabase)
        {
            // Process Debtor
            var (debtor, debtor_new, debtor_updated) = await ProcessDebtor(Company, Database);
            // Process Debtor Credit Cards
            await ProcessDebtorCreditCards(Company, TravellerDatabase, debtor, debtor_new);

            await Database.SaveChangesAsync();
            await TravellerDatabase.SaveChangesAsync();
        }

        private static async Task<(Debtor Debtor, bool New, bool Updated)> ProcessDebtor(Company Company, Database.TravelCTM.Context Database)
        {
            return await Common.Process.Entity(
                Company,
                Database.Debtors.Where(q => q.Identifier == Company.DebtorIdentifier),
                (d) => { d.Identifier = Company.DebtorIdentifier; },
                Database,
                (t) =>
                {
                    t.CreatedBy = Constants.Database.Auditing.Company.Name;
                    t.CreatedOn = DateTime.UtcNow;
                },
                (t) =>
                {
                    t.ModifiedBy = Constants.Database.Auditing.Company.Name;
                    t.ModifiedOn = DateTime.UtcNow;
                },
                new PropertyMapping<Company, Debtor>[]
                {
                    (c => c.GeneralData.Name, d => d.Title),
                    (c => c.DebtorIdentifier, d => d.Identifier),
                    (c => c.RemoteID, d => d.UmbrellaFacesID)
                });
        }

        private static async Task ProcessDebtorCreditCards(Company Company, Database.TravelCTMTraveller.Context Database, Debtor Debtor, bool NewDebtor)
        {
            await Common.Process.CreditCard(
                Company.CreditCards,
                Database.DebtorCreditCards.Where(q => q.DebtorIdentifier == Debtor.Identifier),
                Debtor,
                NewDebtor,
                Database,
                Constants.Database.Auditing.Company.Name,
                (cc) =>
                {
                    cc.DebtorIdentifier = Debtor.Identifier;
                });
        }
    }
}
