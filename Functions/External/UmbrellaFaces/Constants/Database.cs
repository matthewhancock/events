﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Functions.External.UmbrellaFaces.Constants
{
    public static class Database
    {
        public static class Auditing
        {
            public static class Company
            {
                public static string Name = $"{nameof(Functions)}.{nameof(ProcessCompany)}";
            }
            public static class Traveller
            {
                public static string Name = $"{nameof(Functions)}.{nameof(ProcessTraveller)}";
            }
        }
        public const int SourceTypeID = 9;
    }
}
