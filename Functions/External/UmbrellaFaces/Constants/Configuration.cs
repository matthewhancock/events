﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Functions.External.UmbrellaFaces.Constants
{
    public static class Configuration
    {
        public const string ApiKey = "external:umbrellafaces:apikey";
        public static class Database
        {
            public static class Cosmos
            {
                public const string Key = "db:cosmos:external:key";
                public const string Url = "db:cosmos:external:url";
            }
            public static class Sql
            {
                public const string TravelCTM = "db:sql:travelctm";
                public const string TravelCTMTraveller = "db:sql:travelctmtraveller";
            }
        }
        public static class EventGrid
        {
            public const string Endpoint = "eg:endpoint";
            public const string Key = "eg:key";
        }
    }
}
