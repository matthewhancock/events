using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.EventGrid.Models;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using CTM.External.UmbrellaFaces.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using CTM.Common.Util;
using CTM.Database.TravelCTM.Tables;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using CTM.Common.AspNet.System.Web.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Newtonsoft.Json;

namespace CTM.Functions.External.UmbrellaFaces
{
    public static class ProcessTraveller
    {
        //[FunctionName(nameof(ProcessTraveller) + nameof(WebHook))]
        //public static async Task<IActionResult> WebHook([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Post, Route = nameof(ProcessTraveller) + nameof(WebHook))]HttpRequest Request, ILogger Log)
        //{
        //    try
        //    {
        //        await Run(JsonConvert.DeserializeObject<EventGridEvent>(await new StreamReader(Request.Body).ReadToEndAsync()), Log);
        //    } catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return new OkResult();
        //}

        [FunctionName(nameof(ProcessTraveller))]
        public static async Task Run([EventGridTrigger]EventGridEvent Event, ILogger Log)
        {
            var traveller = ((JObject)Event.Data).ToObject<Traveller>();

            var options_travel = new DbContextOptionsBuilder<Database.TravelCTM.Context>().UseSqlServer(Environment.GetEnvironmentVariable(Constants.Configuration.Database.Sql.TravelCTM));
            var options_traveller = new DbContextOptionsBuilder<Database.TravelCTMTraveller.Context>().UseSqlServer(Environment.GetEnvironmentVariable(Constants.Configuration.Database.Sql.TravelCTMTraveller));

            using (var db = new Database.TravelCTM.Context(options_travel.Options))
            {
                var debtor = await db.Debtors.Include(i => i.DebtorUserSettings).Where(q => q.UmbrellaFacesID == traveller.CompanyID).FirstOrDefaultAsync();

                if (debtor == null)
                {
                    throw new Exception($"Traveller [{traveller.RemoteID}]: Debtor [{traveller.CompanyID}] not found");
                }
                (var user, _ , _) = await TravelCTM.Process(traveller, debtor, db);
                await db.SaveChangesAsync();

                using (var db_traveller = new Database.TravelCTMTraveller.Context(options_traveller.Options))
                {
                    var travellers = await TravelCTMTraveller.Process(traveller, debtor, db_traveller);
                    await db_traveller.SaveChangesAsync();
                    
                    // ensure link between User and Traveller
                    var changed = user.LoadFrom(travellers, new Reflection.PropertyMapping<(Database.TravelCTMTraveller.Tables.Traveller Traveller, Database.TravelCTMTraveller.Tables.TravelArranger TravelArranger), User>[] {
                        (s => s.Traveller?.Id, t => t.BusinessIntelligenceTravellerID),
                        (s => s.TravelArranger?.Id, t => t.BusinessIntelligenceTravelArrangerID)
                    });

                    if (changed)
                    {
                        user.ModifiedBy = Constants.Database.Auditing.Traveller.Name;
                        user.ModifiedOn = DateTime.UtcNow;
                        db.Entry(user).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        // Process TravelCTM Database
        private static class TravelCTM
        {
            public static async Task<(User User, bool Inserted, bool Updated)> Process(Traveller Traveller, Debtor Debtor, Database.TravelCTM.Context Database)
            {
                var user = await Database.Users.Where(q => q.DebtorId == Debtor.Id && q.Email == Traveller.GeneralData.Email).FirstOrDefaultAsync();
                (Role Traveller, Role TravelArranger) roles = (
                    await Database.Roles.Where(q => q.Title == CTM.Database.TravelCTM.Constants.Roles.Names.Traveller).FirstOrDefaultAsync(),
                    await Database.Roles.Where(q => q.Title == CTM.Database.TravelCTM.Constants.Roles.Names.TravelArranger).FirstOrDefaultAsync());

                var insert = false;

                if (user == null)
                {
                    insert = true;
                    user = new User()
                    {
                        // Key Values
                        Email = Traveller.GeneralData.Email,
                        Username = Traveller.GeneralData.Email,
                        DebtorId = Debtor.Id,
                        DebtorIdentifier = Debtor.Identifier,
                        ExternalIdentifier = Traveller.RemoteID.ToString(),
                        // Defaults
                        ResetPasswordTriggered = !Debtor.DebtorUserSettings.FirstOrDefault()?.DisablePasswordExpiry ?? true, // default to true unless Debtor has setting
                        ResetSecurityQuestionTriggered = true,
                        NextGenTermConditionsRead = false,
                        IsWelcomeEmailSent = false
                    };
                }

                // Determine Roles
                var userRoles = new List<Role>();
                if (Traveller.GeneralData.TravellerData.Traveller)
                {
                    userRoles.Add(roles.Traveller);
                }
                if (Traveller.GeneralData.TravellerData.Arranger)
                {
                    userRoles.Add(roles.TravelArranger);
                }
                userRoles.Sort((a, b) => a.Id.CompareTo(b.Id)); // sort by ID for consistency
                var roleList = string.Join(',', userRoles.Select(q => q.Id));

                // Merge Objects
                var changed = user.LoadFrom(Traveller, new Reflection.PropertyMapping<Traveller, User>[] {
                    (t => t.DisplayName, u => u.Title),
                    (t => roleList, u => u.UserRoleList),
                    (t => Traveller.Action == CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete, u => u.IsDeleted)
                });

                if (insert)
                {
                    user.CreatedBy = Constants.Database.Auditing.Traveller.Name;
                    user.CreatedOn = DateTime.UtcNow;

                    #region AspNet Auth Schema
                    var conn = (SqlConnection)Database.Database.GetDbConnection();
                    if (conn.State == ConnectionState.Closed)
                    {
                        await conn.OpenAsync();
                    }
                    user.MembershipGuid = SqlMembershipProvider.CreateUser(conn, user.Username, user.Email, true);
                    SqlRoleProvider.AddUsersToRolesCore(conn, user.Username, string.Join(',', userRoles.Select(q => q.Title)));
                    #endregion

                    Database.Entry(user).State = EntityState.Added;
                    await Database.SaveChangesAsync(); // need to insert to trigger and receive Identity column value
                }
                else if (changed)
                {
                    user.ModifiedBy = Constants.Database.Auditing.Traveller.Name;
                    user.ModifiedOn = DateTime.UtcNow;

                    #region AspNet Auth Schema
                    // Get Roles in Asp.Net tables
                    var roleNames = new[] { CTM.Database.TravelCTM.Constants.Roles.Names.Traveller, CTM.Database.TravelCTM.Constants.Roles.Names.TravelArranger };
                    var aspnetRoles = await Database.AspnetRoles.Where(q => roleNames.Contains(q.RoleName)).Select(q => new { q.RoleId, q.RoleName }).ToListAsync();
                    var roleGuids = aspnetRoles.Select(q => q.RoleId);
                    var aspnetUserRoles = await Database.AspnetUsersInRoles.Where(q => q.UserId == user.MembershipGuid && roleGuids.Contains(q.RoleId)).ToListAsync();
                    // Delete where user doesn't have role
                    foreach (var i in aspnetUserRoles)
                    {
                        var r = aspnetRoles.Where(q => q.RoleId == i.RoleId).FirstOrDefault();
                        if (!userRoles.Select(q => q.Title).Contains(r.RoleName))
                        {
                            Database.Entry(i).State = EntityState.Deleted;
                        }
                    }
                    // Insert
                    var aspnetRolesToAdd = aspnetRoles.Where(q => userRoles.Select(ur => ur.Title).Contains(q.RoleName) && !aspnetUserRoles.Any(aur => aur.RoleId == q.RoleId));
                    if (aspnetRolesToAdd.Any())
                    {
                        var conn = (SqlConnection)Database.Database.GetDbConnection();
                        if (conn.State == ConnectionState.Closed)
                        {
                            await conn.OpenAsync();
                        }
                        SqlRoleProvider.AddUsersToRolesCore(conn, user.Username, string.Join(',', aspnetRolesToAdd.Select(q => q.RoleName)));
                    }

                    #endregion

                    Database.Entry(user).State = EntityState.Modified;
                }

                return (user, insert, changed);
            }
        }
        // Process TravelCTMTraveller Database
        private static class TravelCTMTraveller
        {
            public static async Task<(Database.TravelCTMTraveller.Tables.Traveller Traveller, Database.TravelCTMTraveller.Tables.TravelArranger TravelArranger)> Process(Traveller Traveller, Debtor Debtor, Database.TravelCTMTraveller.Context Database)
            {
                (var t, _, _) = await ProcessTraveller(Traveller, Debtor, Database);
                // In Faces a Traveller and Travel Arranger is one entity, need to break this out to a second entity in our database
                (var ta, _, _) = await ProcessTravelArranger(Traveller, Debtor, Database);

                return (t, ta);
            }
            public static async Task<(Database.TravelCTMTraveller.Tables.Traveller Traveller, bool Inserted, bool Updated)> ProcessTraveller(Traveller Traveller, Debtor Debtor, Database.TravelCTMTraveller.Context Database)
            {
                var t = await Database.Travellers.Where(q => q.DebtorIdentifier == Debtor.Identifier && q.Email == Traveller.GeneralData.Email).FirstOrDefaultAsync();

                // Exit out if Traveller doesn't exist in our database, and isn't a Traveller in Faces - no work is necessary
                if (t == null && !Traveller.GeneralData.TravellerData.Traveller)
                {
                    return (null, false, false);
                }

                var insert = false;

                if (t == null)
                {
                    insert = true;
                    t = new Database.TravelCTMTraveller.Tables.Traveller()
                    {
                        DebtorIdentifier = Debtor.Identifier,
                        Email = Traveller.GeneralData.Email
                    };
                }

                // merge objects
                var changed = t.LoadFrom(Traveller, new Reflection.PropertyMapping<Traveller, Database.TravelCTMTraveller.Tables.Traveller>[]
                {
                    (e => e.DisplayName, e => e.Title),
                    (e => e.GeneralData.TravellerData.MobilePhone, e => e.MobileNumber),
                    (e => e.Action == CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete, e => e.IsDeleted),
                    (e => e.GeneralData.TravellerData.Traveller, e => e.Enabled),

                    (e => e.GeneralData.TravellerData.Birthdate, e => e.DateOfBirth),
                    (e => Debtor.Title, e => e.CompanyName),
                    (e => e.GeneralData.TravellerData.Title, e => e.Salutation),
                    (e => e.GeneralData.TravellerData.FirstName, e => e.FirstName),
                    (e => e.GeneralData.Name, e => e.LastName)
                });

                // Update Source System ID if not from Faces
                if (t.SourceTypeId != Constants.Database.SourceTypeID)
                {
                    t.SourceTypeId = Constants.Database.SourceTypeID;
                    t.ExternalSourceId = await Database.GetUmbrellaFacesExternalSourceID();
                    t.UmbrellaFacesID = Traveller.RemoteID;

                    if (!changed)
                    {
                        changed = true;
                    }
                }

                if (insert)
                {
                    t.CreatedOn = DateTime.UtcNow;
                    t.CreatedBy = Constants.Database.Auditing.Traveller.Name;
                    Database.Entry(t).State = EntityState.Added;
                    await Database.SaveChangesAsync(); // save to get Identifier
                }
                else if (changed)
                {
                    t.ModifiedOn = DateTime.UtcNow;
                    t.ModifiedBy = Constants.Database.Auditing.Traveller.Name;
                    Database.Entry(t).State = EntityState.Modified;
                }

                await ProcessTravellerArrangers(Traveller, t, insert, Debtor, Database);
                await ProcessTravellerCreditCards(Traveller, t, insert, Database);
                await ProcessTravellerAlliances(Traveller, t, insert, Database);
                await ProcessTravellerPassports(Traveller, t, insert, Database);
                //await ProcessTravellerVisas(Traveller, t, insert, Database);

                return (t, insert, changed);
            }

            /// <remarks>
            /// The TravelArrangerTraveller table doesn't use the database IDs but the ExternalSourceId value from the source system. It's a nightmare.
            /// </remarks>
            public static async Task ProcessTravellerArrangers(Traveller FacesTraveller, Database.TravelCTMTraveller.Tables.Traveller CTMTraveller, bool NewTraveller, Debtor Debtor, Database.TravelCTMTraveller.Context Database)
            {
                var existing = await Database.TravelArrangerTravellers.Where(q => q.ExternalTravellerId == CTMTraveller.ExternalSourceId).ToListAsync();
                var existing_externalIDs = existing.Select(q => q.SourceTypeId).ToArray();
                var existing_arrangers = await Database.TravelArrangers.Where(q => existing_externalIDs.Contains(q.ExternalSourceId)).Select(s => new { s.Email, s.DebtorIdentifier, s.ExternalSourceId, s.UmbrellaFacesID }).ToListAsync();

                var current_externalIDs = new List<int>();

                if (FacesTraveller.Arrangers != null && FacesTraveller.Arrangers.Any())
                {
                    foreach (var i in FacesTraveller.Arrangers)
                    {
                        var ea = existing_arrangers.Where(q => q.UmbrellaFacesID == i.RemoteID).FirstOrDefault();
                        if (ea == null)
                        {
                            var ta = await Database.TravelArrangers.Where(q => q.UmbrellaFacesID == i.RemoteID).FirstOrDefaultAsync();
                            if (ta != null)
                            {
                                var tat = new Database.TravelCTMTraveller.Tables.TravelArrangerTraveller()
                                {
                                    ExternalTravellerId = CTMTraveller.ExternalSourceId,
                                    ExternalTravelArrangerId = ta.ExternalSourceId,
                                    SourceTypeId = Constants.Database.SourceTypeID
                                };
                                current_externalIDs.Add(ta.ExternalSourceId);
                                Database.Entry(tat).State = EntityState.Added;
                            }
                        }
                        else
                        {
                            current_externalIDs.Add(ea.ExternalSourceId);
                        }
                    }
                }

                // Delete relationships that no longer exist
                foreach (var i in existing.Where(q => !current_externalIDs.Contains(q.ExternalTravelArrangerId)))
                {
                    Database.Entry(i).State = EntityState.Deleted;
                }
            }
            public static async Task ProcessTravellerCreditCards(Traveller FacesTraveller, Database.TravelCTMTraveller.Tables.Traveller CTMTraveller, bool NewTraveller, Database.TravelCTMTraveller.Context Database)
            {
                await Common.Process.CreditCard(
                    FacesTraveller.CreditCards,
                    Database.TravellerCreditCards.Where(q => q.TravellerId == CTMTraveller.Id),
                    CTMTraveller,
                    NewTraveller,
                    Database,
                    Constants.Database.Auditing.Traveller.Name,
                    (cc) =>
                    {
                        cc.DebtorIdentifier = CTMTraveller.DebtorIdentifier;
                        cc.TravellerId = CTMTraveller.Id;
                        cc.ExternalTravellerId = CTMTraveller.ExternalSourceId;
                    },
                    FacesTraveller.DisplayName);
            }
            public static async Task ProcessTravellerAlliances(Traveller FacesTraveller, Database.TravelCTMTraveller.Tables.Traveller CTMTraveller, bool NewTraveller, Database.TravelCTMTraveller.Context Database)
            {
                await Common.Process.ChildEntities(
                    FacesTraveller.BookingData.AllianceMemberships,
                    Database.TravellerMemberships.Where(q => q.TravellerId == CTMTraveller.Id),
                    (s, t) => t.Type == MapFacesAllianceTypeToDatabase(s.Type) && s.Code == t.VendorCode && s.Number == t.MembershipId,
                    (s, t) =>
                    {
                        t.Type = MapFacesAllianceTypeToDatabase(s.Type);
                        t.Program = MapFacesAllianceTypeToDatabase(s.Type);
                        t.VendorCode = s.Code;
                        t.MembershipId = s.Number;
                        t.CreatedOn = DateTime.UtcNow;
                        t.CreatedBy = Constants.Database.Auditing.Traveller.Name;
                        t.SourceTypeId = Constants.Database.SourceTypeID;
                        t.TravellerId = CTMTraveller.Id;
                        t.ExternalTravellerId = CTMTraveller.ExternalSourceId;
                    },
                    (a, a2) => a.Type == a2.Type && a.VendorCode == a2.VendorCode && a.MembershipId == a2.MembershipId,
                    Database,
                    Constants.Database.Auditing.Traveller.Name,
                    NewTraveller,
                    new Reflection.PropertyMapping<CTM.External.UmbrellaFaces.Types.AllianceMembership, Database.TravelCTMTraveller.Tables.TravellerMembership>[]
                    {
                        (_ => FacesTraveller.DisplayName, t => t.Name),
                        (_ => FacesTraveller.Action == CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete, t => t.IsDeleted),
                        (_ => FacesTraveller.Action != CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete, t => t.Enabled)
                    });

                //var anyFaces = FacesTraveller.BookingData.AllianceMemberships?.Any() ?? false;

                //// Exit out if new Traveller and no alliances
                //if (NewTraveller && !anyFaces)
                //{
                //    return;
                //}

                //var existing = Database.TravellerMemberships.Where(q => q.TravellerId == CTMTraveller.Id);

                //// Exit out if there are no alliances in either Database or Faces
                //if (!await existing.AnyAsync() && !anyFaces)
                //{
                //    return;
                //}

                //var processed = new List<Database.TravelCTMTraveller.Tables.TravellerMembership>();

                //if (anyFaces)
                //{
                //    foreach (var i in FacesTraveller.BookingData.AllianceMemberships)
                //    {
                //        var membership = await existing.Where(q => q.Type == MapFacesAllianceTypeToDatabase(i.Type) && i.Code == q.VendorCode && i.Number == q.MembershipId).FirstOrDefaultAsync();
                //        if (membership == null)
                //        {
                //            membership = new Database.TravelCTMTraveller.Tables.TravellerMembership()
                //            {
                //                Type = MapFacesAllianceTypeToDatabase(i.Type),
                //                VendorCode = i.Code,
                //                MembershipId = i.Number,
                //                CreatedOn = DateTime.UtcNow,
                //                CreatedBy = Constants.Database.Auditing.Traveller.Name,
                //                IsDeleted = FacesTraveller.Action == CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete,
                //                Enabled = FacesTraveller.Action != CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete,
                //                SourceTypeId = Constants.Database.SourceTypeID,
                //                ExternalTravellerId = CTMTraveller.ExternalSourceId
                //            };

                //            Database.Entry(membership).State = EntityState.Added;
                //        }
                //        processed.Add(membership);
                //    }
                //}

                //foreach (var delete in existing.Where(q => !processed.Any(a => a.Type == q.Type && a.VendorCode == q.VendorCode && a.MembershipId == q.MembershipId)))
                //{
                //    delete.IsDeleted = true;
                //    delete.Enabled = false;
                //    Database.Entry(delete).State = EntityState.Modified;
                //}
            }
            private static string MapFacesAllianceTypeToDatabase(string FacesType)
            {
                switch (FacesType)
                {
                    case CTM.External.UmbrellaFaces.Constants.Values.AllianceMembership.Types.Car:
                        return Database.TravelCTMTraveller.Constants.Values.TravellerMembership.Type.Car;
                    case CTM.External.UmbrellaFaces.Constants.Values.AllianceMembership.Types.FrequentFlyer:
                        return Database.TravelCTMTraveller.Constants.Values.TravellerMembership.Type.Air;
                    case CTM.External.UmbrellaFaces.Constants.Values.AllianceMembership.Types.Hotel:
                        return Database.TravelCTMTraveller.Constants.Values.TravellerMembership.Type.Hotel;
                    default:
                        return null;
                }
            }

            public static async Task ProcessTravellerPassports(Traveller FacesTraveller, Database.TravelCTMTraveller.Tables.Traveller CTMTraveller, bool NewTraveller, Database.TravelCTMTraveller.Context Database)
            {
                await Common.Process.ChildEntities(
                    FacesTraveller.BookingData.Passports,
                    Database.TravellerPassports.Where(q => q.TravellerId == CTMTraveller.Id),
                    (s, t) => s.IssueCountry == t.CountryOfIssue && s.Number == t.PassportNumber,
                    (s, t) =>
                    {
                        t.PlaceOfIssue = s.IssuePlace;
                        t.CountryOfIssue = s.IssueCountry;
                        t.PassportNumber = s.Number;
                        t.CreatedOn = DateTime.UtcNow;
                        t.CreatedBy = Constants.Database.Auditing.Traveller.Name;
                        t.SourceTypeId = Constants.Database.SourceTypeID;
                        t.TravellerId = CTMTraveller.Id;
                        t.ExternalSourceId = FacesTraveller.RemoteID.ToString();
                        t.ExternalTravellerId = CTMTraveller.ExternalSourceId;
                    },
                    (a, a2) => a.CountryOfIssue == a2.CountryOfIssue && a.PassportNumber == a2.PassportNumber,
                    Database,
                    Constants.Database.Auditing.Traveller.Name,
                    NewTraveller,
                    new Reflection.PropertyMapping<CTM.External.UmbrellaFaces.Types.Passport, Database.TravelCTMTraveller.Tables.TravellerPassport>[]
                    {
                        (s => s.Nationality, t => t.Nationality),
                        (_ => FacesTraveller.GeneralData.TravellerData.Birthdate, t => t.DateOfBirth),
                        (s => s.IssueDate, t => t.IssueDate),
                        (s => s.ExpirationDate, t => t.ExpiryDate),
                        (_ => FacesTraveller.Action == CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete, t => t.IsDeleted),
                        (_ => FacesTraveller.Action != CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete, t => t.Enabled)
                    });
            }

            //public static async Task ProcessTravellerVisas(Traveller FacesTraveller, Database.TravelCTMTraveller.Tables.Traveller CTMTraveller, bool NewTraveller, Database.TravelCTMTraveller.Context Database)
            //{
            //    await Common.Process.ChildEntities(
            //        FacesTraveller.BookingData.Visas,
            //        Database.TravellerVisas.Where(q => q.TravellerId == CTMTraveller.Id),
            //        (s, t) => s.IssueCountry == t.CountryOfIssue && s.Number == t.PassportNumber,
            //        (s, t) =>
            //        {
            //            t.CreatedOn = DateTime.UtcNow;
            //            t.CreatedBy = Constants.Database.Auditing.Traveller.Name;
            //            t.IsDeleted = FacesTraveller.Action == CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete;
            //            t.Enabled = FacesTraveller.Action != CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete;
            //            t.SourceTypeId = Constants.Database.SourceTypeID;
            //            t.TravellerId = CTMTraveller.Id;
            //            t.ExternalTravellerId = CTMTraveller.ExternalSourceId;
            //        },
            //        (a, a2) => a.CountryOfIssue == a2.CountryOfIssue && a.PassportNumber == a2.PassportNumber,
            //        Database,
            //        Constants.Database.Auditing.Traveller.Name,
            //        NewTraveller,
            //        new Reflection.PropertyMapping<CTM.External.UmbrellaFaces.Types.Passport, Database.TravelCTMTraveller.Tables.TravellerPassport>[]
            //        {
            //            (s => s.Nationality, t => t.Nationality),
            //            (s => FacesTraveller.GeneralData.TravellerData.Birthdate, t => t.DateOfBirth),
            //            (s => s.IssueDate, t => t.IssueDate),
            //            (s => s.ExpirationDate, t => t.ExpiryDate)
            //        });
            //}

            public static async Task<(Database.TravelCTMTraveller.Tables.TravelArranger TravelArranger, bool Inserted, bool Updated)> ProcessTravelArranger(Traveller Traveller, Debtor Debtor, Database.TravelCTMTraveller.Context Database)
            {
                var ta = await Database.TravelArrangers.Where(q => q.DebtorIdentifier == Debtor.Identifier && q.Email == Traveller.GeneralData.Email).FirstOrDefaultAsync();

                // Exit out if Travel Arranger doesn't exist in our database, and isn't a Travel Arranger in Faces - no work is necessary
                if (ta == null && !Traveller.GeneralData.TravellerData.Arranger)
                {
                    return (null, false, false);
                }

                var insert = false;

                if (ta == null)
                {
                    insert = true;
                    ta = new Database.TravelCTMTraveller.Tables.TravelArranger()
                    {
                        DebtorIdentifier = Debtor.Identifier,
                        Email = Traveller.GeneralData.Email
                    };
                }

                // merge objects
                var changed = ta.LoadFrom(Traveller, new Reflection.PropertyMapping<Traveller, Database.TravelCTMTraveller.Tables.TravelArranger>[]
                {
                    (t => t.DisplayName, e => e.Title),
                    (t => t.GeneralData.TravellerData.MobilePhone, e => e.Mobile),
                    (t => t.Action == CTM.External.UmbrellaFaces.Constants.Values.Actions.Delete, e => e.IsDeleted),
                    (t => t.GeneralData.TravellerData.Arranger, e => e.Enabled)
                });

                // Update Source System ID if not from Faces
                if (ta.SourceTypeId != Constants.Database.SourceTypeID)
                {
                    ta.SourceTypeId = Constants.Database.SourceTypeID;
                    ta.ExternalSourceId = await Database.GetUmbrellaFacesExternalSourceID();
                    ta.UmbrellaFacesID = Traveller.RemoteID;

                    if (!changed)
                    {
                        changed = true;
                    }
                }

                if (insert)
                {
                    ta.CreatedOn = DateTime.UtcNow;
                    ta.CreatedBy = Constants.Database.Auditing.Traveller.Name;
                    Database.Entry(ta).State = EntityState.Added;
                }
                else if (changed)
                {
                    ta.ModifiedOn = DateTime.UtcNow;
                    ta.ModifiedBy = Constants.Database.Auditing.Traveller.Name;
                    Database.Entry(ta).State = EntityState.Modified;
                }

                return (ta, insert, changed);
            }
        }
    }
}
