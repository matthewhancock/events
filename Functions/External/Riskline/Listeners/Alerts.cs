using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using CTM.External.Riskline.Constants;
using CTM.External.Riskline.Entities;

namespace CTM.Functions.External.Riskline.Webhooks
{
    public static class Alerts
    {
        [FunctionName(nameof(Alerts) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Post, Route = Routes.Alerts)]HttpRequest Request, ILogger Log)
        {
            return await Common.Post<Alert>(Request, Log);
        }

        [FunctionName(nameof(Alerts) + nameof(Put))]
        public static async Task<IActionResult> Put([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Put, Route = Routes.AlertsWithID)]HttpRequest Request, int ID, ILogger Log)
        {
            return await Common.Put<Alert>(Request, Log);
        }

        [FunctionName(nameof(Alerts) + nameof(Delete))]
        public static async Task<IActionResult> Delete([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Delete, Route = Routes.AlertsWithID)]HttpRequest Request, int ID, ILogger Log)
        {
            return await Common.Delete<Alert>(Request, Log, ID.ToString());
        }
    }
}
