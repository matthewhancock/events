using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using CTM.External.Riskline.Constants;
using CTM.External.Riskline.Entities;

namespace CTM.Functions.External.Riskline.Webhooks
{
    public static class Cities
    {
        [FunctionName(nameof(Cities) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Post, Route = Routes.Cities)]HttpRequest Request, ILogger Log)
        {
            return await Common.Post<City>(Request, Log);
        }
    }
}
