﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CTM.Functions.External.Riskline.Webhooks
{
    public static class Common
    {
        private static async Task<IActionResult> Post<Entity>(HttpRequest Request, ILogger Log, string Action) where Entity : class
        {
            if (!System.Authentication.Authenticate(Request))
            {
                return new StatusCodeResult(StatusCodes.Status401Unauthorized);
            }

            var json = await new StreamReader(Request.Body).ReadToEndAsync();

            Log.LogInformation(Action);

            #region "Logging"
            var jsonl = json.Length;
            if (jsonl > CTM.Common.Azure.Functions.Constants.MaxLogLength)
            {
                var index = 0;
                while (index < jsonl)
                {
                    var sub = json.Substring(index, Math.Min(CTM.Common.Azure.Functions.Constants.MaxLogLength, jsonl - index));
                    Log.LogInformation(sub);
                    index += CTM.Common.Azure.Functions.Constants.MaxLogLength;
                }
            }
            else
            {
                Log.LogInformation(json);
            }
            #endregion

            var data = JsonConvert.DeserializeObject<Entity>(json);

            var connection = new CTM.Common.Azure.EventGrid.Connection<Entity>(
                Environment.GetEnvironmentVariable(Constants.Configuration.Keys.EventGrid.Endpoint),
                Environment.GetEnvironmentVariable(Constants.Configuration.Keys.EventGrid.Key));

            await connection.Send(data.GetType().Name, Action, data);

            return new OkResult();
        }

        public static async Task<IActionResult> Post<Entity>(HttpRequest Request, ILogger Log) where Entity : class
        {
            return await Post<Entity>(Request, Log, CTM.External.Riskline.Constants.RequestType.Post);
        }

        public static async Task<IActionResult> Put<Entity>(HttpRequest Request, ILogger Log) where Entity : class
        {
            return await Post<Entity>(Request, Log, CTM.External.Riskline.Constants.RequestType.Put);
        }

        public static async Task<IActionResult> Delete<Entity>(HttpRequest Request, ILogger Log, string ID) where Entity : class
        {
            if (!System.Authentication.Authenticate(Request))
            {
                return new StatusCodeResult(StatusCodes.Status401Unauthorized);
            }
            Log.LogInformation(CTM.External.Riskline.Constants.RequestType.Delete);
            Log.LogInformation(ID);

            var connection = new CTM.Common.Azure.EventGrid.Connection<CTM.Common.Azure.EventGrid.Types.DataID>(
                Environment.GetEnvironmentVariable(Constants.Configuration.Keys.EventGrid.Endpoint),
                Environment.GetEnvironmentVariable(Constants.Configuration.Keys.EventGrid.Key));

            await connection.Send(typeof(Entity).Name, CTM.External.Riskline.Constants.RequestType.Delete, new CTM.Common.Azure.EventGrid.Types.DataID() { ID = ID });

            return new OkResult();
        }
    }
}
