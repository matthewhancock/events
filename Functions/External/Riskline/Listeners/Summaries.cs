using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using CTM.External.Riskline.Entities;
using CTM.External.Riskline.Constants;

namespace CTM.Functions.External.Riskline.Webhooks
{
    public static class Summaries
    {
        [FunctionName(nameof(Summaries) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Post, Route = Routes.Summaries)]HttpRequest Request, ILogger Log)
        {
            return await Common.Post<Summary>(Request, Log);
        }
    }
}
