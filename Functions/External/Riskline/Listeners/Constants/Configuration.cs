﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Functions.External.Riskline.Constants
{
    public static class Configuration
    {
        public static class Keys
        {
            public static class Auth
            {
                public static class Header
                {
                    public const string Key = "auth:header:key";
                    public const string Value = "auth:header:value";
                }
            }
            public static class EventGrid
            {
                public const string Endpoint = "eg:endpoint";
                public const string Key = "eg:key";
            }
        }
    }
}
