using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using CTM.External.Riskline.Entities;
using CTM.External.Riskline.Constants;

namespace CTM.Functions.External.Riskline.Webhooks
{
    public static class Advisories
    {
        [FunctionName(nameof(Advisories) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Post, Route = Routes.Advisories)]HttpRequest Request, ILogger Log)
        {
            return await Common.Post<Advisory>(Request, Log);
        }

        [FunctionName(nameof(Advisories) + nameof(Put))]
        public static async Task<IActionResult> Put([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Put, Route = Routes.AdvisoriesWithID)]HttpRequest Request, int ID, ILogger Log)
        {
            return await Common.Put<Advisory>(Request, Log);
        }

        [FunctionName(nameof(Advisories) + nameof(Delete))]
        public static async Task<IActionResult> Delete([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Delete, Route = Routes.AdvisoriesWithID)]HttpRequest Request, int ID, ILogger Log)
        {
            return await Common.Delete<Advisory>(Request, Log, ID.ToString());
        }
    }
}
