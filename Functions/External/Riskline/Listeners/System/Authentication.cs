﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.Functions.External.Riskline.System
{
    public static class Authentication
    {
        public static bool Authenticate(this HttpRequest Request)
        {
            return ValidateHeader(Request);
        }
        private static bool ValidateHeader(HttpRequest Request)
        {
            var key = Environment.GetEnvironmentVariable(Constants.Configuration.Keys.Auth.Header.Key);

            // if no key configured, ignore validation
            if (key == null)
            {
                return true;
            }

            var value = Environment.GetEnvironmentVariable(Constants.Configuration.Keys.Auth.Header.Value);

            return Request.Headers[key] == value;
        }
    }
}
