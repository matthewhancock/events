using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using CTM.External.Riskline.Entities;
using CTM.External.Riskline.Constants;

namespace CTM.Functions.External.Riskline.Webhooks
{
    public static class Countries
    {
        [FunctionName(nameof(Countries) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, CTM.Common.Util.Http.Methods.Post, Route = Routes.CountryReports)]HttpRequest Request, ILogger Log)
        {
            return await Common.Post<Country>(Request, Log);
        }
    }
}
