
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Smart.External.Riskline.Entities;
using System.Linq;

namespace Smart.Azure.Functions.External.Riskline
{
    public static class Countries
    {
        private static Util.Azure.CosmosDB.Connection<Util.Azure.CosmosDB.Document<Country>> _connection;
        static Countries()
        {
            _connection = new Util.Azure.CosmosDB.Connection<Util.Azure.CosmosDB.Document<Country>>(
                Environment.GetEnvironmentVariable("db:cosmos:external:url"),
                Environment.GetEnvironmentVariable("db:cosmos:external:key"),
                nameof(Riskline), nameof(Countries));
        }


        [FunctionName(nameof(Countries) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, Util.Http.Methods.Post, Route = "country-reports")]HttpRequest Request, ILogger Log)
        {
            return await Common.Upsert(Request, Log, _connection, (c) => c.Code);
        }
    }
}
