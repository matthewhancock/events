
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CTM.External.Riskline.Entities;
using System.Linq;

namespace Smart.Azure.Functions.External.Riskline
{
    public static class Summaries
    {
        private static Util.Azure.CosmosDB.Connection<Util.Azure.CosmosDB.Document<Summary>> _connection;
        static Summaries()
        {
            _connection = new Util.Azure.CosmosDB.Connection<Util.Azure.CosmosDB.Document<Summary>>(
                Environment.GetEnvironmentVariable("db:cosmos:external:url"),
                Environment.GetEnvironmentVariable("db:cosmos:external:key"),
                nameof(Riskline), nameof(Summaries));
        }


        [FunctionName(nameof(Summaries) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, Common.Util.Http.Methods.Post, Route = "summaries/new")]HttpRequest Request, ILogger Log)
        {
            return await Common.Upsert(Request, Log, _connection, (s) => s.Country);
        }
    }
}
