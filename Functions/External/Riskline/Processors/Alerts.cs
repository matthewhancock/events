
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Smart.External.Riskline.Entities;
using System.Linq;

namespace Smart.Azure.Functions.External.Riskline
{
    public static class Alerts
    {
        private static Util.Azure.CosmosDB.Connection<Util.Azure.CosmosDB.Document<Alert>> _connection;
        static Alerts()
        {
            _connection = new Util.Azure.CosmosDB.Connection<Util.Azure.CosmosDB.Document<Alert>>(
                Environment.GetEnvironmentVariable("db:cosmos:external:url"),
                Environment.GetEnvironmentVariable("db:cosmos:external:key"),
                nameof(Riskline), nameof(Alerts));
        }


        [FunctionName(nameof(Alerts) + nameof(Post))]
        public static async Task<IActionResult> Post([HttpTrigger(AuthorizationLevel.Anonymous, Util.Http.Methods.Post, Route = "alerts")]HttpRequest Request, ILogger Log)
        {
            return await Common.Upsert(Request, Log, _connection, (a) => a.ID.ToString());
        }

        [FunctionName(nameof(Alerts) + nameof(Put))]
        public static async Task<IActionResult> Put([HttpTrigger(AuthorizationLevel.Anonymous, Util.Http.Methods.Put, Route = "alerts/{id}")]HttpRequest Request, int ID, ILogger Log)
        {
            return await Common.Upsert(Request, Log, _connection, (a) => a.ID.ToString());
        }

        [FunctionName(nameof(Alerts) + nameof(Delete))]
        public static async Task<IActionResult> Delete([HttpTrigger(AuthorizationLevel.Anonymous, Util.Http.Methods.Delete, Route = "alerts/{id}")]HttpRequest Request, int ID, ILogger Log)
        {
            return await Common.Delete(Request, Log, _connection, ID.ToString());
        }
    }
}
