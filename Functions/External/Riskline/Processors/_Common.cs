﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using static Smart.Azure.Functions.External.Riskline.Util.Azure.CosmosDB;

namespace Smart.Azure.Functions.External.Riskline
{
    public static class Common
    {
        public const int MaxLogLength = 32768;
        public static async Task<IActionResult> Upsert<Entity>(HttpRequest Request, ILogger Log, Connection<Document<Entity>> Connection, Func<Entity, string> GetDocumentID)
        {
            if (!System.Authentication.Authenticate(Request))
            {
                return new StatusCodeResult(StatusCodes.Status401Unauthorized);
            }

            var json = await new StreamReader(Request.Body).ReadToEndAsync();

            #region "Logging"
            var jsonl = json.Length;
            if (jsonl > MaxLogLength)
            {
                var index = 0;
                while (index < jsonl)
                {
                    var sub = json.Substring(index, Math.Min(MaxLogLength, jsonl - index));
                    Log.LogInformation(sub);
                    index += MaxLogLength;
                }
            }
            else
            {
                Log.LogInformation(json);
            }
            #endregion

            var entity = JsonConvert.DeserializeObject<Entity>(json);

            await Connection.Upsert(new Document<Entity>(GetDocumentID(entity), entity));

            return new OkResult();
        }
        public static async Task<IActionResult> Delete<Entity>(HttpRequest Request, ILogger Log, Connection<Document<Entity>> Connection, string DocumentID)
        {
            if (!System.Authentication.Authenticate(Request))
            {
                return new StatusCodeResult(StatusCodes.Status401Unauthorized);
            }

            Log.LogInformation(DocumentID);
            await Connection.Delete(DocumentID);

            return new OkResult();
        }
    }
}
