﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTM.Common.External.Email
{
    public class SendGrid : IEmailGateway
    {
        private readonly SendGridClient _sender;
        public SendGrid(string ApiKey)
        {
            _sender = new SendGridClient(ApiKey);
        }

        public async Task<Response> SendMessage(string From, IEnumerable<string> To, string Subject, string Message, IEnumerable<string> CC = null, IEnumerable<string> BCC = null, bool HtmlContent = true)
        {
            var email = new SendGridMessage()
            {
                Subject = Subject,
                From = new EmailAddress(From)
            };

            email.AddTos(To.Select(q => new EmailAddress(q)).ToList());
            if (CC != null)
            {
                email.AddCcs(CC.Select(q => new EmailAddress(q)).ToList());
            }
            if (BCC != null)
            {
                email.AddBccs(BCC.Select(q => new EmailAddress(q)).ToList());
            }

            if (HtmlContent)
            {
                email.HtmlContent = Message;
            }
            else
            {
                email.PlainTextContent = Message;
            }

            return await _sender.SendEmailAsync(email);
        }

        async Task<object> IEmailGateway.SendMessage(string From, string[] To, string Subject, string Message, string[] CC, string[] BCC, bool HtmlContent)
        {
            return await SendMessage(From, To, Subject, Message, CC, BCC, HtmlContent);
        }
    }
}
