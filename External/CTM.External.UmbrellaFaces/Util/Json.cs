﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CTM.External.UmbrellaFaces.Util
{
    public static class Json
    {
        // https://stackoverflow.com/questions/18088406/how-to-deserialize-date-milliseconds-with-json-net
        public class UnixMillisecondDateTimeConverter : JsonConverter
        {
            private static DateTime Start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            public override bool CanConvert(Type objectType)
            {
                return objectType == typeof(DateTime);
            }

            public override object ReadJson(JsonReader Reader, Type ObjectType, object ExistingValue, JsonSerializer Serializer)
            {
                if (Reader.Value == null)
                {
                    return null;
                }

                if (Reader.Value is long l)
                {
                    return Start.AddMilliseconds(l);
                }
                else
                {
                    var d = double.Parse(Reader.Value.ToString());
                    return Start.AddMilliseconds(d);
                }
            }

            public override void WriteJson(JsonWriter Writer, object Value, JsonSerializer Serializer)
            {
                var date = (DateTime)Value;
                var span = (date - Start).TotalMilliseconds;
                Serializer.Serialize(Writer, span);
            }
        }
    }
}
