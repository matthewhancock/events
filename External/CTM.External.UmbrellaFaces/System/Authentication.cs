﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CTM.External.UmbrellaFaces.System
{
    public static class Authentication
    {
        public static class Constants
        {
            public static class Headers
            {
                public const string Authorization = "Authorization";
            }
        }

        public static bool Authorize(string Body, string HashKey, string AuthorizationHeader)
        {
            return CheckHash(Body, HashKey, AuthorizationHeader);
        }

        private static bool CheckHash(string Body, string HashKey, string AuthorizationHeader)
        {
            var hash = GenerateHash(Body, HashKey);

            return hash.Equals(AuthorizationHeader);
        }
        private static string GenerateHash(string Body, string HashKey)
        {
            // They remove credit cards before calculating the hash
            var json = JObject.Parse(Body);
            var creditCards = json.SelectToken("creditCards");
            if (creditCards != null)
            {
                foreach (JObject cc in creditCards)
                {
                    cc["number"] = string.Empty;
                }

                Body = JsonConvert.SerializeObject(json);
            }

            using (var sha = new HMACSHA256(Encoding.UTF8.GetBytes(HashKey)))
            {
                var hash = sha.ComputeHash(Encoding.UTF8.GetBytes(Body));
                return BitConverter.ToString(hash).Replace("-", "").ToLower();
            }
        }
    }
}
