﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class EmergencyContact
    {
        [JsonProperty(Constants.Keys.FirstName)] public string FirstName { get; set; }
        [JsonProperty(Constants.Keys.LastName)] public string LastName { get; set; }
        [JsonProperty(Constants.Keys.Email)] public string Email { get; set; }
        [JsonProperty(Constants.Keys.Phone)] public string Phone { get; set; }
    }
}
