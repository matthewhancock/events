﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class RailIdentification
    {
        [JsonProperty(Constants.Keys.Type)] public string Type { get; set; }
        [JsonProperty(Constants.Keys.Identification)] public string Identification { get; set; }
    }
}
