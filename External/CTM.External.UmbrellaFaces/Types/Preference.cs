﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class Preferences
    {
        [JsonProperty(Constants.Keys.PreferredSeat)] public string PreferredSeat { get; set; }
        [JsonProperty(Constants.Keys.PreferredFood)] public string PreferredFood { get; set; }
        [JsonProperty(Constants.Keys.Smoker)] public bool Smoker { get; set; }
    }
}
