﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class TravellerData
    {
        [JsonProperty(Constants.Keys.Username)] public string Username { get; set; }
        [JsonProperty(Constants.Keys.Gender)] public string Gender { get; set; }
        [JsonProperty(Constants.Keys.FirstName)] public string FirstName { get; set; }
        [JsonProperty(Constants.Keys.MiddleName)] public string MiddleName { get; set; }
        [JsonProperty(Constants.Keys.Title)] public string Title { get; set; }
        [JsonProperty(Constants.Keys.Birthdate)] [JsonConverter(typeof(Util.Json.UnixMillisecondDateTimeConverter))] public DateTime? Birthdate { get; set; }

        [JsonProperty(Constants.Keys.BusinessPhone)] public string BusinessPhone { get; set; }
        [JsonProperty(Constants.Keys.PrivatePhone)] public string PrivatePhone { get; set; }
        [JsonProperty(Constants.Keys.MobilePhone)] public string MobilePhone { get; set; }
        [JsonProperty(Constants.Keys.Language)] public string Language { get; set; }
        [JsonProperty(Constants.Keys.Nationality)] public string Nationality { get; set; }
        [JsonProperty(Constants.Keys.SuppressCreditCard)] public bool SuppressCreditCard { get; set; }
        [JsonProperty(Constants.Keys.CompanyName)] public string CompanyName { get; set; }
        [JsonProperty(Constants.Keys.ParentCompanyName)] public string ParentCompanyName { get; set; }
        [JsonProperty(Constants.Keys.ReceiveDocs)] public bool ReceiveDocs { get; set; }
        [JsonProperty(Constants.Keys.Traveller)] public bool Traveller { get; set; }
        [JsonProperty(Constants.Keys.Arranger)] public bool Arranger { get; set; }
        [JsonProperty(Constants.Keys.Approver)] public bool Approver { get; set; }
        [JsonProperty(Constants.Keys.Substitute)] public ArrangerApproverData Substitute { get; set; }
        [JsonProperty(Constants.Keys.OutOfOfficeUntil)] [JsonConverter(typeof(Util.Json.UnixMillisecondDateTimeConverter))] public DateTime? OutOfOfficeUntil { get; set; }
    }
}
