﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class RailPreferences
    {
        [JsonProperty(Constants.Keys.TicketDelivery)] public string TicketDelivery { get; set; }
        [JsonProperty(Constants.Keys.WagonType)] public string WagonType { get; set; }
        [JsonProperty(Constants.Keys.WagonExtraType)] public string WagonExtraType { get; set; }
        [JsonProperty(Constants.Keys.SeatPreference)] public string SeatPreference { get; set; }
        [JsonProperty(Constants.Keys.ClassPreference)] public string ClassPreference { get; set; }
        [JsonProperty(Constants.Keys.RailIdentification)] public RailIdentification RailIdentification { get; set; }
        [JsonProperty(Constants.Keys.RailCards)] public IEnumerable<RailMemberCard> RailCards { get; set; }
    }
}
