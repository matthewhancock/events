﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public abstract class BookingData
    {
        [JsonProperty(Constants.Keys.GenericValues)] public IEnumerable<GenericValue> GenericValues { get; set; }
        [JsonProperty(Constants.Keys.WebCard)] public int WebCard { get; set; }
        [JsonProperty(Constants.Keys.HotelGuarantee)] public int HotelGuarantee { get; set; }
        [JsonProperty(Constants.Keys.CarGuarantee)] public int CarGuarantee { get; set; }
    }

    public class CompanyBookingData : BookingData
    {
        [JsonProperty(Constants.Keys.CorporateAllianceMemberships)] public IEnumerable<CorporateAllianceMembership> CorporateAllianceMemberships { get; set; }
    }

    public class TravellerBookingData : BookingData
    {
        [JsonProperty(Constants.Keys.Passports)] public IEnumerable<Passport> Passports { get; set; }
        [JsonProperty(Constants.Keys.EmployeeCards)] public IEnumerable<EmployeeCard> EmployeeCards { get; set; }
        [JsonProperty(Constants.Keys.Visas)] public IEnumerable<Visa> Visas { get; set; }
        [JsonProperty(Constants.Keys.Preferences)] public Preferences Preferences { get; set; }
        [JsonProperty(Constants.Keys.RailPreferences)] public RailPreferences RailPreferences { get; set; }
        [JsonProperty(Constants.Keys.ResidentInformation)] public ResidentInformation ResidentInformation { get; set; }
        [JsonProperty(Constants.Keys.AllianceMemberships)] public IEnumerable<AllianceMembership> AllianceMemberships { get; set; }

        //[JsonProperty("preferences", NullValueHandling = NullValueHandling.Ignore)]
        //public Preferences Preferences { get; set; }

        //[JsonProperty("railPreferences", NullValueHandling = NullValueHandling.Ignore)]
        //public RailPreferences RailPreferences { get; set; }

        //[JsonProperty("allianceMemberships", NullValueHandling = NullValueHandling.Ignore)]
        //public List<AllianceMembership> AllianceMemberships { get; set; }
    }
}
