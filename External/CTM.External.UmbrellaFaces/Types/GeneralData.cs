﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class GeneralData
    {
        [JsonProperty(Constants.Keys.Name)] public string Name { get; set; }
        [JsonProperty(Constants.Keys.Email)] public string Email { get; set; }
    }

    public class GeneralCompanyData : GeneralData
    {
        [JsonProperty(Constants.Keys.CompanyData)] public CompanyData CompanyData { get; set; }
    }

    public class GeneralTravellerData : GeneralData
    {
        [JsonProperty(Constants.Keys.TravellerData)] public TravellerData TravellerData { get; set; }
    }
}
