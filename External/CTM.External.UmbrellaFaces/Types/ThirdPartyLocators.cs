﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class ThirdPartyLocators
    {
        [JsonProperty("AETM", NullValueHandling = NullValueHandling.Ignore)]
        public string Aetm { get; set; }

        [JsonProperty("CSX", NullValueHandling = NullValueHandling.Ignore)]
        public string Csx { get; set; }

        [JsonProperty("GALILEO_WS", NullValueHandling = NullValueHandling.Ignore)]
        public string GalileoWs { get; set; }
    }
}
