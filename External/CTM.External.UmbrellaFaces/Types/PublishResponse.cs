﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class PublishResponse
    {
        [JsonProperty(Constants.Keys.Status)] public string Status { get; set; }
        [JsonProperty(Constants.Keys.StatusText)] public string StatusText { get; set; }
        [JsonProperty(Constants.Keys.Locator)] public string Locator { get; set; }
    }
}
