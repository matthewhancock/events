﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class GenericValue
    {
        [JsonProperty(Constants.Keys.Label)] public string Label { get; set; }
        [JsonProperty(Constants.Keys.DbiType)] public string DbiType { get; set; }
        [JsonProperty(Constants.Keys.FieldPath)] public string FieldPath { get; set; }
        [JsonProperty(Constants.Keys.InternalCode)] public string InternalCode { get; set; }
        [JsonProperty(Constants.Keys.Value)] public string Value { get; set; }
        [JsonProperty(Constants.Keys.Elective)] public string Elective { get; set; }
    }
}
