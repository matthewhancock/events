﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class CreditCard
    {
        [JsonProperty(Constants.Keys.Number)] public string Number { get; set; }
        [JsonProperty(Constants.Keys.Type)] public string Type { get; set; }
        [JsonProperty(Constants.Keys.Expiration)] [JsonConverter(typeof(Util.Json.UnixMillisecondDateTimeConverter))] public DateTime? Expiration { get; set; }
        [JsonProperty(Constants.Keys.PublishAsFop)] public bool PublishAsFop { get; set; }
        [JsonProperty(Constants.Keys.Remark)] public string Remark { get; set; }
        [JsonProperty(Constants.Keys.Uuid)] public string Uuid { get; set; }
    }
}