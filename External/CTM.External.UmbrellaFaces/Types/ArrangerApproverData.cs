﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class ArrangerApproverData
    {
        [JsonProperty(Constants.Keys.RemoteID)] public Guid RemoteID {get; set; }
        [JsonProperty(Constants.Keys.Email)] public string Email { get; set; }
        [JsonProperty(Constants.Keys.ReceiveDocs)] public bool ReceiveDocs { get; set; }
    }
}
