﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class ResidentInformation
    {
        [JsonProperty(Constants.Keys.Area)] public string Area { get; set; }
        [JsonProperty(Constants.Keys.AreaCode)] public string AreaCode { get; set; }
        [JsonProperty(Constants.Keys.CardType)] public string CardType { get; set; }
        [JsonProperty(Constants.Keys.CardNumber)] public string CardNumber { get; set; }
        [JsonProperty(Constants.Keys.FirstName)] public string FirstName { get; set; }
        [JsonProperty(Constants.Keys.FirstSurname)] public string FirstSurname { get; set; }
        [JsonProperty(Constants.Keys.SecondSurname)] public string SecondSurname { get; set; }
    }
}
