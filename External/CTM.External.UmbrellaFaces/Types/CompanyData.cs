﻿using CTM.External.UmbrellaFaces.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class CompanyData
    {
        [JsonProperty(Constants.Keys.CustomerNumber)] public string CustomerNumber { get; set; }
        [JsonProperty(Constants.Keys.ShortName)] public string ShortName { get; set; }
        [JsonProperty(Constants.Keys.Street)] public string Street { get; set; }
        [JsonProperty(Constants.Keys.Street2)] public string Street2 { get; set; }
        [JsonProperty(Constants.Keys.ZipCode)] public string ZipCode { get; set; }
        [JsonProperty(Constants.Keys.Place)] public string Place { get; set; }
        [JsonProperty(Constants.Keys.CountryCode)] public string CountryCode { get; set; }
        [JsonProperty(Constants.Keys.Phone)] public string Phone { get; set; }
        [JsonProperty(Constants.Keys.Fax)] public string Fax { get; set; }
        [JsonProperty(Constants.Keys.Email2)] public string Email2 { get; set; }
        [JsonProperty(Constants.Keys.Email3)] public string Email3 { get; set; }
        [JsonProperty(Constants.Keys.EmailDelivery)] public string EmailDelivery { get; set; }
        [JsonProperty(Constants.Keys.EmailInvoiceDelivery)] public string EmailInvoiceDelivery { get; set; }
        [JsonProperty(Constants.Keys.Confidential)] public string Confidential { get; set; }
        [JsonProperty(Constants.Keys.ReportingOffice)] public string ReportingOffice { get; set; }
        [JsonProperty(Constants.Keys.RailFormOfPayment)] public string RailFormOfPayment { get; set; }
        [JsonProperty(Constants.Keys.TravellerCreditCardOptions)] public TravellerCreditCardOptions TravellerCreditCardOptions { get; set; }
    }
}
