﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class AllianceMembership
    {
        [JsonProperty(Constants.Keys.Type)] public string Type { get; set; }
        [JsonProperty(Constants.Keys.Code)] public string Code { get; set; }
        [JsonProperty(Constants.Keys.Number)] public string Number { get; set; }
        [JsonProperty(Constants.Keys.AdditionalInformation)] public string AdditionalInformation { get; set; }
        [JsonProperty(Constants.Keys.SpecialRequest)] public string SpecialRequest { get; set; }
    }
}
