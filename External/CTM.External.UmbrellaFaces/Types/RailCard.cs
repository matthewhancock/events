﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class RailMemberCard
    {
        [JsonProperty(Constants.Keys.Type)] public string Type { get; set; }
        [JsonProperty(Constants.Keys.RailClass)] public string RailClass { get; set; }
        [JsonProperty(Constants.Keys.Number)] public string Number { get; set; }
        [JsonProperty(Constants.Keys.Expiration)] [JsonConverter(typeof(Util.Json.UnixMillisecondDateTimeConverter))] public DateTime? Expiration { get; set; }
        [JsonProperty(Constants.Keys.CollectBonusPoints)] public bool CollectBonusPoints { get; set; }
    }
}
