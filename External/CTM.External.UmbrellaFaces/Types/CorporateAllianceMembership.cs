﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class CorporateAllianceMembership : AllianceMembership
    {
        [JsonProperty(Constants.Keys.AdditionalInformation2)] public string AdditionalInformation2 { get; set; }
        [JsonProperty(Constants.Keys.Flag)] public string Flag { get; set; }
    }
}
