﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class EmployeeCard
    {
        [JsonProperty(Constants.Keys.Number)] public string Number { get; set; }
        [JsonProperty(Constants.Keys.Nationality)] public string Nationality { get; set; }
        [JsonProperty(Constants.Keys.IssueDate)] [JsonConverter(typeof(Util.Json.UnixMillisecondDateTimeConverter))] public DateTime IssueDate { get; set; }
        [JsonProperty(Constants.Keys.IssuePlace)] public string IssuePlace { get; set; }
        [JsonProperty(Constants.Keys.ExpirationDate)] [JsonConverter(typeof(Util.Json.UnixMillisecondDateTimeConverter))] public DateTime ExpirationDate { get; set; }
    }
}
