﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Types
{
    public class TravellerCreditCardOptions
    {
        [JsonProperty(Constants.Keys.Fop)] public string Fop { get; set; }
        [JsonProperty(Constants.Keys.WebCard)] public string WebCard { get; set; }
        [JsonProperty(Constants.Keys.CarGuarantee)] public string CarGuarantee { get; set; }
        [JsonProperty(Constants.Keys.HotelGuarantee)] public string HotelGuarantee { get; set; }
    }
}
