﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using CTM.External.UmbrellaFaces.Entities;

namespace CTM.External.UmbrellaFaces.Types
{
    [JsonConverter(typeof(ProfileJsonConverter))]
    public class PublishRequest
    {
        [JsonProperty(Constants.Keys.Type)] public string Type { get; set; }
        [JsonProperty(Constants.Keys.Action)] public string Action { get; set; }
        [JsonProperty(Constants.Keys.AgencyID)] public string AgencyID { get; set; }
        [JsonProperty(Constants.Keys.ThirdPartyLocators)] public ThirdPartyLocators ThirdPartyLocators { get; set; }
        [JsonProperty(Constants.Keys.ID)] public string ID { get; set; }
        [JsonProperty(Constants.Keys.RemoteID)] public Guid RemoteID { get; set; }
        [JsonProperty(Constants.Keys.RequestTime)] [JsonConverter(typeof(Util.Json.UnixMillisecondDateTimeConverter))] public DateTime RequestTime { get; set; }
        [JsonProperty(Constants.Keys.EmergencyContact)] public EmergencyContact EmergencyContact { get; set; }
        [JsonProperty(Constants.Keys.FormOfPaymentDirective)] public string FormOfPaymentDirective { get; set; }
        [JsonProperty(Constants.Keys.CreditCards)] public IEnumerable<CreditCard> CreditCards { get; set; }
        [JsonProperty(Constants.Keys.DataToCrs)] public string DataToCrs { get; set; }
        [JsonProperty(Constants.Keys.Comment)] public string Comment { get; set; }
        [JsonProperty(Constants.Keys.Creator)] public string Creator { get; set; }
        [JsonProperty(Constants.Keys.AgencyCreditCards)] public IEnumerable<string> AgencyCreditCards { get; set; }
        [JsonProperty(Constants.Keys.SupportedLanguages)] public IEnumerable<string> SupportedLanguages { get; set; }
    }

    public class ProfileJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type ObjectType) => ObjectType == typeof(PublishRequest);

        public override object ReadJson(JsonReader Reader, Type ObjectType, object ExistingValue, JsonSerializer Serializer)
        {
            if (ObjectType == typeof(PublishRequest))
            {
                var json = JObject.Load(Reader);

                switch (json[Constants.Keys.Type].Value<string>())
                {
                    case Constants.Values.Types.Traveller:
                        return json.ToObject<Traveller>(Serializer);
                    case Constants.Values.Types.Company:
                        return json.ToObject<Company>(Serializer);
                    default:
                        throw new NotImplementedException();
                }
            }
            else
            {
                // Need to skip the converter on all subtypes other than the explicitly specified Profile class
                Serializer.ContractResolver.ResolveContract(ObjectType).Converter = null;
                return Serializer.Deserialize(Reader, ObjectType);
            }
        }

        public override bool CanWrite => false;
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new NotImplementedException(); // won't be called because CanWrite returns false
    }
}
