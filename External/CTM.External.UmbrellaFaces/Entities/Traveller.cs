﻿using CTM.External.UmbrellaFaces.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Entities
{
    public class Traveller : PublishRequest
    {
        [JsonProperty(Constants.Keys.GeneralData)] public GeneralTravellerData GeneralData { get; set; }
        [JsonProperty(Constants.Keys.BookingData)] public TravellerBookingData BookingData { get; set; }
        [JsonProperty(Constants.Keys.CompanyID)] public Guid CompanyID { get; set; }
        [JsonProperty(Constants.Keys.CompanyExternalNumber)] public string CompanyExternalNumber { get; set; }
        [JsonProperty(Constants.Keys.CompanyShortName)] public string CompanyShortName { get; set; }
        [JsonProperty(Constants.Keys.Arrangers)] public IEnumerable<ArrangerApproverData> Arrangers { get; set; }
        [JsonProperty(Constants.Keys.Approvers)] public IEnumerable<ArrangerApproverData> Approvers { get; set; }


        [JsonIgnore]
        public string DisplayName => $"{GeneralData.TravellerData.FirstName} {GeneralData.Name}";
    }
}
