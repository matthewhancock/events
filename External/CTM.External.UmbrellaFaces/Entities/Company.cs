﻿using CTM.External.UmbrellaFaces.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CTM.External.UmbrellaFaces.Entities
{
    public class Company : PublishRequest
    {
        [JsonProperty(Constants.Keys.GeneralData)] public GeneralCompanyData GeneralData { get; set; }
        [JsonProperty(Constants.Keys.BookingData)] public CompanyBookingData BookingData { get; set; }


        [JsonIgnore]
        public string DebtorIdentifier => BookingData.GenericValues.Where(q => q.InternalCode == Constants.Values.GenericValues.InternalCode.DebtorIdentifier).FirstOrDefault()?.Value;
    }
}
