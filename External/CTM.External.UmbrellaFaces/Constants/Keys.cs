﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Constants
{
    public static class Keys
    {
        public const string ID = "id";
        public const string RemoteID = "remoteId";

        public const string Type = "type";
        public const string Code = "code";
        public const string Name = "name";
        public const string Number = "number";
        public const string Email = "email";
        public const string Phone = "phone";

        public const string WebCard = "webCard";
        public const string CarGuarantee = "carGuarantee";
        public const string HotelGuarantee = "hotelGuarantee";

        // AllianceMembership
        public const string AdditionalInformation = "additionalInfo";
        public const string SpecialRequest = "specialRequest";
        public const string AdditionalInformation2 = "additionalInfo2";
        public const string Flag = "flag";
        // ArrangerApproverData
        public const string ReceiveDocs = "receiveDocs";
        // BookingData
        public const string Passports = "passports";
        public const string EmployeeCards = "employeecards";
        public const string Visas = "visa";
        public const string GenericValues = "genericValues";
        public const string Preferences = "preferences";
        public const string RailPreferences = "railPreferences";
        public const string ResidentInformation = "residentInformation";
        public const string AllianceMemberships = "allianceMemberships";
        public const string CorporateAllianceMemberships = "corporateAllianceMemberships";
        // CompanyData
        public const string CustomerNumber = "customerNumber";
        public const string ShortName = "shortname";
        public const string Street = "street";
        public const string Street2 = "street2";
        public const string ZipCode = "zipCode";
        public const string Place = "place";
        public const string CountryCode = "countryCode";
        public const string Fax = "fax";
        public const string Email2 = "email2";
        public const string Email3 = "email3";
        public const string EmailDelivery = "emailDelivery";
        public const string EmailInvoiceDelivery = "emailInvoiceDelivery";
        public const string Confidential = "confidential";
        public const string ReportingOffice = "reportingOffice";
        public const string RailFormOfPayment = "railFormOfPayment";
        public const string TravellerCreditCardOptions = "travellerCcOptions";
        // CreditCard
        public const string Expiration = "expiration";
        public const string PublishAsFop = "publishAsFop";
        public const string Remark = "remark";
        public const string Uuid = "uuid";
        // EmergencyContact
        public const string FirstName = "firstname";
        public const string LastName = "lastname";
        // GeneralData
        public const string CompanyData = "companyData";
        public const string TravellerData = "travellerData";
        // GenericValue
        public const string Label = "label";
        public const string DbiType = "dbiType";
        public const string FieldPath = "fieldPath";
        public const string InternalCode = "internalCode";
        public const string Value = "value";
        public const string Elective = "elective";
        // Passport
        public const string Nationality = "nationality";
        public const string IssueDate = "issueDate";
        public const string IssuePlace = "issuePlace";
        public const string ExpirationDate = "expirationDate";
        public const string IssueCountry = "issueCountry";
        public const string Primary = "primary";
        // Preference
        public const string PreferredSeat = "preferredSeat";
        public const string PreferredFood = "preferredFood";
        public const string Smoker = "smoker";
        // PublishRequest
        public const string Action = "action";
        public const string RequestTime = "requestTime";
        public const string AgencyID = "agencyId";
        public const string CompanyID = "companyId";
        public const string CompanyExternalNumber = "companyExternalNr";
        public const string CompanyShortName = "companyShortname";
        public const string ThirdPartyLocators = "thirdPartyLocators";
        public const string GeneralData = "generalData";
        public const string BookingData = "bookingData";
        public const string Arrangers = "arrangers";
        public const string Approvers = "approvers";
        public const string EmergencyContact = "emergencyContact";
        public const string FormOfPaymentDirective = "formOfPaymentDirective";
        public const string CreditCards = "creditCards";
        public const string DataToCrs = "dataToCrs";
        public const string Comment = "comment";
        public const string Creator = "creator";
        public const string AgencyCreditCards = "agencyCreditCards";
        public const string SupportedLanguages = "supportedLanguages";
        // PublishResponse
        public const string Status = "status";
        public const string StatusText = "statusText";
        public const string Locator = "locator";
        // RailIdentification
        public const string Identification = "identification";
        // RailMemberCard
        public const string RailClass = "railClass";
        public const string CollectBonusPoints = "collectBonusPoints";
        // RailPreferences
        public const string TicketDelivery = "ticketDelivery";
        public const string WagonType = "wagonType";
        public const string WagonExtraType = "wagonExtraType";
        public const string SeatPreference = "seatPreference";
        public const string ClassPreference = "classPreference";
        public const string RailIdentification = "railIdentification";
        public const string RailCards = "railCards";
        // ResidentInformation
        public const string Area = "area";
        public const string AreaCode = "areaCode";
        public const string CardType = "cardType";
        public const string CardNumber = "cardNumber";
        public const string FirstSurname = "firstSurname";
        public const string SecondSurname = "secondSurname";
        // TravellerCreditCardOptions
        public const string Fop = "fop";
        // TravellerData
        public const string Username = "username";
        public const string Gender = "gender";
        public const string MiddleName = "middlename";
        public const string Title = "title";
        public const string Birthdate = "birthdate";
        public const string BusinessPhone = "businessPhone";
        public const string PrivatePhone = "privatePhone";
        public const string MobilePhone = "mobilePhone";
        public const string Language = "language";
        public const string SuppressCreditCard = "suppressCreditCard";
        public const string CompanyName = "companyName";
        public const string ParentCompanyName = "parentCompanyName";
        public const string Traveller = "traveller";
        public const string Arranger = "arranger";
        public const string Approver = "approver";
        public const string Substitute = "substitute";
        public const string OutOfOfficeUntil = "outOfOfficeUntil";
        // VIsa
        public const string EntryType = "entryType";
    }
}
