﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.UmbrellaFaces.Constants
{
    public static class Values
    {
        public static class Types
        {
            public const string Company = "COMPANY";
            public const string Traveller = "TRAVELLER";
        }
        public static class Actions
        {
            public const string Save = "SAVE";
            public const string Delete = "DELETE";
        }
        public static class Status
        {
            public const string Ok = "OK";
            public const string Fail = "FAIL";
        }
        public static class AllianceMembership
        {
            public static class Types
            {
                public const string Car = "RENTALCAR";
                public const string FrequentFlyer = "FLIGHT";
                public const string Hotel = "HOTEL";
            }
        }
        public static class GenericValues
        {
            public static class InternalCode
            {
                /// <summary>
                /// Customer DK (All)
                /// </summary>
                public const string DebtorIdentifier = "OtherCustomerDKTripTyp";
            }
        }
    }
}
