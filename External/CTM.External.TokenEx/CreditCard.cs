﻿using Ctm.Vault;
using Ctm.Vault.Http;
using Ctm.Vault.TokenEx;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CTM.External.TokenEx
{
    public static class CreditCard
    {


        public static async Task<string> Tokenize(string CreditCardNumber)
        {
            var v = new Vault(new TokenExVaultClient(new ClientFactory(new TokenExHttpClient(GetConfiguration(), new HttpClient()))));

            var deposit = v.Deposit(CreditCardNumber);

            return await Task.FromResult(deposit.Token);
        }


        private static TokenExVaultConfiguration GetConfiguration()
        {
            return new TokenExVaultConfiguration(
                Environment.GetEnvironmentVariable(Constants.Configuration.Keys.ApiKey),
                Environment.GetEnvironmentVariable(Constants.Configuration.Keys.ID),
                Environment.GetEnvironmentVariable(Constants.Configuration.Keys.Endpoint));
        }

        public class TokenExVaultConfiguration : Ctm.Vault.TokenEx.Configuration.ITokenExVaultConfiguration
        {
            public TokenExVaultConfiguration(string ApiKey, string TokenExId, string EndpointUri)
            {
                this.ApiKey = ApiKey;
                this.TokenExId = TokenExId;
                this.EndpointUri = EndpointUri;
            }
            public string ApiKey { get; set; }
            public string TokenExId { get; set; }
            public string EndpointUri { get; set; }
        }
    }
}
