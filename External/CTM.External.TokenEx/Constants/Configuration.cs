﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.TokenEx.Constants
{
    public static class Configuration
    {
        public static class Keys
        {
            public const string ID = "external:tokenex:id";
            public const string ApiKey = "external:tokenex:key";
            public const string Endpoint = "external:tokenex:endpoint";
        }
    }
}
