﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Types
{
    public class GeoLocation
    {
        [JsonProperty(Constants.Keys.GeoLatitude)] public double Latitude { get; private set; }
        [JsonProperty(Constants.Keys.GeoLongitude)] public double Longitude { get; private set; }
    }
}
