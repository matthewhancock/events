﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Types
{
    public class Duration
    {
        [JsonProperty(Constants.Keys.StartDate)] [JsonConverter(typeof(UnixDateTimeConverter))] public DateTime StartDate { get; private set; }
        [JsonProperty(Constants.Keys.EndDate)] [JsonConverter(typeof(UnixDateTimeConverter))] public DateTime EndDate { get; private set; }
    }
}
