﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Types
{
    public class Geometry
    {
        [JsonProperty("name")] public string Name { get; private set; }
        [JsonProperty("coordinates")] public JArray Coordinates { get; private set; }
        [JsonProperty("color")] public string Color { get; private set; }
        [JsonProperty("text")] public string Text { get; private set; }
    }
}
