﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Types
{
    public class Region
    {
        [JsonProperty(Constants.Keys.Name)] public string Name { get; private set; }
        [JsonProperty(Constants.Keys.Code)] public string Code { get; private set; }
    }
}
