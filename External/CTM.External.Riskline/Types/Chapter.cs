﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Types
{
    public class Chapter
    {
        [JsonProperty(Constants.Keys.ChapterID)] public int ChapterID { get; private set; }
        [JsonProperty(Constants.Keys.Title)] public string Title { get; private set; }
        [JsonProperty(Constants.Keys.Sections)] public IEnumerable<Section> Sections { get; private set; }
        [JsonProperty(Constants.Keys.RiskLevel)] public string RiskLevel { get; private set; }


        public class Section
        {
            [JsonProperty(Constants.Keys.ChapterID)] public int ChapterID { get; private set; }
            [JsonProperty(Constants.Keys.Title)] public string Title { get; private set; }
            [JsonProperty(Constants.Keys.Text)] public string Text { get; private set; }
            [JsonProperty(Constants.Keys.Subcategory)] public string Subcategory { get; private set; }
            [JsonProperty(Constants.Keys.SubcategoryID)] public int SubcategoryID { get; private set; }
        }
    }
}
