﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Entities
{
    public class Alert
    {
        [JsonProperty(Constants.Keys.ID)] public int ID { get; private set; }
        [JsonProperty(Constants.Keys.Title)] public string Title { get; private set; }
        [JsonProperty(Constants.Keys.Text)] public string Text { get; private set; }
        [JsonProperty(Constants.Keys.Country)] public string Country { get; private set; }
        [JsonProperty(Constants.Keys.Breaking)] public bool Breaking { get; private set; }
        [JsonProperty(Constants.Keys.RiskLevel)] public string RiskLevel { get; private set; }
        [JsonProperty(Constants.Keys.Duration)] public Types.Duration Duration { get; private set; }
        [JsonProperty(Constants.Keys.Category)] public string Category { get; private set; }
        [JsonProperty(Constants.Keys.Airports)] public string Airports { get; private set; }
        [JsonProperty(Constants.Keys.Regions)] public IEnumerable<Types.Region> Regions { get; private set; }
        [JsonProperty(Constants.Keys.Geo)] public Types.GeoLocation GeoLocation { get; private set; }
        [JsonProperty(Constants.Keys.LocationExplanation)] public string GeoLocationDescription { get; private set; }
    }
}
