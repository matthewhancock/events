﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Entities
{
    public class PreTravelAdvisory
    {
        [JsonProperty(Constants.Keys.Country)] public string Country { get; private set; }
        [JsonProperty(Constants.Keys.RiskLevel)] public string RiskLevel { get; private set; }
        [JsonProperty(Constants.Keys.ClientID)] public int ClientID { get; private set; }
        [JsonProperty(Constants.Keys.File)] public string File { get; private set; }
    }
}
