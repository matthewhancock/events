﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Entities
{
    public class Country
    {
        [JsonProperty(Constants.Keys.Country)] public string Name { get; private set; }
        [JsonProperty(Constants.Keys.CountryCode)] public string Code { get; private set; }
        [JsonProperty(Constants.Keys.RiskLevel)] public string RiskLevel { get; private set; }
        [JsonProperty(Constants.Keys.ClientID)] public float ClientID { get; private set; }
        [JsonProperty(Constants.Keys.Chapters)] public IEnumerable<Types.Chapter> Chapters { get; private set; }
        [JsonProperty(Constants.Keys.Summary)] public Summary Summary { get; private set; }
        [JsonProperty(Constants.Keys.Pdf)] public string ReportUrl { get; private set; }
        [JsonProperty(Constants.Keys.Geometries)] public IEnumerable<Types.Geometry> Geometries { get; private set; }
    }
}
