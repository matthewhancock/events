﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Entities
{
    public class Summary
    {
        [JsonProperty(Constants.Keys.Summary)] public string Text { get; private set; }
        [JsonProperty(Constants.Keys.Country)] public string Country { get; private set; }
        [JsonProperty(Constants.Keys.RiskLevel)] public string RiskLevel { get; private set; }
        [JsonProperty(Constants.Keys.ClientID)] public int ClientID { get; private set; }
    }
}
