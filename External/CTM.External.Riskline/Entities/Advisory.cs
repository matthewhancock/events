﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Entities
{
    public class Advisory
    {
        [JsonProperty(Constants.Keys.ID)] public int ID { get; private set; }
        [JsonProperty(Constants.Keys.Text)] public string Text { get; private set; }
        [JsonProperty(Constants.Keys.IsPublished)] public bool Published { get; private set; }
        [JsonProperty(Constants.Keys.Title)] public string Title { get; private set; }
        [JsonProperty(Constants.Keys.RiskLevelID)] public int RiskLevelID { get; private set; }
        [JsonProperty(Constants.Keys.RiskLevel)] public string RiskLevel { get; private set; }
        [JsonProperty(Constants.Keys.Country)] public string Country { get; private set; }
        [JsonProperty(Constants.Keys.Geometries)] public IEnumerable<Types.Geometry> Geometries { get; private set; }
        [JsonProperty(Constants.Keys.CountryGeometries)] public IEnumerable<Types.Geometry> CountryGeometries { get; private set; }
    }
}
