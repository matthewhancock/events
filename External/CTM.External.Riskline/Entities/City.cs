﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Entities
{
    public class City
    {
        [JsonProperty(Constants.Keys.Name)] public string Name { get; private set; }
        [JsonProperty(Constants.Keys.Code)] public string Code { get; private set; }
        [JsonProperty(Constants.Keys.Country)] public string Country { get; private set; }
        [JsonProperty(Constants.Keys.RiskLevelID)] public int RiskLevelID { get; private set; }
        [JsonProperty(Constants.Keys.Overview)] public string Overview { get; private set; }
        [JsonProperty(Constants.Keys.Arrival)] public string Arrival { get; private set; }
        [JsonProperty(Constants.Keys.GettingAround)] public string GettingAround { get; private set; }
        [JsonProperty(Constants.Keys.Health)] public string Health { get; private set; }
        [JsonProperty(Constants.Keys.Calendar)] public string Calendar { get; private set; }
        [JsonProperty(Constants.Keys.Latitude)] public float? Latitude { get; private set; }
        [JsonProperty(Constants.Keys.Longitude)] public float? Longitude { get; private set; }
        [JsonProperty(Constants.Keys.Zoom)] public float? Zoom { get; private set; }
        [JsonProperty(Constants.Keys.ClientID)] public float CompanyID { get; private set; }
    }
}
