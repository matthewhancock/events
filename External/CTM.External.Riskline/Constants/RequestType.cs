﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Constants
{
    public static class RequestType
    {
        public const string Post = nameof(Post);
        public const string Put = nameof(Put);
        public const string Delete = nameof(Delete);
    }
}
