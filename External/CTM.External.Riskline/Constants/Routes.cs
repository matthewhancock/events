﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Constants
{
    public static class Routes
    {
        public static class Paths
        {
            public const string ID = "/{id}";
        }

        public const string Advisories = "advisories";
        public const string AdvisoriesWithID = "advisories" + Paths.ID;

        public const string Alerts = "alerts";
        public const string AlertsWithID = "alerts" + Paths.ID;

        public const string Cities = "cities";

        public const string CountryReports = "country-reports";

        public const string PreTravelAdvisories = "ptas";

        public const string Summaries = "summaries/new";
    }
}
