﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTM.External.Riskline.Constants
{    public static class Keys
    {
        public const string ID = "id";
        public const string Title = "title";
        public const string Text = "text";
        public const string Category = "category";
        public const string Code = "code";
        public const string Name = "name";

        public const string IsPublished = "is_published";

        public const string RiskLevel = "risk_level";
        public const string RiskLevelID = "risk_level_id";

        public const string Country = "country";
        public const string CountryCode = "country_code";
        public const string Regions = "regions";
        public const string Airports = "airports";

        public const string Geometries = "geometries";
        public const string CountryGeometries = "country_geometries";
        public const string Geo = "geo";
        public const string GeoLatitude = "lat";
        public const string GeoLongitude = "lng";
        public const string Latitude = "latitude";
        public const string Longitude = "longitude";

        public const string Breaking = "breaking";
        public const string LocationExplanation = "location_explanation";

        public const string Duration = "duration";
        public const string StartDate = "start_date";
        public const string EndDate = "end_date";

        public const string Overview = "overview";
        public const string Arrival = "arrival";
        public const string GettingAround = "getting_around";
        public const string Safety = "safety";
        public const string Health = "health";
        public const string Calendar = "calendar";
        public const string Zoom = "zoom";
        public const string ClientID = "client_id";

        public const string Chapters = "chapters";
        public const string ChapterID = "chapter_id";
        public const string Sections = "sections";
        public const string Subcategory = "subcategory";
        public const string SubcategoryID = "subcategory_id";
        public const string Summary = "summary";
        public const string Pdf = "pdf";
        public const string File = "file";
    }
}
